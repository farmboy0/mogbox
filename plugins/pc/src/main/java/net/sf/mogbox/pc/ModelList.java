/* 
 * ModelList.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pc;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

class ModelList {
	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	private Entry[] list;

	public ModelList(String filename) {
		InputStream in = ModelList.class.getResourceAsStream(filename);
		if (in == null) {
			list = new Entry[0];
			return;
		}

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(true);

		try {
			SAXParser parser = factory.newSAXParser();
			parser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			parser.setProperty(JAXP_SCHEMA_SOURCE, getClass().getResourceAsStream("list.xsd"));
			parser.parse(in, new ListContentHandeler());
		} catch (Throwable t) {
			RuntimeException exception = new RuntimeException(t.getMessage(), t.getCause());
			exception.setStackTrace(t.getStackTrace());
			throw exception;
		}
	}

	public String getName(int id, int race) {
		if (id < 0 || id >= list.length)
			return null;
		Entry entry = list[id];
		if (entry == null)
			return null;
		if (entry.alts != null && race >= 0 && race <= 7) {
			Entry alt = entry.alts[race];
			if (alt != null && alt.name != null) {
				return alt.name;
			}
		}
		return entry.name;
	}

	public boolean isHidden(int id, int race) {
		if (id < 0 || id >= list.length)
			return false;
		Entry entry = list[id];
		if (entry == null)
			return false;
		if (entry.alts != null && race >= 0 && race <= 7) {
			Entry alt = entry.alts[race];
			if (alt != null && alt.hide != null) {
				return alt.hide;
			}
		}
		return entry.hide != null ? entry.hide : false;
	}

	public int[] getOverrides(int id, int race) {
		if (id < 0 || id >= list.length)
			return null;
		Entry entry = list[id];
		if (entry == null)
			return null;
		if (entry.alts != null && race >= 0 && race <= 7) {
			Entry alt = entry.alts[race];
			if (alt != null && alt.overrides != null) {
				return alt.overrides;
			}
		}
		return entry.overrides;
	}

	private class Entry {
		public int id;
		public Boolean hide;
		public String name;
		public int[] overrides;
		public Entry[] alts;
	}

	private class ListContentHandeler extends DefaultHandler {
		private ArrayList<Entry> workingList;

		private Entry entry;
		private Entry alt;

		public void startDocument() throws SAXException {
			workingList = new ArrayList<Entry>();
		}

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			if (ns.equals("urn:xmlns:mogbox:pc:1.0")) {
				if (name.equals("item")) {
					entry = new Entry();
					entry.id = Integer.parseInt(attrs.getValue("", "id"));
					process(entry, attrs);
				} else if (name.equals("alt")) {
					alt = new Entry();
					String race = attrs.getValue("", "race");
					if (race.equals("HM"))
						alt.id = 0;
					else if (race.equals("HF"))
						alt.id = 1;
					else if (race.equals("EM"))
						alt.id = 2;
					else if (race.equals("EF"))
						alt.id = 3;
					else if (race.equals("TM"))
						alt.id = 4;
					else if (race.equals("TF"))
						alt.id = 5;
					else if (race.equals("M"))
						alt.id = 6;
					else if (race.equals("G"))
						alt.id = 7;
					process(alt, attrs);
				}
			}
		}

		private void process(Entry entry, Attributes attrs) {
			entry.name = attrs.getValue("", "name");

			String temp;

			if ((temp = attrs.getValue("", "hide")) != null)
				entry.hide = temp.equals("true");

			boolean set = false;
			int[] overrides = { -1, -1, -1, -1, -1, -1, -1, -1, -1 };

			if ((temp = attrs.getValue("", "face")) != null) {
				set = true;
				overrides[0] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "head")) != null) {
				set = true;
				overrides[1] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "body")) != null) {
				set = true;
				overrides[2] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "hands")) != null) {
				set = true;
				overrides[3] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "legs")) != null) {
				set = true;
				overrides[4] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "feet")) != null) {
				set = true;
				overrides[5] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "main")) != null) {
				set = true;
				overrides[6] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "sub")) != null) {
				set = true;
				overrides[7] = Integer.parseInt(temp);
			}
			if ((temp = attrs.getValue("", "ranged")) != null) {
				set = true;
				overrides[8] = Integer.parseInt(temp);
			}

			if (set)
				entry.overrides = overrides;
		}

		@Override
		public void endElement(String ns, String name, String qName) throws SAXException {
			if (ns.equals("urn:xmlns:mogbox:pc:1.0")) {
				if (name.equals("item")) {
					if (workingList.size() > entry.id) {
						workingList.set(entry.id, entry);
					} else {
						for (int i = workingList.size(); i < entry.id; i++)
							workingList.add(i, null);
						workingList.add(entry.id, entry);
					}
				} else if (name.equals("alt")) {
					if (entry.alts == null)
						entry.alts = new Entry[8];
					entry.alts[alt.id] = alt;
				}
			}
		}

		@Override
		public void endDocument() throws SAXException {
			Entry[] temp = new Entry[workingList.size()];
			list = workingList.toArray(temp);
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void warning(SAXParseException e) throws SAXException {
			e.printStackTrace();
		}
	}
}
