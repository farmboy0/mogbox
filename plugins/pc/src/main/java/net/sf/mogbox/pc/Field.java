/* 
 * Field.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pc;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

public class Field {
	private static final String[] files = { "/face.xml", "/head.xml", "/body.xml", "/hands.xml", "/legs.xml",
			"/feet.xml", "/main.xml", "/sub.xml", "/ranged.xml" };

	private int slot;

	private int race = 0;

	private int selection;
	private boolean enabled;
	private int override = -1;

	private ModelList list;
	private int[][] ids;

	private Label label;
	private Composite comboPanel;
	private Combo[] combos;
	private Button check;

	private StackLayout comboStack;

	List<StateListener> listeners = new LinkedList<StateListener>();

	public Field(Composite parent, String name, int slot) {
		this.slot = slot;

		list = new ModelList(files[slot]);

		label = new Label(parent, SWT.NONE);
		comboPanel = new Composite(parent, SWT.NONE);
		check = new Button(parent, SWT.CHECK);

		label.setText(name);

		EventHandler handler = new EventHandler();
		check.addListener(SWT.Selection, handler);

		enabled = check.getSelection();

		ids = new int[8][PlayerCharacter.getMax(slot)];
		combos = new Combo[8];
		for (int i = 0; i < 8; i++) {
			combos[i] = new Combo(comboPanel, SWT.READ_ONLY);
			combos[i].setVisibleItemCount(16);
			combos[i].addListener(SWT.Selection, handler);
			combos[i].setEnabled(enabled);
		}

		comboStack = new StackLayout();
		comboPanel.setLayout(comboStack);
		comboStack.topControl = combos[race];

		loadList();
	}

	public Control getLabel() {
		return label;
	}

	public Control getCombo() {
		return comboPanel;
	}

	public Control getCheck() {
		return check;
	}

	public int getSlot() {
		return slot;
	}

	public void setRace(int race) {
		if (race >= 0 && race < combos.length) {
			this.race = race;
			comboStack.topControl = combos[race];
			if (override < 0) {
				for (Combo c : combos)
					c.setEnabled(enabled);
				selection = combos[race].getSelectionIndex();
				if (selection >= 0)
					selection = ids[race][selection];
			}
		} else {
			this.race = -1;
			comboStack.topControl = combos[0];
			for (Combo c : combos)
				c.setEnabled(false);
			selection = -1;
		}
		comboPanel.layout();
	}

	public void setEnabled(boolean enabled) {
		check.setSelection(enabled);
		processEnabledState();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setSelection(int id) {
		if (race < 0 || override >= 0) {
			selection = id;
		} else {
			int index = 0;
			for (int i = 0; i < ids[race].length; i++) {
				if (ids[race][i] <= id)
					index = i;
				else
					break;
			}
			combos[race].select(index);
		}
		processSelectionChange();
	}

	public int getSelection() {
		return selection;
	}

	public int[] getOverrides(int id) {
		return list.getOverrides(id, race);
	}

	public void setOverride(int id) {
		if (id < 0) {
			if (override < 0)
				return;
			override = -1;
			check.setSelection(enabled);
			check.setEnabled(true);
			for (int race = 0; race < 8; race++) {
				int index = 0;
				for (int i = 0; i < ids[race].length; i++) {
					if (ids[race][i] <= selection)
						index = i;
					else
						break;
				}
				combos[race].select(index);
				combos[race].remove(combos[race].getItemCount() - 1);
				combos[race].setEnabled(enabled);
			}
		} else {
			check.setSelection(false);
			for (int race = 0; race < 8; race++) {
				combos[race].setEnabled(false);
				check.setEnabled(false);
				check.setSelection(true);
				int index = combos[race].getItemCount();
				if (override < 0) {
					combos[race].add(getName(id, race));
					combos[race].select(index);
				} else {
					combos[race].setItem(index - 1, getName(id, race));
					combos[race].select(index - 1);
				}
			}
			override = id;
		}
	}

	public int getOverride() {
		return override;
	}

	public void addStateListener(StateListener l) {
		listeners.add(l);
	}

	public void removeStateListener(StateListener l) {
		listeners.remove(l);
	}

	private void loadList() {
		for (int race = 0; race < 8; race++) {
			String[] names = new String[ids[race].length];

			int number = 0;
			for (int i = 0; i < names.length; i++) {
				if (PlayerCharacter.isUsed(race, slot, i) && !list.isHidden(i, race)) {
					ids[race][number] = i;
					names[number] = getName(i, race);
					number++;
				}
			}
			ids[race] = Arrays.copyOf(ids[race], number);
			combos[race].setItems(Arrays.copyOf(names, number));
		}
	}

	private String getName(int id, int race) {
		String name = list.getName(id, race);
		if (name == null)
			return Strings.getString("unknown") + " [" + id + "]";
		return name;
	}

	private void processEnabledState() {
		enabled = check.getSelection();
		if (race >= 0 && override < 0) {
			for (Combo c : combos)
				c.setEnabled(enabled);
		}
	}

	private void processSelectionChange() {
		if (override < 0) {
			if (race >= 0) {
				selection = combos[race].getSelectionIndex();
				if (selection >= 0)
					selection = ids[race][selection];
			}
			for (int race = 0; race < 8; race++) {
				int index = 0;
				for (int i = 0; i < ids[race].length; i++) {
					if (ids[race][i] <= selection)
						index = i;
					else
						break;
				}
				combos[race].select(index);
			}
		}
	}

	public void notifyStateListeners() {
		StateEvent e = new StateEvent(this);
		for (StateListener l : listeners)
			l.stateChanged(e);
	}

	private class EventHandler implements Listener {
		@Override
		public void handleEvent(Event e) {
			if (e.widget == check)
				processEnabledState();
			else
				processSelectionChange();
			notifyStateListeners();
		}
	}
}