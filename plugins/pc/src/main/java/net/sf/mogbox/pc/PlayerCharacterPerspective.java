/* 
 * PlayerCharacterPerspective.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pc;

import java.net.URL;

import net.sf.mogbox.preferences.Preferences;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.swt.Perspective;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

public class PlayerCharacterPerspective implements Perspective {
	private PlayerCharacter character;
	private Preferences preferences;

	public PlayerCharacterPerspective() {
		character = new PlayerCharacter();
		preferences = Preferences.getInstance(this);
	}

	@Override
	public Node getNode() {
		return character;
	}

	@Override
	public String getName() {
		return "mogbox.pc";
	}

	@Override
	public String getDisplayName() {
		return Strings.getString("perspective.name");
	}

	@Override
	public URL getIconLocation() {
		return null;
	}

	@Override
	public void createPanel(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));

		Label raceLabel = new Label(composite, SWT.NONE);
		raceLabel.setText(Strings.getString("race"));
		raceLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

		Combo raceCombo = new Combo(composite, SWT.READ_ONLY);
		new Label(composite, SWT.NONE);

		raceCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		raceCombo.add(Strings.getString("race.hume_male"));
		raceCombo.add(Strings.getString("race.hume_female"));
		raceCombo.add(Strings.getString("race.elvaan_male"));
		raceCombo.add(Strings.getString("race.elvaan_female"));
		raceCombo.add(Strings.getString("race.tarutaru_male"));
		raceCombo.add(Strings.getString("race.tarutaru_female"));
		raceCombo.add(Strings.getString("race.mithra"));
		raceCombo.add(Strings.getString("race.galka"));

		raceCombo.setVisibleItemCount(8);

		raceCombo.addListener(SWT.Selection, new RaceSelectionHandler());

		createField(composite, Strings.getString("slot.face"), PlayerCharacter.FACE);

		createGroupLabel(composite, Strings.getString("weapons"));

		createField(composite, Strings.getString("slot.main"), PlayerCharacter.MAIN);
		createField(composite, Strings.getString("slot.sub"), PlayerCharacter.SUB);
		createField(composite, Strings.getString("slot.ranged"), PlayerCharacter.RANGED);

		createGroupLabel(composite, Strings.getString("armor"));

		createField(composite, Strings.getString("slot.head"), PlayerCharacter.HEAD);
		createField(composite, Strings.getString("slot.body"), PlayerCharacter.BODY);
		createField(composite, Strings.getString("slot.hands"), PlayerCharacter.HANDS);
		createField(composite, Strings.getString("slot.legs"), PlayerCharacter.LEGS);
		createField(composite, Strings.getString("slot.feet"), PlayerCharacter.FEET);

		character.enable(PlayerCharacter.FACE, preferences.getBoolean("pc.slot.0.enabled", true));
		character.enable(PlayerCharacter.HEAD, preferences.getBoolean("pc.slot.1.enabled", true));
		character.enable(PlayerCharacter.BODY, preferences.getBoolean("pc.slot.2.enabled", true));
		character.enable(PlayerCharacter.HANDS, preferences.getBoolean("pc.slot.3.enabled", true));
		character.enable(PlayerCharacter.LEGS, preferences.getBoolean("pc.slot.4.enabled", true));
		character.enable(PlayerCharacter.FEET, preferences.getBoolean("pc.slot.5.enabled", true));
		character.enable(PlayerCharacter.MAIN, preferences.getBoolean("pc.slot.6.enabled", true));
		character.enable(PlayerCharacter.SUB, preferences.getBoolean("pc.slot.7.enabled", true));
		character.enable(PlayerCharacter.RANGED, preferences.getBoolean("pc.slot.8.enabled", true));

		character.set(PlayerCharacter.FACE, preferences.getInt("pc.slot.0", 0));
		character.set(PlayerCharacter.HEAD, preferences.getInt("pc.slot.1", 0));
		character.set(PlayerCharacter.BODY, preferences.getInt("pc.slot.2", 0));
		character.set(PlayerCharacter.HANDS, preferences.getInt("pc.slot.3", 0));
		character.set(PlayerCharacter.LEGS, preferences.getInt("pc.slot.4", 0));
		character.set(PlayerCharacter.FEET, preferences.getInt("pc.slot.5", 0));
		character.set(PlayerCharacter.MAIN, preferences.getInt("pc.slot.6", 0));
		character.set(PlayerCharacter.SUB, preferences.getInt("pc.slot.7", 0));
		character.set(PlayerCharacter.RANGED, preferences.getInt("pc.slot.8", 0));

		raceCombo.select(preferences.getInt("pc.race", 0));
		raceCombo.notifyListeners(SWT.Selection, new Event());
	}

	private void createField(Composite parent, String fieldLabel, int slot) {
		Field field = new Field(parent, fieldLabel, slot);

		Control label = field.getLabel();
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

		Control combo = field.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Control check = field.getCheck();
		check.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		character.setField(slot, field);

		field.addStateListener(new SlotListener(field));
	}

	private void createGroupLabel(Composite parent, String text) {
		Composite com = new Composite(parent, SWT.NONE);

		Label label = new Label(com, SWT.NONE);
		label.setText(text);

		Label separator = new Label(com, SWT.SEPARATOR | SWT.HORIZONTAL);

		GridLayout layout = new GridLayout(2, false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.marginTop = 11;
		com.setLayout(layout);

		separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		com.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
	}

	private class RaceSelectionHandler implements Listener {
		@Override
		public void handleEvent(Event e) {
			Combo widget = (Combo) e.widget;
			int race = widget.getSelectionIndex();
			character.setRace(race);
			preferences.setInt("pc.race", race);
		}
	}

	private class SlotListener implements StateListener {
		private Field field;

		public SlotListener(Field field) {
			this.field = field;
		}

		@Override
		public void stateChanged(StateEvent e) {
			int selection = field.getSelection();
			String key = "pc.slot." + field.getSlot();
			if (selection >= 0)
				preferences.setInt(key, selection);
			preferences.setBoolean(key + ".enabled", field.isEnabled());
		}
	}
}
