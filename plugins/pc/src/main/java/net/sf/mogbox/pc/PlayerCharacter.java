/* 
 * PlayerCharacter.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.DataFile;
import net.sf.mogbox.pol.ffxi.loader.DataBlock;
import net.sf.mogbox.pol.ffxi.loader.DataBlockLoader;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.renderer.FFXIModel;
import net.sf.mogbox.renderer.FFXIMotion;
import net.sf.mogbox.renderer.FFXISkeleton;
import net.sf.mogbox.renderer.FFXITexture;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.SkeletonNode;
import net.sf.mogbox.renderer.engine.scene.SkinNode;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

public class PlayerCharacter extends Node {
	private static Logger log = Logger.getLogger(PlayerCharacter.class.getName());

	public static final int FACE = 0;
	public static final int HEAD = 1;
	public static final int BODY = 2;
	public static final int HANDS = 3;
	public static final int LEGS = 4;
	public static final int FEET = 5;
	public static final int MAIN = 6;
	public static final int SUB = 7;
	public static final int RANGED = 8;

	private static final int[] MAX = { 32, 256, 256, 256, 256, 256, 512, 512, 256, 512 };

	private static final int[] SKELETONS = { 0x1BA0, 0x2808, 0x3470, 0x40D8, 0x4D40, 0x4D40, 0x5A88, 0x66F0 };

	private static final int[][] OFFSETS = {
			{ 0x1BA8, 0x1BC8, 0x1CC8, 0x1DC8, 0x1EC8, 0x1FC8, 0x20C8, 0xA0EF, 0x24C8, 0x22C8 },
			{ 0x2810, 0x2830, 0x2930, 0x2A30, 0x2B30, 0x2C30, 0x2D30, 0xA5EF, 0x3130, 0x2F30 },
			{ 0x3478, 0x3498, 0x3598, 0x3698, 0x3798, 0x3898, 0x3998, 0xAAEF, 0x3D98, 0x3B98 },
			{ 0x40E0, 0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0xAFEF, 0x4A00, 0x4800 },
			{ 0x4D48, 0x4D68, 0x4E68, 0x4F68, 0x5068, 0x5168, 0x5268, 0xB4EF, 0x5668, 0x5468 },
			{ 0x59A8, 0x4D68, 0x4E68, 0x4F68, 0x5068, 0x5168, 0x5268, 0xB4EF, 0x5668, 0x5468 },
			{ 0x5A90, 0x5AB0, 0x5BB0, 0x5CB0, 0x5DB0, 0x5EB0, 0x5FB0, 0xB9EF, 0x63B0, 0x66D8 },
			{ 0x66F8, 0x6718, 0x6818, 0x6918, 0x6A18, 0x6B18, 0x6C18, 0xBEEF, 0x7018, 0x6E18 } };

	private static final int[][] OFFSETS2 = { { 0, 0x0F75B, 0x0F79B, 0x0F7DB, 0x0F81B, 0x0F85B, 0x0F89B, 0x0F8DB },
			{ 0, 0x0F91B, 0x0F95B, 0x0F99B, 0x0F9DB, 0x0FA1B, 0x0FA5B, 0x0FA9B },
			{ 0, 0x0FADB, 0x0FB1B, 0x0FB5B, 0x0FB9B, 0x0FBDB, 0x0FC1B, 0x0FC5B },
			{ 0, 0x0FC9B, 0x0FCDB, 0x0FD1B, 0x0FD5B, 0x0FD9B, 0x0FDDB, 0x0FE1B },
			{ 0, 0x0FE5B, 0x0FE9B, 0x0FEDB, 0x0FF1B, 0x0FF5B, 0x0FF9B, 0x0FFDB },
			{ 0, 0x0FE5B, 0x0FE9B, 0x0FEDB, 0x0FF1B, 0x0FF5B, 0x0FF9B, 0x0FFDB },
			{ 0, 0x1001B, 0x1005B, 0x1009B, 0x100DB, 0x1011B, 0x1015B, 0x1019B },
			{ 0, 0x101DB, 0x1021B, 0x1025B, 0x1029B, 0x102DB, 0x1031B, 0x1035B } };

	private static final int[][] DUMMIES = {
			{ 0, 0x10DE7, 0x10E08, 0x10E35, 0x10E55, 0x10E75, 0x10E98, 0x10F9A, 0x10FD0 },
			{ 0, 0x1104F, 0x11070, 0x1109D, 0x110BD, 0x110DD, 0x11100, 0x11202, 0x11238 },
			{ 0, 0x112B4, 0x112D4, 0x11300, 0x1131F, 0x1133E, 0x11360, 0x11461, 0x11497 },
			{ 0, 0x11519, 0x11539, 0x11565, 0x11584, 0x115A3, 0x115C5, 0x116C6, 0x116FC },
			{ 0, 0x11772, 0x11792, 0x117BE, 0x117DD, 0x117FC, 0x1181E, 0x1191F, 0x11955 },
			{ 0, 0x11772, 0x11792, 0x117BE, 0x117DD, 0x117FC, 0x1181E, 0x1191F, 0x11955 },
			{ 0, 0x119EE, 0x11A0E, 0x11A3A, 0x11A59, 0x11A78, 0x11A9A, 0x11B9B, 0x11BD1 },
			{ 0, 0x11C50, 0x11C70, 0x11C9C, 0x11CBB, 0x11CDA, 0x11CFC, 0x11DFD, 0x11E33 } };

	private int race = -1;
	private int[] ids = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
	private int[] tmp = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

	private boolean changedRace = false;
	private boolean[] changed = { false, false, false, false, false, false, false, false, false, false };

	private SkeletonNode skeleton;
	private Node[] nodes = new Node[10];

	private Field[] fields = new Field[9];

	private StateListener listener = new ControlStateListener();

	public void setRace(final int race) {
		if (race < -1 || race > 7)
			throw new IndexOutOfBoundsException();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i] != null)
				fields[i].setRace(race);
		}
		applyOverrides(ids);
		this.race = race;
		changedRace = true;
	}

	public void set(int slot, int id) {
		if (id < 0 || id > getMax(slot))
			throw new IndexOutOfBoundsException();
		if (fields[slot] != null) {
			fields[slot].setSelection(id);
			id = fields[slot].getSelection();
		}

		for (int i = 0; i < ids.length; i++) {
			if (i == slot)
				tmp[i] = id;
			else
				tmp[i] = ids[i];
		}
		applyOverrides(tmp);
		for (int i = 0; i < ids.length; i++) {
			if (tmp[i] >= -1 && tmp[i] != ids[i]) {
				ids[i] = tmp[i];
				changed[i] = true;
			}
		}
	}

	public void enable(final int slot, final boolean enabled) {
		if (fields[slot] != null) {
			fields[slot].setEnabled(enabled);
		} else if (!enabled) {
			preUpdateExec(new Callable<Object>() {
				public Object call() {
					try {
						performSet(slot, -1);
					} catch (Throwable t) {
						log.log(Level.SEVERE, null, t);
					}
					return null;
				}
			});
		}
	}

	public void setField(final int slot, final Field field) {
		if (fields[slot] != null)
			fields[slot].removeStateListener(listener);
		if (field != null) {
			field.addStateListener(listener);
			field.setRace(race);
			field.setSelection(ids[slot]);
		}
		fields[slot] = field;
	}

	public static int getMax(int slot) {
		if (slot >= HEAD && slot <= FEET)
			return MAX[slot] + 63;
		else if (slot == MAIN)
			return MAX[slot] + 127;
		else
			return MAX[slot] - 1;
	}

	public static boolean isUsed(int race, int slot, int id) {
		int file = DataFile.lookupFileNumber(getDatID(race, slot, id));
		if (file < 0)
			return false;
		if (id == 0)
			return true;
		if (slot == MAIN && id <= 18)
			return true;
		if (slot >= HANDS && slot <= BODY && id == 1)
			return true;
		if (slot == FACE) {
			if (id < 16)
				return true;
			return file != DataFile.lookupFileNumber(getDatID(race, slot, id - 16));
		}
		return file != DUMMIES[race][slot];
	}

	@Override
	public void update(float delta) {
		try {
			if (changedRace) {
				changedRace = false;
				performSetRace(race);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		super.update(delta);
	}

	private void performSetRace(int race) throws IOException {
		if (skeleton != null) {
			remove(skeleton);
			skeleton = null;
		}

		if (race < 0)
			return;

		DataFile dat = new DataFile(SKELETONS[race]);
		log.log(Level.INFO, "[PC] " + dat);
		if (!dat.exists())
			return;

		SkeletonNode temp = null;

		FileChannel c = dat.getFileChannel();
		try {
			Loader<?> loader = Loader.newInstance(c);

			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					if (b.getType() == DataBlock.SKELETON) {
						temp = new FFXISkeleton(b.getData());
						break;
					}
				}
			}

			if (temp == null)
				return;

			c.position(0);

			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					switch (b.getType()) {
					case DataBlock.MOTION:
						if (temp != null) {
							int n = b.getID();
							StringBuilder name = new StringBuilder();
							name.append((char) (n & 0xFF)).append((char) (n >> 8 & 0xFF))
									.append((char) (n >> 16 & 0xFF));
							temp.addMotion(name.toString(), n >> 24 & 0xFF, new FFXIMotion(b.getData()));
						}
						break;
					}
				}
			}
		} finally {
			while (c.isOpen()) {
				try {
					c.close();
				} catch (IOException e) {
					log.log(Level.WARNING, null, e);
				}
			}
		}

		dat = new DataFile(SKELETONS[race] + 1);

		log.log(Level.INFO, "[PC] " + dat);

		c = dat.getFileChannel();
		try {
			Loader<?> loader = Loader.newInstance(c);

			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					switch (b.getType()) {
					case DataBlock.MOTION:
						if (temp != null) {
							int n = b.getID();
							StringBuilder name = new StringBuilder();
							name.append((char) (n & 0xFF)).append((char) (n >> 8 & 0xFF))
									.append((char) (n >> 16 & 0xFF));
							temp.addMotion(name.toString(), n >> 24 & 0xFF, new FFXIMotion(b.getData()));
						}
						break;
					}
				}
			}
		} finally {
			while (c.isOpen()) {
				try {
					c.close();
				} catch (IOException e) {
					log.log(Level.WARNING, null, e);
				}
			}
		}

		if (temp != null) {
			skeleton = temp;
			skeleton.setMotion("idl");
			add(skeleton);
		}

		for (int i = 0; i < 10; i++)
			performSet(i, ids[i]);
	}

	private void performSet(int slot, int id) throws IOException {
		if (race < 0)
			return;

		if (nodes[slot] != null) {
			skeleton.remove(nodes[slot]);
			nodes[slot] = null;
		}

		if (id != -1) {
			DataFile dat = new DataFile(getDatID(race, slot, id));
			nodes[slot] = loadModel(dat);
			if (nodes[slot] != null)
				skeleton.add(nodes[slot]);
		}
	}

	private static int getDatID(int race, int slot, int id) {
		if (id < 0 || id > getMax(slot))
			throw new IndexOutOfBoundsException();
		if (slot < HEAD || slot > MAIN) {
			return OFFSETS[race][slot] + id;
		} else {
			if (id < MAX[slot])
				return OFFSETS[race][slot] + id;
			else
				return OFFSETS2[race][slot] + id - MAX[slot];
		}
	}

	private Node loadModel(DataFile dat) throws IOException, FileNotFoundException {
		log.log(Level.INFO, "[PC] " + dat);
		if (!dat.exists())
			return null;
		SkinNode node = new SkinNode();
		FileChannel c = dat.getFileChannel();
		Loader<?> loader = Loader.newInstance(c);
		Deque<TextureState> environment = new LinkedList<TextureState>();
		Map<String, TextureState> textures = new HashMap<String, TextureState>();
		if (loader instanceof DataBlockLoader) {
			for (DataBlock b : (DataBlockLoader) loader) {
				try {
					switch (b.getType()) {
					case DataBlock.BEGIN_CONTAINER:
						environment.push(environment.peek());
						break;
					case DataBlock.TEXTURE:
						FFXITexture t = new FFXITexture(b.getData());
						textures.put(t.getName(), t);
						if (b.getID() == 0x76657665) {
							environment.pop();
							environment.push(t);
						}
						break;
					case DataBlock.MODEL:
						FFXIModel m = new FFXIModel(b.getData(), textures);
						m.setEnvironmentMap(environment.peek());
						node.add(m);
						break;
					case DataBlock.END_CONTAINER:
						environment.pop();
						break;
					}
				} catch (RuntimeException e) {
					e.printStackTrace();
				}
			}
		}
		while (c.isOpen()) {
			try {
				c.close();
			} catch (IOException e) {
				log.log(Level.WARNING, null, e);
			}
		}
		return node;
	}

	private void applyOverrides(int[] ids) {
		int[] overrides = new int[fields.length];
		for (int i = 0; i < fields.length; i++) {
			overrides[i] = -1;
		}
		for (int i = 0; i < fields.length; i++) {
			if (overrides[i] >= 0)
				continue;
			int[] o = fields[i].getOverrides(fields[i].getSelection());
			if (o == null)
				continue;
			for (int j = 0; j < o.length; j++) {
				overrides[j] = o[j];
			}
		}
		for (int i = 0; i < fields.length; i++) {
			fields[i].setOverride(overrides[i]);
			if (overrides[i] >= 0)
				ids[i] = overrides[i];
			else if (fields[i].isEnabled())
				ids[i] = fields[i].getSelection();
			else
				ids[i] = -1;
		}
	}

	private class ControlStateListener implements StateListener {
		@Override
		public void stateChanged(StateEvent e) {
			Field source = (Field) e.getSource();
			int slot = source.getSlot();
			final int[] tmp = new int[10];
			for (int i = 0; i < ids.length; i++) {
				if (i == slot)
					tmp[i] = source.getSelection();
				else
					tmp[i] = ids[i];
			}
			applyOverrides(tmp);

			preUpdateExec(new Callable<Object>() {
				public Object call() throws IOException {
					for (int i = 0; i < ids.length; i++) {
						if (ids[i] != tmp[i]) {
							ids[i] = tmp[i];
							performSet(i, tmp[i]);
						}
					}
					return null;
				}
			});
		}
	}
}
