/* 
 * Furniture.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.furniture;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.DataFile;
import net.sf.mogbox.pol.ffxi.loader.DataBlock;
import net.sf.mogbox.pol.ffxi.loader.DataBlockLoader;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.pol.ffxi.loader.UnsupportedFormatException;
import net.sf.mogbox.renderer.FFXIModel;
import net.sf.mogbox.renderer.FFXIMotion;
import net.sf.mogbox.renderer.FFXISkeleton;
import net.sf.mogbox.renderer.FFXITerrainModel;
import net.sf.mogbox.renderer.FFXITexture;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.SkeletonNode;
import net.sf.mogbox.renderer.engine.scene.SkinNode;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

public class Furniture extends Node {
	private static Logger log = Logger.getLogger(Furniture.class.getName());

	private int id = 0;
	private boolean changed = false;

	private Node furniture;
	private SkeletonNode skeleton;

	private static final int MAX = 255;
	private static final int[] OFFSETS = { 32928, 51439 };

	public void set(final int id) {
		this.id = id;
		changed = true;
	}

	public static int getMax() {
		return MAX * 2;
	}

	public static boolean isUsed(int id) {
		if (id > 2 * MAX)
			throw new IndexOutOfBoundsException();
		if (id >= MAX) {
			int file1 = DataFile.lookupFileNumber(getId(id - MAX));
			int file2 = DataFile.lookupFileNumber(getId(id));
			if (file1 == file2)
				return false;
		}
		DataFile dat = new DataFile(getId(id));
		if (!dat.exists())
			return false;
		FileChannel c = null;
		try {
			c = dat.getFileChannel();
			Loader<?> loader = null;
			try {
				loader = Loader.newInstance(c);
			} catch (UnsupportedFormatException e) {
				return false;
			}
			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					switch (b.getType()) {
					case DataBlock.TERRAIN_MODEL:
					case DataBlock.SKELETON:
						return true;
					}
				}
			}
		} catch (Throwable t) {
			log.log(Level.WARNING, null, t);
			return false;
		} finally {
			while (c != null && c.isOpen()) {
				try {
					c.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, null, e);
				}
			}
		}
		return false;
	}

	@Override
	public void update(float update) {
		if (changed) {
			changed = false;
			try {
				performSet(id);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		super.update(update);
	}

	private void performSet(int id) throws IOException {
		if (furniture != null)
			remove(furniture);
		if (skeleton != null)
			remove(skeleton);
		furniture = null;
		skeleton = null;

		if (id == -1)
			return;

		DataFile dat = new DataFile(getId(id));
		log.log(Level.INFO, "[FURNITURE] " + dat);
		if (!dat.exists())
			return;

		FileChannel c = dat.getFileChannel();
		Loader<?> loader = Loader.newInstance(c);

		boolean found = false;

		Node node = new Node();
		Map<String, TextureState> textures = new HashMap<String, TextureState>();
		if (loader instanceof DataBlockLoader) {
			for (DataBlock b : (DataBlockLoader) loader) {
				try {
					switch (b.getType()) {
					case DataBlock.TEXTURE:
						FFXITexture t = new FFXITexture(b.getData());
						textures.put(t.getName(), t);
						break;
					case DataBlock.TERRAIN_MODEL:
						found = true;
						node.add(new FFXITerrainModel(b.getData(), textures));
						break;
					}
				} catch (Throwable t) {
					log.log(Level.SEVERE, null, t);
				}
			}
		}

		if (found) {
			furniture = node;
			add(node);
		} else {
			c.position(0);

			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					try {
						if (b.getType() == DataBlock.SKELETON) {
							skeleton = new FFXISkeleton(b.getData());
							break;
						}
					} catch (Throwable t) {
						log.log(Level.SEVERE, null, t);
					}
				}
			}

			if (skeleton != null) {
				c.position(0);

				node = new SkinNode();
				if (loader instanceof DataBlockLoader) {
					for (DataBlock b : (DataBlockLoader) loader) {
						try {
							switch (b.getType()) {
							case DataBlock.MODEL:
								node.add(new FFXIModel(b.getData(), textures));
								break;
							case DataBlock.MOTION:
								int n = b.getID();
								StringBuilder name = new StringBuilder();
								name.append((char) (n & 0xFF)).append((char) (n >> 8 & 0xFF))
										.append((char) (n >> 16 & 0xFF));
								skeleton.addMotion(name.toString(), n >> 24 & 0xFF, new FFXIMotion(b.getData()));
								break;
							}
						} catch (Throwable t) {
							log.log(Level.SEVERE, null, t);
						}
					}
				}

				skeleton.add(node);
				skeleton.setMotion("idl");
				add(skeleton);
			}
		}

		while (c.isOpen()) {
			try {
				c.close();
			} catch (IOException e) {
				log.log(Level.SEVERE, null, e);
			}
		}
	}

	private static int getId(int id) {
		if (id < MAX)
			return id + OFFSETS[0];
		return id - MAX + OFFSETS[1];
	}
}
