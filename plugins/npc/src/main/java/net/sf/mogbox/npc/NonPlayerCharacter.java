/* 
 * NonPlayerCharacter.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.npc;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.DataFile;
import net.sf.mogbox.pol.ffxi.loader.DataBlock;
import net.sf.mogbox.pol.ffxi.loader.DataBlockLoader;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.renderer.FFXIModel;
import net.sf.mogbox.renderer.FFXIMotion;
import net.sf.mogbox.renderer.FFXISkeleton;
import net.sf.mogbox.renderer.FFXITexture;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.SkeletonNode;
import net.sf.mogbox.renderer.engine.scene.SkinNode;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

public class NonPlayerCharacter extends Node {
	private static Logger log = Logger.getLogger(NonPlayerCharacter.class.getName());

	private int id = -1;
	private boolean changed = false;

	private SkeletonNode skeleton;

	public void set(final int id) {
		if (this.id != id) {
			this.id = id;
			changed = true;
		}
	}

	public static int getMax() {
		return 2376;
	}

	public static boolean isUsed(int id) {
		if (id < 0 || id > getMax())
			throw new IndexOutOfBoundsException();
		id = getId(id);
		DataFile dat = new DataFile(id);
		if (!dat.exists())
			return false;
		FileChannel c = null;
		try {
			c = dat.getFileChannel();
			Loader<?> loader = Loader.newInstance(c);
			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					if (b.getType() == DataBlock.SKELETON)
						return true;
				}
			}
		} catch (Throwable t) {
			log.log(Level.SEVERE, null, t);
		} finally {
			while (c != null && c.isOpen()) {
				try {
					c.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, null, e);
				}
			}
		}
		return false;
	}

	public static int getExpansion(int id) {
		if (id < 0 || id > getMax())
			throw new IndexOutOfBoundsException();
		return DataFile.lookupFileNumber(getId(id)) >>> 16;
	}

	private static int getId(int id) {
		if (id < 0)
			throw new IndexOutOfBoundsException();
		if (id < 1500)
			return id + 1300;
		return id + 50295;
	}

	@Override
	public void update(float delta) {
		if (changed) {
			changed = false;
			try {
				performSet(id);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		super.update(delta);
	}

	private void performSet(int id) throws IOException {
		if (skeleton != null)
			remove(skeleton);
		skeleton = null;

		if (id == -1)
			return;

		DataFile dat = new DataFile(getId(id));
		log.log(Level.INFO, "[NPC] " + dat);
		if (!dat.exists())
			return;

		FileChannel c = dat.getFileChannel();
		Loader<?> loader = Loader.newInstance(c);

		if (loader instanceof DataBlockLoader) {
			for (DataBlock b : (DataBlockLoader) loader) {
				try {
					if (b.getType() == DataBlock.SKELETON) {
						skeleton = new FFXISkeleton(b.getData());
						break;
					}
				} catch (Throwable t) {
					log.log(Level.SEVERE, null, t);
				}
			}
		}

		if (skeleton != null) {
			c.position(0);

			SkinNode node = new SkinNode();
			Map<String, TextureState> textures = new HashMap<String, TextureState>();
			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					try {
						switch (b.getType()) {
						case DataBlock.TEXTURE:
							FFXITexture t = new FFXITexture(b.getData());
							textures.put(t.getName(), t);
							break;
						case DataBlock.MODEL:
							node.add(new FFXIModel(b.getData(), textures));
							break;
						case DataBlock.MOTION:
							int n = b.getID();
							StringBuilder name = new StringBuilder();
							name.append((char) (n & 0xFF)).append((char) (n >> 8 & 0xFF))
									.append((char) (n >> 16 & 0xFF));
							skeleton.addMotion(name.toString(), n >> 24 & 0xFF, new FFXIMotion(b.getData()));
							break;
						}
					} catch (Throwable t) {
						log.log(Level.SEVERE, null, t);
					}
				}
			}

			skeleton.add(node);
			skeleton.setMotion("idl");
			add(skeleton);
		}
		while (c.isOpen()) {
			try {
				c.close();
			} catch (IOException e) {
				log.log(Level.SEVERE, null, e);
			}
		}
	}
}
