package net.sf.mogbox.npc;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.sf.mogbox.pol.ffxi.DataFile;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.pol.ffxi.loader.StringLoader;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class GathererTool {
	private static Map<String, Set<String>> values = new TreeMap<String, Set<String>>();
	private static Set<Integer> ids2 = new TreeSet<Integer>();
	private static Map<Integer, String> ids3 = new HashMap<Integer, String>();

	private static Map<Integer, String> pool = new TreeMap<Integer, String>();

	public static void main(String[] args) {
		try {
			InputStream in;

			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);

			in = new FileInputStream("npc_data.xml");
			try {
				SAXParser parser = factory.newSAXParser();
				parser.parse(in, new NPCContentHandeler());
			} catch (Throwable t) {
				RuntimeException exception = new RuntimeException(t.getMessage(), t.getCause());
				exception.setStackTrace(t.getStackTrace());
				throw exception;
			}

			in = new FileInputStream("pool_mobs2.xml");
			try {
				SAXParser parser = factory.newSAXParser();
				parser.parse(in, new PoolContentHandeler());
			} catch (Throwable t) {
				RuntimeException exception = new RuntimeException(t.getMessage(), t.getCause());
				exception.setStackTrace(t.getStackTrace());
				throw exception;
			}

			in = new FileInputStream("pool_mobids.xml");
			try {
				SAXParser parser = factory.newSAXParser();
				parser.parse(in, new MobContentHandeler());
			} catch (Throwable t) {
				RuntimeException exception = new RuntimeException(t.getMessage(), t.getCause());
				exception.setStackTrace(t.getStackTrace());
				throw exception;
			}

			BufferedWriter out = new BufferedWriter(new FileWriter("npcs.txt"));
			for (String name : values.keySet()) {
				out.write(String.format("%-28s", name));
				Set<String> data = values.get(name);
				for (String d : data) {
					out.write(d + ' ');
				}
				out.write("\n");
			}
			out.close();

			int last = 0;
			out = new BufferedWriter(new FileWriter("ids.txt"));
			for (int id2 : ids2) {
				if (id2 - last > 1)
					out.write("\n");
				last = id2;
				String id3 = ids3.get(id2);
				String names = "";
				if (id2 < 1000)
					names = "\t";
				for (Entry<String, Set<String>> e : values.entrySet()) {
					for (String d : e.getValue()) {
						if (d.equals(id3)) {
							names += "\t" + e.getKey() + "\n\t";
						}
					}
				}
				out.write(String.format("%d%s", id2, names));
				out.write("\n");
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done " + values.size());
	}

	private static class NPCContentHandeler extends DefaultHandler {
		StringBuilder buffer;

		int currentZone = -1;
		List<String> names;

		int id = 0;
		String look = null;

		public void startDocument() throws SAXException {
		}

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			buffer = new StringBuilder();
		}

		@Override
		public void endElement(String ns, String name, String qName) throws SAXException {
			if ("row".equals(name)) {
				int zone = id >> 12 & 0xFF;
				id &= 0xFFF;
				if (id <= 0)
					return;
				if (zone != currentZone) {
					currentZone = zone;
					names = new LinkedList<String>();
					DataFile file = new DataFile(6720 + zone);
					FileChannel c = null;
					try {
						c = file.getFileChannel();
						Loader<?> loader = Loader.newInstance(c);
						for (String[] n : (StringLoader) loader) {
							names.add(n[0]);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} finally {
						if (c != null) {
							try {
								c.close();
							} catch (Throwable t) {
							}
						}
					}
				}
				name = names.get(id);
				if (!name.isEmpty()) {
					Set<String> d = values.get(name);
					if (d == null)
						values.put(name, d = new TreeSet<String>());
					d.add(look);

					int type = Integer.reverseBytes(Integer.parseInt(look.substring(0, 4), 10)) >>> 16;
					if (type == 0) {
						String id3_1 = look.substring(4, 8);
						String id3_2 = look.substring(8, 12);
						int id2 = Integer.reverseBytes(Integer.parseInt(id3_1, 16)) >>> 16
								| Integer.reverseBytes(Integer.parseInt(id3_2, 16));
						if (ids2.add(id2))
							ids3.put(id2, look);
					}
				}
			} else if ("id".equals(name)) {
				id = Integer.parseInt(buffer.toString(), 10);
			} else if ("look".equals(name)) {
				look = buffer.toString();
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			buffer.append(ch, start, length);
		}

		@Override
		public void endDocument() throws SAXException {
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void warning(SAXParseException e) throws SAXException {
			e.printStackTrace();
		}
	}

	private static class PoolContentHandeler extends DefaultHandler {
		StringBuilder buffer;

		int id = 0;
		String look = null;

		public void startDocument() throws SAXException {
		}

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			buffer = new StringBuilder();
		}

		@Override
		public void endElement(String ns, String name, String qName) throws SAXException {
			if ("row".equals(name)) {
				pool.put(id, look);
			} else if ("poolid".equals(name)) {
				id = Integer.parseInt(buffer.toString(), 10);
			} else if ("look".equals(name)) {
				look = buffer.toString();
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			buffer.append(ch, start, length);
		}

		@Override
		public void endDocument() throws SAXException {
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void warning(SAXParseException e) throws SAXException {
			e.printStackTrace();
		}
	}

	private static class MobContentHandeler extends DefaultHandler {
		StringBuilder buffer;

		int currentZone = -1;
		List<String> names;

		int id = 0;
		int poolid = 0;

		public void startDocument() throws SAXException {
		}

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			buffer = new StringBuilder();
		}

		@Override
		public void endElement(String ns, String name, String qName) throws SAXException {
			if ("row".equals(name)) {
				int zone = id >> 12 & 0xFF;
				id &= 0xFFF;
				if (id <= 0)
					return;
				if (zone != currentZone) {
					currentZone = zone;
					names = new LinkedList<String>();
					DataFile file = new DataFile(6720 + zone);
					FileChannel c = null;
					try {
						c = file.getFileChannel();
						Loader<?> loader = Loader.newInstance(c);
						for (String[] n : (StringLoader) loader) {
							names.add(n[0]);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} finally {
						if (c != null) {
							try {
								c.close();
							} catch (Throwable t) {
							}
						}
					}
				}
				name = names.get(id);
				String look = pool.get(poolid);
				if (!name.isEmpty()) {
					Set<String> d = values.get(name);
					if (d == null)
						values.put(name, d = new TreeSet<String>());
					d.add(look);

					int type = Integer.reverseBytes(Integer.parseInt(look.substring(0, 4), 10)) >>> 16;
					if (type == 0) {
						String id3_1 = look.substring(4, 8);
						String id3_2 = look.substring(8, 12);
						int id2 = Integer.reverseBytes(Integer.parseInt(id3_1, 16)) >>> 16
								| Integer.reverseBytes(Integer.parseInt(id3_2, 16));
						if (ids2.add(id2))
							ids3.put(id2, look);
					}
				}
			} else if ("id".equals(name)) {
				id = Integer.parseInt(buffer.toString(), 10);
			} else if ("look".equals(name)) {
				poolid = Integer.parseInt(buffer.toString(), 10);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			buffer.append(ch, start, length);
		}

		@Override
		public void endDocument() throws SAXException {
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void warning(SAXParseException e) throws SAXException {
			e.printStackTrace();
		}
	}
}
