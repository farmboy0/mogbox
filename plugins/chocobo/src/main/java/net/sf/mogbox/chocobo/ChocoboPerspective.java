/* 
 * ChocoboPerspective.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.chocobo;

import java.net.URL;

import net.sf.mogbox.preferences.Preferences;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.swt.Perspective;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

public class ChocoboPerspective implements Perspective {
	private Chocobo chocobo;
	private Preferences preferences;

	public ChocoboPerspective() {
		chocobo = new Chocobo();
		preferences = Preferences.getInstance(this);
	}

	@Override
	public Node getNode() {
		return chocobo;
	}

	@Override
	public String getName() {
		return "mogbox.chocobo";
	}

	@Override
	public String getDisplayName() {
		return Strings.getString("perspective.name");
	}

	@Override
	public URL getIconLocation() {
		return null;
	}

	@Override
	public void createPanel(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));

		Label raceLabel = new Label(composite, SWT.NONE);
		raceLabel.setText(Strings.getString("breed"));
		raceLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

		Combo breedCombo = new Combo(composite, SWT.READ_ONLY);
		breedCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(composite, SWT.NONE);

		breedCombo.add(Strings.getString("breed.rounsey"));
		breedCombo.add(Strings.getString("breed.destrier"));
		breedCombo.add(Strings.getString("breed.palfrey"));
		breedCombo.add(Strings.getString("breed.courser"));
		breedCombo.add(Strings.getString("breed.jennet"));

		breedCombo.addListener(SWT.Selection, new BreedSelectionHandler());

		createGroupLabel(composite, Strings.getString("basics"));

		createField(composite, Strings.getString("slot.body"), Chocobo.BODY);
		createField(composite, Strings.getString("slot.feet"), Chocobo.FEET);
		createField(composite, Strings.getString("slot.head"), Chocobo.HEAD);
		createField(composite, Strings.getString("slot.tail"), Chocobo.TAIL);

		createGroupLabel(composite, Strings.getString("extras"));

		createField(composite, Strings.getString("slot.armor"), Chocobo.ARMOR);

		chocobo.enable(Chocobo.BODY, preferences.getBoolean("chocobo.slot.0.enabled", true));
		chocobo.enable(Chocobo.FEET, preferences.getBoolean("chocobo.slot.1.enabled", true));
		chocobo.enable(Chocobo.HEAD, preferences.getBoolean("chocobo.slot.2.enabled", true));
		chocobo.enable(Chocobo.TAIL, preferences.getBoolean("chocobo.slot.3.enabled", true));
		chocobo.enable(Chocobo.ARMOR, preferences.getBoolean("chocobo.slot.4.enabled", true));

		chocobo.set(Chocobo.BODY, preferences.getInt("chocobo.slot.0", 0));
		chocobo.set(Chocobo.FEET, preferences.getInt("chocobo.slot.1", 0));
		chocobo.set(Chocobo.HEAD, preferences.getInt("chocobo.slot.2", 0));
		chocobo.set(Chocobo.TAIL, preferences.getInt("chocobo.slot.3", 0));
		chocobo.set(Chocobo.ARMOR, preferences.getInt("chocobo.slot.4", 0));

		breedCombo.select(preferences.getInt("chocobo.breed", 0));
		breedCombo.notifyListeners(SWT.Selection, new Event());
	}

	private void createField(Composite parent, String fieldLabel, int slot) {
		Field field = new Field(parent, fieldLabel, slot);

		Control label = field.getLabel();
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

		Control combo = field.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Control check = field.getCheck();
		check.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		chocobo.setField(slot, field);

		field.addStateListener(new SlotListener(field));
	}

	private void createGroupLabel(Composite parent, String text) {
		Composite com = new Composite(parent, SWT.NONE);

		Label label = new Label(com, SWT.NONE);
		label.setText(text);

		Label separator = new Label(com, SWT.SEPARATOR | SWT.HORIZONTAL);

		GridLayout layout = new GridLayout(2, false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.marginTop = 11;
		com.setLayout(layout);

		separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		com.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
	}

	private class BreedSelectionHandler implements Listener {
		@Override
		public void handleEvent(Event e) {
			Combo widget = (Combo) e.widget;
			int breed = widget.getSelectionIndex();
			chocobo.setBreed(breed);
			preferences.setInt("chocobo.breed", breed);
		}
	}

	private class SlotListener implements StateListener {
		private Field field;

		public SlotListener(Field field) {
			this.field = field;
		}

		@Override
		public void stateChanged(StateEvent e) {
			int selection = field.getSelection();
			String key = "chocobo.slot." + field.getSlot();
			if (selection >= 0)
				preferences.setInt(key, selection);
			preferences.setBoolean(key + ".enabled", field.isEnabled());
		}
	}
}
