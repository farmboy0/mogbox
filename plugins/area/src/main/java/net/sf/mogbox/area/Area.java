/* 
 * Area.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.area;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.DataFile;
import net.sf.mogbox.pol.ffxi.loader.DataBlock;
import net.sf.mogbox.pol.ffxi.loader.DataBlockLoader;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.renderer.FFXITerainLayout;
import net.sf.mogbox.renderer.FFXITerrainModel;
import net.sf.mogbox.renderer.FFXITexture;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

public class Area extends Node {
	private static Logger log = Logger.getLogger(Area.class.getName());

	private int id = 0;
	private boolean changed = false;

	private Node layout;

	private static final int MAX = 255;
	private static final int OFFSET = 100;

	public void set(final int id) {
		this.id = id;
		changed = true;
	}

	public static int getMax() {
		return MAX;
	}

	public static boolean isUsed(int id) {
		if (id == 15)
			return true;
		return DataFile.lookupFileNumber(getId(id)) != 0x10028;
	}

	public static int getExpansion(int id) {
		if (id < 0 || id > getMax())
			throw new IndexOutOfBoundsException();
		return DataFile.lookupFileNumber(getId(id)) >>> 16;
	}

	@Override
	public void update(float delta) {
		if (changed) {
			changed = false;
			try {
				performSet(id);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		super.update(delta);
	}

	private void performSet(int id) throws IOException {
		if (layout != null)
			remove(layout);
		layout = null;

		DataFile dat = new DataFile(getId(id));
		log.log(Level.INFO, "[AREA] " + dat);
		if (!dat.exists())
			return;

		Node temp = null;

		FileChannel c = dat.getFileChannel();
		try {
			Loader<?> loader = Loader.newInstance(c);

			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					if (b.getType() == DataBlock.TERRAIN_LAYOUT) {
						temp = new FFXITerainLayout(b.getData());
						break;
					}
				}
			}

			if (temp == null)
				return;

			c.position(0);

			Map<String, TextureState> textures = new HashMap<String, TextureState>();
			if (loader instanceof DataBlockLoader) {
				for (DataBlock b : (DataBlockLoader) loader) {
					try {
						switch (b.getType()) {
						case DataBlock.TEXTURE:
							FFXITexture t = new FFXITexture(b.getData());
							textures.put(t.getName(), t);
							break;
						case DataBlock.TERRAIN_MODEL:
							temp.add(new FFXITerrainModel(b.getData(), textures));
							break;
						}
					} catch (Throwable t) {
						log.log(Level.SEVERE, null, t);
					}
				}
			}
		} finally {
			while (c.isOpen()) {
				try {
					c.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, null, e);
				}
			}
		}

		if (temp != null) {
			layout = temp;
			add(layout);
		}
	}

	private static int getId(int id) {
		return OFFSET + id;
	}
}
