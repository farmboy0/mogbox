/* 
 * FileDumpMemoryHandler.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.logging;

import java.io.UnsupportedEncodingException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.ErrorManager;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

public class FileDumpMemoryHandler extends Handler {
	private Level pushLevel;
	private String pattern;

	private Queue<LogRecord> vitalBuffer = new ConcurrentLinkedQueue<LogRecord>();

	private LogRecord[] buffer;
	private int head;

	private Handler handler = null;

	public FileDumpMemoryHandler(String pattern, int size, Level pushLevel) {
		String level = LogManager.getLogManager().getProperty("mogbox.logging.FileDumpMemoryHandler.level");
		setLevel(level == null ? Level.ALL : Level.parse(level));
		if (pattern == null || pushLevel == null)
			throw new NullPointerException();
		if (pattern.isEmpty())
			throw new IllegalArgumentException();

		this.pushLevel = pushLevel;
		this.pattern = pattern;

		buffer = new LogRecord[size];
		head = 0;
	}

	@Override
	public synchronized void close() throws SecurityException {
		flush();
		buffer = null;
		if (handler != null) {
			handler.close();
			handler = null;
		}
	}

	@Override
	public void flush() {
		if (handler != null)
			handler.flush();
	}

	@Override
	public void setFormatter(Formatter newFormatter) throws SecurityException {
		super.setFormatter(newFormatter);
		if (handler != null)
			handler.setFormatter(newFormatter);
	}

	@Override
	public void setErrorManager(ErrorManager em) {
		super.setErrorManager(em);
		if (handler != null)
			handler.setErrorManager(em);
	}

	@Override
	public void setEncoding(String encoding) throws SecurityException, UnsupportedEncodingException {
		super.setEncoding(encoding);
		if (handler != null)
			handler.setEncoding(encoding);
	}

	public synchronized void push() {
		if (buffer == null)
			return;
		try {
			if (handler == null) {
				handler = new FileHandler(pattern, 0, 10, false);
				handler.setEncoding(getEncoding());
				handler.setFormatter(getFormatter());
				handler.setLevel(Level.ALL);
				handler.setErrorManager(getErrorManager());
				handler.setFilter(null);
			}

			LogRecord record;
			while (!vitalBuffer.isEmpty()) {
				record = vitalBuffer.poll();
				if (record != null)
					handler.publish(record);
			}

			record = new LogRecord(Level.INFO, "========== Error Dump ==========");
			handler.publish(record);
			for (int i = 0; i < buffer.length; i++) {
				int n = (i + head) % buffer.length;
				record = buffer[n];
				buffer[n] = null;
				if (record != null)
					handler.publish(record);
			}
			record = new LogRecord(Level.INFO, "================================");
			handler.publish(record);
		} catch (Exception e) {
			getErrorManager().error("Unable to open output stream", e, ErrorManager.OPEN_FAILURE);
		}
	}

	@Override
	public synchronized void publish(LogRecord record) {
		if (buffer == null || !isLoggable(record))
			return;
		if (record.getLoggerName() == "") {
			if (!vitalBuffer.offer(record)) {
				getErrorManager().error("Unable to add record to buffer", null, ErrorManager.GENERIC_FAILURE);
			}
		} else {
			buffer[head] = record;
			head = (head + 1) % buffer.length;
		}
		Level level = record.getLevel();
		if (level.intValue() >= pushLevel.intValue())
			push();
	}
}
