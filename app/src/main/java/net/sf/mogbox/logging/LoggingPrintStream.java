/* 
 * LoggingPrintStream.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.logging;

import java.io.FilterOutputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingPrintStream extends PrintStream {
	private StringBuilder buffer = new StringBuilder();

	private Logger log;
	private Level level;

	public LoggingPrintStream(Logger log, Level level) {
		super(new FilterOutputStream(null), false);

		this.log = log;
		this.level = level;
	}

	@Override
	public PrintStream append(char c) {
		synchronized (buffer) {
			buffer.append(c);
			flush();
			return this;
		}
	}

	@Override
	public PrintStream append(CharSequence csq) {
		synchronized (buffer) {
			buffer.append(csq);
			flush();
			return this;
		}
	}

	@Override
	public PrintStream append(CharSequence csq, int start, int end) {
		synchronized (buffer) {
			buffer.append(csq, start, end);
			flush();
			return this;
		}
	}

	@Override
	public void close() {
		synchronized (buffer) {
			buffer.append('\n');
			flush();
		}
	}

	public void flush() {
		synchronized (buffer) {
			int start = 0;
			int end = 0;
			while ((end = buffer.indexOf("\n", start)) >= 0) {
				String record = buffer.substring(start, end);
				if (record.length() > 0)
					log.log(level, record);
				start = end + 1;
			}
			buffer.delete(0, start);
		}
	}

	@Override
	public PrintStream format(Locale l, String format, Object... args) {
		synchronized (buffer) {
			buffer.append(String.format(l, format, args));
			flush();
			return this;
		}
	}

	@Override
	public PrintStream format(String format, Object... args) {
		synchronized (buffer) {
			buffer.append(String.format(format, args));
			flush();
			return this;
		}
	}

	@Override
	public void print(boolean b) {
		synchronized (buffer) {
			buffer.append(b);
			flush();
		}
	}

	@Override
	public void print(char c) {
		synchronized (buffer) {
			buffer.append(c);
			flush();
		}
	}

	@Override
	public void print(char[] s) {
		synchronized (buffer) {
			buffer.append(s);
			flush();
		}
	}

	@Override
	public void print(double d) {
		synchronized (buffer) {
			buffer.append(d);
			flush();
		}
	}

	@Override
	public void print(float f) {
		synchronized (buffer) {
			buffer.append(f);
			flush();
		}
	}

	@Override
	public void print(int i) {
		synchronized (buffer) {
			buffer.append(i);
			flush();
		}
	}

	@Override
	public void print(long l) {
		synchronized (buffer) {
			buffer.append(l);
			flush();
		}
	}

	@Override
	public void print(Object obj) {
		synchronized (buffer) {
			buffer.append(obj);
			flush();
		}
	}

	@Override
	public void print(String s) {
		synchronized (buffer) {
			buffer.append(s);
			flush();
		}
	}

	@Override
	public PrintStream printf(Locale l, String format, Object... args) {
		synchronized (buffer) {
			buffer.append(String.format(format, args));
			flush();
			return this;
		}
	}

	@Override
	public PrintStream printf(String format, Object... args) {
		synchronized (buffer) {
			buffer.append(String.format(format, args));
			flush();
			return this;
		}
	}

	@Override
	public void println() {
		synchronized (buffer) {
			buffer.append('\n');
			flush();
		}
	}

	@Override
	public void println(boolean x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(char x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(char[] x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(double x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(float x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(int x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(long x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(Object x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void println(String x) {
		synchronized (buffer) {
			buffer.append(x).append('\n');
			flush();
		}
	}

	@Override
	public void write(byte[] buf, int off, int len) {
	}

	@Override
	public void write(int b) {
	}
}
