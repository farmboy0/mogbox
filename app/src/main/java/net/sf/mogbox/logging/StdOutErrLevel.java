/* 
 * StdOutErrLevel.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.logging;

import java.util.logging.Level;

public class StdOutErrLevel extends Level {
	/**
	 * Private constructor
	 */
	private StdOutErrLevel(String name, int value) {
		super(name, value);
	}

	/**
	 * Level for STDOUT activity.
	 */
	public static Level STDOUT = new StdOutErrLevel("STDOUT", 900);
	/**
	 * Level for STDERR activity
	 */
	public static Level STDERR = new StdOutErrLevel("STDERR", 900);
}
