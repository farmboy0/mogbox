/* 
 * LogFormatter.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.logging;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS z", Locale.US);

	public LogFormatter() {
		dateFormat.setTimeZone(TimeZone.getTimeZone("Universal"));
	}

	@Override
	public String format(LogRecord record) {
		StringBuilder b = new StringBuilder();
		b.append("[ ").append(dateFormat.format(record.getMillis())).append(" ] ");
		b.append(record.getLevel().getName());
		String s = record.getMessage();
		if (s != null)
			b.append(": ").append(s);
		b.append(System.getProperty("line.separator"));
		Throwable t = record.getThrown();
		if (t != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.close();
			try {
				sw.close();
			} catch (IOException e) {
			}
			b.append(sw);
		}
		return b.toString();
	}
}