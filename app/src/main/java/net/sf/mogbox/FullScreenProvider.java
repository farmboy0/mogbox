package net.sf.mogbox;

public interface FullScreenProvider {

	boolean isFullScreen();

	void setFullScreen(boolean fullScreen);
}
