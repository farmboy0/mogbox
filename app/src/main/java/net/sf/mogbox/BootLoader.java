/* 
 * BootLoader.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.logging.FileDumpMemoryHandler;
import net.sf.mogbox.logging.LogFormatter;
import net.sf.mogbox.logging.LoggingPrintStream;
import net.sf.mogbox.logging.StdOutErrLevel;
import net.sf.mogbox.os.OS;
import net.sf.mogbox.plugin.ExtensionPoint;
import net.sf.mogbox.plugin.Plugin;
import net.sf.mogbox.plugin.PluginEngine;
import net.sf.mogbox.plugin.PluginVersion;
import net.sf.mogbox.plugin.manifest.PluginManifest;
import net.sf.mogbox.pol.Region;
import net.sf.mogbox.preferences.Preferences;
import net.sf.mogbox.swt.SWTErrorHandler;
import net.sf.mogbox.swt.SWTGui;

public class BootLoader {
	private static final String FFXI_LOCATION = "ffxi.location";
	private static final String FFXI_REGION = "ffxi.region";

	private static final Logger log = Logger.getLogger(BootLoader.class.getName());
	private static final Preferences PREFERENCES = Preferences.getRootInstance();

	private static Map<String, String> arguments = new HashMap<>();

	public static void main(String[] args) {
		try {
			processArguments(args);
			setupLogger();

			log.info(() -> "Java Version " + System.getProperty("java.version"));
			log.info(() -> System.getProperty("java.runtime.name"));
			log.info(() -> System.getProperty("java.vm.name"));
			log.info(() -> String.format("Operating System: %s (%s)", System.getProperty("os.name"),
					System.getProperty("os.version")));

			SWTGui ui = initializeUI();
			if (!getBoolean("no-splash", false)) {
				ui.start(true);
			}

			initializeLocale();

			log.info("Language: " + Locale.getDefault().getDisplayName());

			initializeRegion(ui);

			Plugin plugin = initializePluginEngine();

			ui.showMogbox(plugin);
		} catch (Exception e) {
			log.log(Level.SEVERE, "Intialization Error", e);
			SWTErrorHandler.handle(e);
			System.exit(-1);
		}
	}

	private static void processArguments(String[] args) {
		StringBuilder sb = new StringBuilder();
		String last = null;
		for (String a : args) {
			if (a.charAt(0) == '-') {
				arguments.put(last, sb.toString());
				sb.setLength(0);
				last = a.substring(1);
			} else {
				if (sb.length() == 0)
					sb.append(' ');
				sb.append(a);
			}
		}
		arguments.put(last, sb.toString().trim());
	}

	private static void setupLogger() {
		Logger root = Logger.getLogger("");
		root.setLevel(Level.INFO);

		for (Handler h : root.getHandlers())
			root.removeHandler(h);

		Formatter formatter = new LogFormatter();

		boolean debug = getBoolean("debug", false);

		File location = OS.getUserSettingsLocation();
		if (!location.exists())
			location.mkdirs();
		String logs = location.getAbsolutePath();

		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.ALL);
		consoleHandler.setFormatter(formatter);
		root.addHandler(consoleHandler);

		Handler logHandler;
		try {
			if (debug) {
				logHandler = new FileHandler(logs + File.separator + "debug.%u.log", false);
			} else {
				logHandler = new FileDumpMemoryHandler(logs + File.separator + "dump.%g.%u.log", 100, Level.SEVERE);
			}
			logHandler.setFormatter(formatter);
			try {
				logHandler.setEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				root.log(Level.SEVERE, "Unable to set log encoding.", e);
			}
			root.addHandler(logHandler);
			Runtime.getRuntime().addShutdownHook(new Thread(logHandler::close, "Shutdown"));
		} catch (IOException e) {
			root.log(Level.SEVERE, "Unable to install log handler.", e);
		}

		System.setOut(new LoggingPrintStream(Logger.getLogger("stdout"), StdOutErrLevel.STDOUT));
		System.setErr(new LoggingPrintStream(Logger.getLogger("stderr"), StdOutErrLevel.STDERR));

		if (debug)
			root.info("Debug Mode Enabled");
	}

	private static SWTGui initializeUI() throws IOException {
		final URL[] iconURLs = new URL[] { //
				BootLoader.class.getResource("/icon16.png"), //
				BootLoader.class.getResource("/icon32.png"), //
				BootLoader.class.getResource("/icon48.png"), //
				BootLoader.class.getResource("/icon256.png") //
		};

		String location = getString("splash", null);
		URL splashURL = null;
		if (location != null) {
			File file = new File(location);
			if (file.exists() && file.isFile())
				splashURL = file.toURI().toURL();
		}
		if (splashURL == null) {
			splashURL = BootLoader.class.getResource("/splash.png");
		}
		return new SWTGui(splashURL, iconURLs);
	}

	private static void initializeLocale() {
		Locale locale = Locale.getDefault();
		String name = PREFERENCES.getString("locale", null);
		if (name != null) {
			for (Locale l : Locale.getAvailableLocales()) {
				if (name.equals(l.toString())) {
					locale = l;
					break;
				}
			}
		}
		if (locale.getLanguage().equals("ja"))
			locale = Locale.JAPANESE;
		else if (locale.getLanguage().equals("fr"))
			locale = Locale.FRENCH;
		else if (locale.getLanguage().equals("de"))
			locale = Locale.GERMAN;
		else if (locale.getCountry().equals("US"))
			locale = Locale.US;
		else
			locale = Locale.UK;
		Locale.setDefault(locale);
	}

	private static boolean initializeRegion(SWTGui ui) throws Exception {
		Region region = null;

		try {
			region = Region.valueOf(PREFERENCES.getString(FFXI_REGION, ""));
			if (!region.isFFXIInstalled())
				region = null;
		} catch (IllegalArgumentException e) {
		}

		if (region == null) {
			List<Region> installed = Region.getInstalledFFXIRegions();
			if (installed.size() == 1)
				region = installed.get(0);
			else if (installed.size() > 1) {
				region = ui.chooseRegion(installed).get();
			}
		}

		if (region == null) {
			region = Region.NONE;
			String path = PREFERENCES.getString(FFXI_LOCATION, null);
			if (path != null) {
				region.setFFXILocation(new File(path));
			}
			if (!region.isFFXIInstalled()) {
				path = ui.chooseFFXIPath().get();
				if (path != null)
					region.setFFXILocation(new File(path));
			}
		}

		Region.setDefaultFFXIRegion(region);

		if (region != Region.NONE) {
			PREFERENCES.setString(FFXI_REGION, region.toString());
			PREFERENCES.remove(FFXI_LOCATION);
		} else {
			File location = region.getFFXILocation();
			if (location != null)
				PREFERENCES.setString(FFXI_LOCATION, location.getAbsolutePath());
			PREFERENCES.remove(FFXI_REGION);
		}

		return region.isFFXIInstalled();
	}

	private static Plugin initializePluginEngine() {
		PluginEngine pluginEngine = new PluginEngine();
		pluginEngine.setResourceLocation(new File("res"));

		PluginManifest manifest = new PluginManifest();
		manifest.setName("mogbox");

		StringBuilder version = new StringBuilder();
		version.append(Strings.getString("version.major")).append('.');
		version.append(Strings.getString("version.minor")).append('.');
		version.append(Strings.getString("version.maintenance")).append(' ');
		version.append(Strings.getString("version.build"));
		manifest.setVersion(new PluginVersion(version.toString()));
		manifest.addExtensionPoint(new ExtensionPoint("perspective"));

		Plugin plugin = new Plugin(pluginEngine, manifest);

		pluginEngine.addPluginLocation(new File("plugins"));

		pluginEngine.start();

		return plugin;
	}

	public static String getString(String name, String defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			return value;
		}
		return defaultValue;
	}

	public static byte getByte(String name, byte defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Byte.parseByte(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	public static short getShort(String name, short defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Short.parseShort(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	public static int getInt(String name, int defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Integer.parseInt(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	public static long getLong(String name, long defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Long.parseLong(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	public static boolean getBoolean(String name, boolean defaultValue) {
		return arguments.containsKey(name);
	}

	public static float getFloat(String name, float defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Float.parseFloat(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	public static double getDouble(String name, double defaultValue) {
		String value = arguments.get(name);
		if (value != null) {
			try {
				return Double.parseDouble(value);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}
}
