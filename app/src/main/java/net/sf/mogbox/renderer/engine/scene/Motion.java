package net.sf.mogbox.renderer.engine.scene;

import net.sf.mogbox.renderer.engine.vecmath.Joint;

public interface Motion {
	public Joint[] apply(float t, Joint[] src, Joint[] dst);

	public float getDuration();
}
