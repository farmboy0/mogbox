/* 
 * Core.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine;

import static net.sf.mogbox.renderer.engine.OpenGLException.translateGLErrorString;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.GL_OUT_OF_MEMORY;
import static org.lwjgl.opengl.GL11.glGetError;

import java.util.logging.Logger;

import net.sf.mogbox.FullScreenProvider;
import net.sf.mogbox.input.JInputHandler;
import net.sf.mogbox.renderer.ModelViewerRenderer;
import net.sf.mogbox.renderer.engine.scene.Node;

public abstract class Core {
	protected boolean initialized = false;

	private static Logger log = Logger.getLogger(Core.class.getName());

	private final FullScreenProvider fullScreenProvider;

	private Renderer renderer;
	private JInputHandler input;

	private boolean resized = false;
	private int width = 0;
	private int height = 0;

	public Core(FullScreenProvider fullScreenProvider) {
		this.fullScreenProvider = fullScreenProvider;
		renderer = new ModelViewerRenderer();
		input = new JInputHandler(this, renderer.getCamera());
	}

	public void initialize() {
		renderer.initialize();
		input.initialize();
		checkError();
	}

	public boolean isFullscreen() {
		return fullScreenProvider.isFullScreen();
	}

	public void setFullscreen(final boolean fullscreen) {
		fullScreenProvider.setFullScreen(fullscreen);
	}

	public void reset() {
		renderer.reset();
	}

	public void changeNode(Node node) {
		renderer.displayNode(node);
	}

	public RenderImageData readImageData() {
		return renderer.readImageData();
	}

	public final void update(float delta) {
		input.update();
		renderer.update(delta);
	}

	public final void render(float delta) {
		makeCurrent();
		if (resized) {
			renderer.resize(width, height);
			resized = false;
		}
		renderer.render(delta);
		checkError();
		flipBuffers();
	}

	public final void dispose() {
		if (!initialized)
			return;
		makeCurrent();
		renderer.dispose();
		checkError();
		initialized = false;
	}

	private int checkError() {
		int error = glGetError();
		if (error != GL_NO_ERROR) {
			if (error == GL_OUT_OF_MEMORY)
				throw new OpenGLException("Out of memory");
			else
				log.warning(() -> "OpenGL: " + translateGLErrorString(error));
		}
		return error;
	}

	public abstract int getMaxSamples();

	public abstract void focus();

	public abstract boolean hasFocus();

	public abstract boolean isInBounds(int x, int y);

	public boolean isWireframeEnabled() {
		return renderer.isWireframeEnabled();
	}

	public void setWireframeEnabled(boolean enabled) {
		renderer.setWireframeEnabled(enabled);
	}

	public boolean isTransparencyEnabled() {
		return renderer.isTransparencyEnabled();
	}

	public void setTransparencyeEnabled(boolean enabled) {
		renderer.setTransparencyeEnabled(enabled);
	}

	protected void resize(int width, int height) {
		resized = true;
		this.width = width;
		this.height = height;
	}

	protected abstract void makeCurrent();

	protected abstract void flipBuffers();
}
