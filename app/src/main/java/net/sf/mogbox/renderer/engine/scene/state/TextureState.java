/* 
 * TextureState.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene.state;

import static org.lwjgl.opengl.ARBMultitexture.GL_TEXTURE0_ARB;
import static org.lwjgl.opengl.ARBMultitexture.glActiveTextureARB;
import static org.lwjgl.opengl.ARBTextureCompression.glCompressedTexImage2DARB;
import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
import static org.lwjgl.opengl.GL11.GL_EXTENSIONS;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_RGB;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.BufferUtils;

public class TextureState extends RenderState {
	private static Logger log = Logger.getLogger(TextureState.class.getName());

	protected static TextureState current = null;

	protected int w, h;
	protected int target = GL_TEXTURE_2D;
	protected int type;
	protected ByteBuffer data;

	private int id;
	private int unit = GL_TEXTURE0_ARB;

	@Override
	public int getType() {
		return TEXTURE;
	}

	@Override
	protected void initializeState() {
		IntBuffer intBuffer = BufferUtils.createIntBuffer(1);
		glGenTextures(intBuffer);
		id = intBuffer.get();
		glBindTexture(target, id);

		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		try {
			switch (type) {
			case GL_RGBA:
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
				break;
			case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
			case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
			case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
				if (glGetString(GL_EXTENSIONS).indexOf("GL_ARB_texture_compression") != -1)
					glCompressedTexImage2DARB(target, 0, type, w, h, data);
				break;
			default:
				ByteBuffer texture = BufferUtils.createByteBuffer(3);
				texture.put((byte) 0xFF).put((byte) 0x00).put((byte) 0xFF).clear();
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);
			}
		} catch (Throwable t) {
			log.log(Level.WARNING, "Failed to load texture.", t);
			intBuffer.clear();
			intBuffer.put(id).flip();
			id = 0;
			glDeleteTextures(intBuffer);
		}

		glBindTexture(target, 0);
	}

	public int getTextureType() {
		return type;
	}

	@Override
	protected void applyState() {
		glActiveTextureARB(unit);
		glBindTexture(target, id);
		glActiveTextureARB(GL_TEXTURE0_ARB);
	}

	protected void clearState() {
		glActiveTextureARB(unit);
		glBindTexture(target, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
	}

	@Override
	protected void disposeState() {
		IntBuffer t = IntBuffer.allocate(1);
		t.put(id);
		id = 0;
		glDeleteTextures(t);
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}

	public int getUnit() {
		return unit;
	}
}
