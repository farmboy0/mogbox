/* 
 * FFXITriangleMesh.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;

import java.nio.ByteBuffer;

public class FFXITriangleMesh extends FFXIMesh {
	public FFXITriangleMesh(FFXIModel model, ByteBuffer b) {
		super(model);
		int num = b.getShort() & 0xFFFF;
		vertices = new short[num * 3];
		uv = new float[num * 6];
		for (int i = 0; i < num; i++) {
			vertices[i * 3] = b.getShort();
			vertices[i * 3 + 1] = b.getShort();
			vertices[i * 3 + 2] = b.getShort();
			uv[i * 6] = b.getFloat();
			uv[i * 6 + 1] = b.getFloat();
			uv[i * 6 + 2] = b.getFloat();
			uv[i * 6 + 3] = b.getFloat();
			uv[i * 6 + 4] = b.getFloat();
			uv[i * 6 + 5] = b.getFloat();
		}
	}

	@Override
	protected void begin() {
		glBegin(GL_TRIANGLES);
	}

	@Override
	protected void end() {
		glEnd();
	}
}