/* 
 * MaterialState.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene.state;

import static org.lwjgl.opengl.GL11.GL_AMBIENT;
import static org.lwjgl.opengl.GL11.GL_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_SHININESS;
import static org.lwjgl.opengl.GL11.GL_SPECULAR;
import static org.lwjgl.opengl.GL11.glMaterialf;
import static org.lwjgl.opengl.GL11.glMaterialfv;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class MaterialState extends DefaultMaterialState {
	private FloatBuffer ambient = BufferUtils.createFloatBuffer(4);
	private FloatBuffer diffuse = BufferUtils.createFloatBuffer(4);
	private FloatBuffer specular = BufferUtils.createFloatBuffer(4);
	private float shininess = 0;

	public MaterialState() {
		ambient.put(0, 1).put(1, 1).put(2, 1).put(3, 1);
		diffuse.put(0, 1).put(1, 1).put(2, 1).put(3, 1);
		specular.put(3, 1);
	}

	public void setAmbient(float f) {
		ambient.put(0, f).put(1, f).put(2, f);
	}

	public void setAmbientColor(float r, float g, float b) {
		ambient.put(0, r).put(1, g).put(2, b);
	}

	public void setDiffuse(float f) {
		diffuse.put(0, f).put(1, f).put(2, f);
	}

	public void setDiffuseColor(float r, float g, float b) {
		diffuse.put(0, r).put(1, g).put(2, b);
	}

	public void setAbmientAndDiffuse(float f) {
		setAmbient(f);
		setDiffuse(f);
	}

	public void setAbmientAndDiffuseColor(float r, float g, float b) {
		setAmbientColor(r, g, b);
		setDiffuseColor(r, g, b);
	}

	public void setSpecular(float f) {
		specular.put(0, f).put(1, f).put(2, f);
	}

	public void setSpecularColor(float r, float g, float b) {
		specular.put(0, r).put(1, g).put(2, b);
	}

	public void setShininess(float f) {
		if (f < 0)
			shininess = 0.0f;
		else if (f > 128)
			shininess = 128;
		else
			shininess = f;
	}

	public void setAlpha(float f) {
		diffuse.put(3, f);
	}

	@Override
	public int getType() {
		return MATERIAL;
	}

	@Override
	protected void applyState() {
		// glColor(difR, difG, difB, difA);
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	}

	@Override
	protected void clearState() {
		super.applyState();
	}
}
