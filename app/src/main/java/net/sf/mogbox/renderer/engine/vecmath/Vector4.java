/* 
 * Vector4.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.vecmath;

public class Vector4 extends Vector3 {
	public float w;

	public Vector4() {
		w = 0.0f;
	}

	public Vector4(float x, float y, float z, float w) {
		super(x, y, z);
		this.w = w;
	}

	public Vector4(Vector2 v) {
		super(v);
		w = 1.0f;
	}

	public Vector4(Vector2 v, float z, float w) {
		super(v, z);
		this.w = w;
	}

	public Vector4(Vector3 v) {
		super(v);
		w = 1.0f;
	}

	public Vector4(Vector3 v, float w) {
		super(v);
		this.w = w;
	}

	public Vector4(Vector4 v) {
		super(v);
		w = v.w;
	}

	public Vector4 add(Vector4 v, Vector4 dst) {
		super.add(v, dst);
		dst.w = w + v.w;
		return dst;
	}

	public Vector4 multiply(Vector4 v, Vector4 dst) {
		super.multiply(v, dst);
		dst.w = w * v.w;
		return dst;
	}

	public Vector4 copy(Vector4 dst) {
		super.copy(dst);
		dst.w = w;
		return dst;
	}

	public float dot(Vector4 v) {
		return super.dot(v) + w * v.w;
	}

	public Vector4 invert(Vector4 dst) {
		super.invert(dst);
		dst.w = -w;
		return dst;
	}

	public float magnetude() {
		float mag = x * x + y * y + z * z + w * w;
		if (Math.abs(mag - 1.0) > EPSILON) {
			return (float) Math.sqrt(mag);
		} else {
			return 1.0f;
		}
	}

	public Vector4 normalize(Vector4 dst) {
		float mag2 = x * x + y * y + z * z + w * w;
		if (Math.abs(mag2 - 1.0) > EPSILON) {
			float mag = (float) Math.sqrt(mag2);
			dst.x = x / mag;
			dst.y = y / mag;
			dst.z = z / mag;
			dst.w = w / mag;
		} else {
			dst.x = x;
			dst.y = y;
			dst.z = z;
			dst.w = w;
		}
		return dst;
	}

	public Vector4 scale(float s, Vector4 dst) {
		super.scale(s, dst);
		dst.w = w * s;
		return dst;
	}

	public static Vector4 interpolate(Vector4 a, Vector4 b, float t, Vector4 dst) {
		Vector3.interpolate(a, b, t, dst);
		dst.w = a.w * (1.0f - t) + b.w * t;
		return dst;
	}
}
