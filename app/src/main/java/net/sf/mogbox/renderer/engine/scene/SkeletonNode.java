/* 
 * SkeletonNode.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.sf.mogbox.renderer.engine.vecmath.Joint;

public class SkeletonNode extends Node {
	protected Joint[] bind;
	protected Joint[] pose;

	private Map<String, Map<Integer, Motion>> motions = new HashMap<String, Map<Integer, Motion>>();
	private Collection<Motion> currentMotion = null;
	private float t = 0;

	@Override
	protected void initializeNode() {
	}

	public Joint[] getBind() {
		return bind;
	}

	public void setPose(Joint[] pose) {
		if (pose == null) {
			this.pose = null;
		} else {
			if (pose.length != bind.length)
				return;
			this.pose = pose;
		}
	}

	public void addMotion(String name, int number, Motion motion) {
		Map<Integer, Motion> set = motions.get(name);
		if (set == null) {
			set = new TreeMap<Integer, Motion>();
			motions.put(name, set);
		}
		set.put(number, motion);
	}

	public Set<String> getMotions() {
		return motions.keySet();
	}

	public void setMotion(String name) {
		Map<Integer, Motion> set = motions.get(name);
		currentMotion = set == null ? null : set.values();
	}

	@Override
	protected void updateNode(float delta) {
		if (currentMotion != null) {
			Joint[] bind = getBind();
			Joint[] pose = new Joint[bind.length];
			for (int i = 0; i < pose.length; i++)
				pose[i] = new Joint(bind[i]);
			for (Motion m : currentMotion) {
				float t2 = t % m.getDuration();
				m.apply(t2, bind, pose);
			}
			t += delta;
			setPose(pose);
		} else {
			setPose(null);
		}
	}

	@Override
	protected void renderNode(float delta) {
		Joint[] p = new Joint[bind.length];
		if (pose == null) {
			for (int i = 0; i < bind.length; i++) {
				if (bind[i].parent <= 0 || bind[i].parent > i) {
					p[i] = new Joint(bind[i]);
				} else {
					p[i] = p[bind[i].parent].transform(bind[i], new Joint());
				}
			}
		} else {
			for (int i = 0; i < pose.length; i++) {
				if (pose[i].parent <= 0 || pose[i].parent > i) {
					p[i] = new Joint(pose[i]);
				} else {
					p[i] = p[pose[i].parent].transform(pose[i], new Joint());
				}
			}
		}
		for (Node n : children) {
			if (n instanceof SkinNode) {
				((SkinNode) n).setPose(p);
			}
		}
	}
}
