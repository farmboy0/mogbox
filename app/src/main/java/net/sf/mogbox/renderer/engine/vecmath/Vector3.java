/* 
 * Vector3.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.vecmath;

public class Vector3 extends Vector2 {
	public float z;

	public Vector3() {
		z = 0.0f;
	}

	public Vector3(float x, float y, float z) {
		super(x, y);
		this.z = z;
	}

	public Vector3(Vector2 v) {
		super(v);
		z = 0.0f;
	}

	public Vector3(Vector2 v, float z) {
		super(v);
		this.z = z;
	}

	public Vector3(Vector3 v) {
		super(v);
		z = v.z;
	}

	public Vector3 add(Vector3 v, Vector3 dst) {
		super.add(v, dst);
		dst.z = z + v.z;
		return dst;
	}

	public Vector3 multiply(Vector3 v, Vector3 dst) {
		super.multiply(v, dst);
		dst.z = z * v.z;
		return dst;
	}

	public Vector3 copy(Vector3 dst) {
		super.copy(dst);
		dst.z = z;
		return dst;
	}

	public Vector3 cross(Vector3 v, Vector3 dst) {
		float tx = y * v.z - z * v.y;
		float ty = z * v.x - x * v.z;
		float tz = x * v.y - y * v.x;
		dst.x = tx;
		dst.y = ty;
		dst.z = tz;
		return dst;
	}

	public float dot(Vector3 v) {
		return super.dot(v) + z * v.z;
	}

	public Vector3 invert(Vector3 dst) {
		super.invert(dst);
		dst.z = -z;
		return dst;
	}

	public float magnetude() {
		float mag = x * x + y * y + z * z;
		if (Math.abs(mag - 1.0) > EPSILON) {
			return (float) Math.sqrt(mag);
		} else {
			return 1.0f;
		}
	}

	public Vector3 normalize(Vector3 dst) {
		float mag2 = x * x + y * y + z * z;
		if (Math.abs(mag2 - 1.0) > EPSILON) {
			float mag = (float) Math.sqrt(mag2);
			dst.x = x / mag;
			dst.y = y / mag;
			dst.z = z / mag;
		} else {
			dst.x = x;
			dst.y = y;
			dst.z = z;
		}
		return dst;
	}

	public Vector3 scale(float s, Vector3 dst) {
		super.scale(s, dst);
		dst.z = z * s;
		return dst;
	}

	public static Vector3 interpolate(Vector3 a, Vector3 b, float t, Vector3 dst) {
		Vector2.interpolate(a, b, t, dst);
		dst.z = a.z * (1.0f - t) + b.z * t;
		return dst;
	}
}
