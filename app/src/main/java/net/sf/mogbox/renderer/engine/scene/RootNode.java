/* 
 * RootNode.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene;

import java.util.LinkedList;
import java.util.Queue;

public class RootNode extends Node {
	private Queue<Resource> initializeQueue = new LinkedList<Resource>();
	private Queue<Resource> disposeQueue = new LinkedList<Resource>();

	public RootNode() {
		root = this;
	}

	public void initialize(Resource r) {
		disposeQueue.remove(r);
		if (!initializeQueue.contains(r))
			initializeQueue.add(r);
	}

	public void dispose(Resource r) {
		initializeQueue.remove(r);
		if (!disposeQueue.contains(r))
			disposeQueue.add(r);
	}

	@Override
	public void initialize() {
		process();
		super.initialize();
	}

	@Override
	public void render(float delta) {
		process();
		super.render(delta);
	}

	@Override
	public void dispose() {
		process();
		super.dispose();
	}

	private void process() {
		while (!disposeQueue.isEmpty())
			disposeQueue.poll().dispose();
		while (!initializeQueue.isEmpty())
			initializeQueue.poll().initialize();
	}
}
