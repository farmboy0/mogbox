/* 
 * Joint.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.vecmath;

public class Joint {
	public int parent;
	public Quaternion orientation;
	public Vector3 position;
	public Vector3 scale;

	public Joint() {
		position = new Vector3();
		orientation = new Quaternion();
		scale = new Vector3(1, 1, 1);
	}

	public Joint(Joint j) {
		parent = j.parent;
		position = new Vector3(j.position);
		orientation = new Quaternion(j.orientation);
		scale = new Vector3(j.scale);
	}

	public Joint(int parent, Vector3 pos, Quaternion orient) {
		this.parent = parent;
		this.position = pos;
		this.orientation = orient;
		scale = new Vector3(1, 1, 1);
	}

	public Joint(int parent, Vector3 pos, Quaternion orient, Vector3 scale) {
		this.parent = parent;
		this.position = pos;
		this.orientation = orient;
		this.scale = scale;
	}

	public Joint transform(Joint j, Joint dst) {
		if (dst == null)
			dst = new Joint();
		dst.parent = j.parent;
		scale.multiply(j.position, dst.position);
		orientation.rotate(dst.position, dst.position);
		position.add(dst.position, dst.position);
		orientation.concat(j.orientation, dst.orientation);
		scale.multiply(j.scale, dst.scale);
		return dst;
	}

	public Vector3 transform(Vector3 v, Vector3 dst) {
		if (dst == null)
			dst = new Vector3();

		scale.multiply(v, dst);
		orientation.rotate(v, dst);
		position.add(dst, dst);
		return dst;
	}

	public Joint copy(Joint dst) {
		dst.parent = parent;
		orientation.copy(dst.orientation);
		position.copy(dst.position);
		scale.copy(dst.scale);
		return dst;
	}

	public static Joint interpolate(Joint a, Joint b, float t, Joint dst) {
		dst.parent = a.parent;
		Quaternion.interpolate(a.orientation, b.orientation, t, dst.orientation);
		Vector3.interpolate(a.position, b.position, t, dst.position);
		Vector3.interpolate(a.scale, b.scale, t, dst.scale);
		return dst;
	}

	public static Joint[] interpolate(Joint[] a, Joint[] b, float t, Joint[] dst) {
		for (int i = 0; i < a.length; i++)
			dst[i] = Joint.interpolate(a[i], b[i], t, dst[i]);
		return dst;
	}
}
