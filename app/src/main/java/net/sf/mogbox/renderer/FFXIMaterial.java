/* 
 * FFXIMaterial.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.GL11.GL_EMISSION;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.glMaterialfv;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import net.sf.mogbox.renderer.engine.scene.state.MaterialState;
import net.sf.mogbox.renderer.engine.scene.state.ShaderState;

import org.lwjgl.BufferUtils;

public class FFXIMaterial extends MaterialState {
	private FloatBuffer defaultEmission = BufferUtils.createFloatBuffer(4);
	private FloatBuffer reflectivity = BufferUtils.createFloatBuffer(4);

	boolean reflective = false;

	int priority;
	int slot;

	public FFXIMaterial(ByteBuffer in) {
		in.position(in.position() + 13);
		int temp = in.get() & 0xFF;
		if (temp <= 4) {
			priority = temp;
		} else {
			slot = temp - 4;
			priority = temp - slot - 3;
		}
		if (in.getShort() != 0) {
			reflective = true;
			reflectivity.put(0, 1);
			reflectivity.put(1, 0);
			reflectivity.put(2, 0);
		}
		setAbmientAndDiffuse(in.getFloat());
		in.position(in.position() + 16);
		setShininess(in.getFloat());
		setSpecular(in.getFloat());
	}

	public void applyReflection() {
		if (ShaderState.vertexShaderSupported() && ShaderState.fragmentShaderSupported())
			glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, reflectivity);
	}

	@Override
	protected void clearState() {
		super.clearState();
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, defaultEmission);
	}
}