/* 
 * DefaultMaterialState.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene.state;

import static org.lwjgl.opengl.GL11.GL_AMBIENT_AND_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_SHININESS;
import static org.lwjgl.opengl.GL11.GL_SPECULAR;
import static org.lwjgl.opengl.GL11.glMaterialf;
import static org.lwjgl.opengl.GL11.glMaterialfv;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class DefaultMaterialState extends RenderState {
	private FloatBuffer ambientDiffuse = BufferUtils.createFloatBuffer(4);
	private FloatBuffer specular = BufferUtils.createFloatBuffer(4);
	private float shininess = 0;

	public DefaultMaterialState() {
		ambientDiffuse.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
	}

	@Override
	public int getType() {
		return MATERIAL;
	}

	@Override
	public boolean shouldAlwaysApply() {
		return true;
	}

	@Override
	protected void applyState() {
		// glColor3f(ambientDiffuse.get(0));
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, ambientDiffuse);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	}
}
