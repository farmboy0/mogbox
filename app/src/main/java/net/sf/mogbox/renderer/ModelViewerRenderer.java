/* 
 * ModelViewerEngine.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.ARBMultisample.GL_MULTISAMPLE_ARB;
import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_AMBIENT;
import static org.lwjgl.opengl.GL11.GL_AMBIENT_AND_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_COLOR_MATERIAL;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_EXTENSIONS;
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11.GL_FOG;
import static org.lwjgl.opengl.GL11.GL_FOG_COLOR;
import static org.lwjgl.opengl.GL11.GL_FOG_DENSITY;
import static org.lwjgl.opengl.GL11.GL_FOG_END;
import static org.lwjgl.opengl.GL11.GL_FOG_HINT;
import static org.lwjgl.opengl.GL11.GL_FOG_MODE;
import static org.lwjgl.opengl.GL11.GL_FOG_START;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_GEQUAL;
import static org.lwjgl.opengl.GL11.GL_LEQUAL;
import static org.lwjgl.opengl.GL11.GL_LIGHT0;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.GL_LIGHT_MODEL_AMBIENT;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_MODULATE;
import static org.lwjgl.opengl.GL11.GL_NICEST;
import static org.lwjgl.opengl.GL11.GL_NORMALIZE;
import static org.lwjgl.opengl.GL11.GL_POSITION;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_RENDERER;
import static org.lwjgl.opengl.GL11.GL_RGB;
import static org.lwjgl.opengl.GL11.GL_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_SPECULAR;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV_MODE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glClearDepth;
import static org.lwjgl.opengl.GL11.glColorMaterial;
import static org.lwjgl.opengl.GL11.glDepthFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFogf;
import static org.lwjgl.opengl.GL11.glFogfv;
import static org.lwjgl.opengl.GL11.glFogi;
import static org.lwjgl.opengl.GL11.glFrontFace;
import static org.lwjgl.opengl.GL11.glFrustum;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL11.glHint;
import static org.lwjgl.opengl.GL11.glLightModelfv;
import static org.lwjgl.opengl.GL11.glLightModeli;
import static org.lwjgl.opengl.GL11.glLightfv;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glShadeModel;
import static org.lwjgl.opengl.GL11.glTexEnvi;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL12.GL_LIGHT_MODEL_COLOR_CONTROL;
import static org.lwjgl.opengl.GL12.GL_SEPARATE_SPECULAR_COLOR;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import net.sf.mogbox.renderer.engine.RenderImageData;
import net.sf.mogbox.renderer.engine.Renderer;
import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.RootNode;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class ModelViewerRenderer implements Renderer {
	private static final float DRAW_DISTANCE = 500.0f;
	private static final float FOG_DISTANCE = 0f;
	private static final float FOG_DENSITY = 0.3f;

	private boolean transparency = true;
	private boolean wireframe = false;

	private int w = 1, h = 1;

	private Node currentNode = null;
	private List<Node> nodes = new LinkedList<>();

	private FloatBuffer color = BufferUtils.createFloatBuffer(4);

	private Camera camera;
	private RootNode root;

	public ModelViewerRenderer() {
		camera = new Camera();
		root = new RootNode();
	}

	@Override
	public Camera getCamera() {
		return camera;
	}

	@Override
	public boolean isWireframeEnabled() {
		return wireframe;
	}

	@Override
	public void setWireframeEnabled(boolean enabled) {
		wireframe = enabled;
	}

	@Override
	public boolean isTransparencyEnabled() {
		return transparency;
	}

	@Override
	public void setTransparencyeEnabled(boolean enabled) {
		transparency = enabled;
	}

	@Override
	public void addNode(final Node node) {
		nodes.add(node);
	}

	@Override
	public void displayNode(Node node) {
		root.remove(currentNode);
		currentNode = node;
		if (node != null)
			root.add(node);
	}

	@Override
	public void initialize() {
		GL.createCapabilities();

		Logger log = Logger.getLogger("");
		log.info(() -> "OpenGL Version: " + glGetString(GL_VERSION));
		log.info(() -> "OpenGL Vendor: " + glGetString(GL_VENDOR));
		log.info(() -> "OpenGL Renderer: " + glGetString(GL_RENDERER));

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_NORMALIZE);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_FOG);

		glEnable(GL_MULTISAMPLE_ARB);

		glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
		glShadeModel(GL_SMOOTH);
		glDepthFunc(GL_LEQUAL);
		glClearDepth(1.0);
		glFrontFace(GL11.GL_CW);
		glAlphaFunc(GL_GEQUAL, 0.0625f);

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		glBindTexture(GL_TEXTURE_2D, 0);
		ByteBuffer texture = BufferUtils.createByteBuffer(3);
		texture.put((byte) 0xFF).put((byte) 0xFF).put((byte) 0xFF).clear();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);

		color.put(3, 1);
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, color);

		color.put(1f).put(1f).put(1f).put(0f).clear();
		glLightfv(GL_LIGHT0, GL_POSITION, color);
		color.put(0.5f).put(0.5f).put(0.5f).put(0f).clear();
		glLightfv(GL_LIGHT0, GL_SPECULAR, color);
		color.put(0.5f).put(0.5f).put(0.5f).put(0f).clear();
		glLightfv(GL_LIGHT0, GL_AMBIENT, color);
		color.put(0.5f).put(0.5f).put(0.5f).put(1f).clear();
		glLightfv(GL_LIGHT0, GL_DIFFUSE, color);

		if (glGetString(GL_EXTENSIONS).indexOf("GL_ARB_vertex_program") == -1) {
			glEnable(GL_COLOR_MATERIAL);

			glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
			glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

			glFogi(GL_FOG_MODE, GL_LINEAR);
			glHint(GL_FOG_HINT, GL_NICEST);
		}

		root.initialize();
	}

	@Override
	public void reset() {
		camera.resetCamera();
	}

	@Override
	public void resize(int w, int h) {
		this.w = w < 1 ? 1 : w;
		this.h = h < 1 ? 1 : h;
		glViewport(0, 0, this.w, this.h);
	}

	@Override
	public void update(float delta) {
		root.update(delta);
	}

	@Override
	public void render(float delta) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_PROJECTION);
		double zNear = 1.0f;
		double top = zNear;
		if (camera.getZoom() > 0)
			top *= Math.tan(camera.getFov() / camera.getZoom() / 360 * Math.PI);
		double right = top;
		if (h > 0)
			right = top * w / h;
		glLoadIdentity();
		glFrustum(-right, right, -top, top, zNear, DRAW_DISTANCE);
		glMatrixMode(GL_MODELVIEW);

		glLoadIdentity();
		glRotatef(camera.getPanY(), 1, 0, 0);
		glRotatef(camera.getPanX(), 0, 1, 0);
		glTranslated(0, 0, -camera.getDistance());
		glRotatef(camera.getRoll(), 0, 0, 1);
		glRotatef(camera.getPitch(), 1, 0, 0);
		glRotatef(camera.getYaw(), 0, 1, 0);
		glTranslatef(0, -camera.getAltitude(), 0);
		glRotatef(180, 1, 0, 0);

		glFogf(GL_FOG_START, FOG_DISTANCE * DRAW_DISTANCE);
		glFogf(GL_FOG_END, DRAW_DISTANCE);
		glFogf(GL_FOG_DENSITY, FOG_DENSITY);
		color.put(0.25f).put(0.25f).put(0.25f).put(1f).clear();
		glFogfv(GL_FOG_COLOR, color);

		if (wireframe) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_ALPHA_TEST);
		} else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			if (transparency)
				glEnable(GL_ALPHA_TEST);
			else
				glDisable(GL_ALPHA_TEST);
		}

		root.render(delta);
	}

	@Override
	public void dispose() {
		root.dispose();
	}

	@Override
	public RenderImageData readImageData() {
		final RenderImageData result = new RenderImageData();
		result.width = w;
		result.height = h;
		result.color = new byte[(w * 3 + 3) / 4 * 4 * h];
		result.alpha = new byte[w * h];

		ByteBuffer buffer = BufferUtils.createByteBuffer(result.color.length);

		GL11.glReadBuffer(GL11.GL_BACK);
		GL11.glPushClientAttrib(GL11.GL_PACK_ALIGNMENT);

		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 4);
		GL11.glReadPixels(0, 0, w, h, GL12.GL_BGR, GL11.GL_UNSIGNED_BYTE, buffer);
		buffer.get(result.color).clear();

		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		GL11.glReadPixels(0, 0, w, h, GL11.GL_ALPHA, GL11.GL_UNSIGNED_BYTE, buffer);
		buffer.get(result.alpha).clear();

		GL11.glPopClientAttrib();

		return result;
	}
}
