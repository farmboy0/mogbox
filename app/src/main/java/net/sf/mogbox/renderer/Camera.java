package net.sf.mogbox.renderer;

public class Camera {
	private float altitude = 1;
	private float distance = 5;
	private float zoom = 1;

	private float yaw = 0;
	private float pitch = 0;
	private float roll = 0;
	private float fov = 37.8f;
	private float panX = 0;
	private float panY = 0;

	public void resetCamera() {
		yaw = 0;
		pitch = 0;
		roll = 0;
		distance = 5;
		fov = 37.8f;
		panX = 0;
		panY = 0;
	}

	public float getAltitude() {
		return altitude;
	}

	public void setAltitude(float altitude) {
		this.altitude = altitude;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		if (distance < 0.2)
			this.distance = 0.2f;
		if (distance > 2000)
			this.distance = 2000;
		else
			this.distance = distance;
	}

	public float getZoom() {
		return zoom;
	}

	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw %= 360;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		if (pitch > 90)
			this.pitch = 90;
		else if (pitch < -90)
			this.pitch = -90;
		else
			this.pitch = pitch;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll %= 360;
	}

	public float getFov() {
		return fov;
	}

	public void setFov(float fov) {
		if (fov < 1)
			this.fov = 1;
		else if (fov > 180)
			this.fov = 180;
		else
			this.fov = fov;
	}

	public float getPanX() {
		return panX;
	}

	public void setPanX(float panX) {
		this.panX = panX;
	}

	public float getPanY() {
		return panY;
	}

	public void setPanY(float panY) {
		this.panY = panY;
	}
}
