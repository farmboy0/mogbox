/* 
 * Vector2.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.vecmath;

public class Vector2 {
	public static final float EPSILON = 6E-8f;

	public float x, y;

	public Vector2() {
		x = 0.0f;
		y = 0.0f;
	}

	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2(Vector2 v) {
		x = v.x;
		y = v.y;
	}

	public Vector2 add(Vector2 v, Vector2 dst) {
		dst.x = x + v.x;
		dst.y = y + v.y;
		return dst;
	}

	public Vector2 multiply(Vector2 v, Vector2 dst) {
		dst.x = x * v.x;
		dst.y = y * v.y;
		return dst;
	}

	public Vector2 copy(Vector2 dst) {
		dst.x = x;
		dst.y = y;
		return dst;
	}

	public float dot(Vector2 v) {
		return x * v.x + y * v.y;
	}

	public Vector2 invert(Vector2 dst) {
		dst.x = -x;
		dst.y = -y;
		return dst;
	}

	public float magnetude() {
		float mag = x * x + y * y;
		if (Math.abs(mag - 1.0) > EPSILON) {
			return (float) Math.sqrt(mag);
		} else {
			return 1.0f;
		}
	}

	public Vector2 normalize(Vector2 dst) {
		float mag2 = x * x + y * y;
		if (Math.abs(mag2 - 1.0) > EPSILON) {
			float mag = (float) Math.sqrt(mag2);
			dst.x = x / mag;
			dst.y = y / mag;
		} else {
			dst.x = x;
			dst.y = y;
		}
		return dst;
	}

	public Vector2 scale(float s, Vector2 dst) {
		dst.x = x * s;
		dst.y = y * s;
		return dst;
	}

	public static Vector2 interpolate(Vector2 a, Vector2 b, float t, Vector2 dst) {
		dst.x = a.x * (1.0f - t) + b.x * t;
		dst.y = a.y * (1.0f - t) + b.y * t;
		return dst;
	}

	public float angle(Vector2 v) {
		if (Math.abs(magnetude() - 1.0) < EPSILON)
			return 0;
		if (Math.abs(v.magnetude() - 1.0) < EPSILON)
			return 0;
		double theta = Math.atan2(y, -x);
		theta -= Math.atan2(v.y, -v.x);
		return (float) (theta * 180 / Math.PI);
	}
}
