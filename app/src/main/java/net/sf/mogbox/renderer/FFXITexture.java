/* 
 * FFXITexture.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
import static org.lwjgl.opengl.EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
import static org.lwjgl.opengl.GL11.GL_RGBA;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import net.sf.mogbox.renderer.engine.scene.state.TextureState;

import org.lwjgl.BufferUtils;

public class FFXITexture extends TextureState {
	private String name;

	boolean used = false;

	public FFXITexture(ByteBuffer buffer) {
		ByteBuffer b = BufferUtils.createByteBuffer(buffer.capacity());
		b.put(buffer);
		b.clear();
		byte flags = b.get(); // Flags
		char[] c = new char[16];
		for (int i = 0; i < c.length; i++)
			c[i] = (char) (b.get() & 0xFF);
		name = new String(c);
		b.getInt();
		w = b.getInt();
		h = b.getInt();
		b.getShort();
		b.getShort();
		b.getInt();
		b.getInt();
		b.getInt();
		b.getInt();
		b.getInt();
		b.getInt();
		try {
			if ((flags & 0x30) == 0x20) {
				type = b.getInt();
				switch (type) {
				case 0x44585431: // DXT1
					type = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
					break;
				case 0x44585433: // DXT3
					type = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
					break;
				case 0x44585435: // DXT5
					type = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
					break;
				default:
					throw new RuntimeException(String.format("Unknown Texture Type: %c%c%c%c[%08X]", type >> 24 & 0xFF,
							type >> 16 & 0xFF, type >> 8 & 0xFF, type & 0xFF, type));
				}
				int size = b.getInt();
				b.getInt();
				data = BufferUtils.createByteBuffer(size);
				b.limit(b.position() + size);
				data.put(b).flip();
			} else {
				type = GL_RGBA;
				int[] pallet = new int[256];
				for (int i = 0; i < 256; i++) {
					int color = 0;
					color |= (b.get() & 0xFF) << 0x10; // B
					color |= (b.get() & 0xFF) << 0x08; // G
					color |= (b.get() & 0xFF) << 0x00; // R
					color |= (b.get() & 0xFF) << 0x18; // A
					pallet[i] = color;
				}
				int start = b.position();
				int size = w * h * 4;
				data = BufferUtils.createByteBuffer(size);
				IntBuffer temp = data.asIntBuffer();
				for (int y = h - 1; y >= 0; y--) {
					for (int x = 0; x < w; x++) {
						int i = b.get(start + y * w + x) & 0xFF;
						temp.put(pallet[i]);
					}
				}
				temp.flip();
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public String getName() {
		return name;
	}
}
