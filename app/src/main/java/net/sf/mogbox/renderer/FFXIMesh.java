/* 
 * FFXIMesh.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.ARBMultitexture.GL_TEXTURE1_ARB;

import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.state.RenderState;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

import org.lwjgl.opengl.GL11;

public abstract class FFXIMesh extends Node {
	private final FFXIModel model;

	protected short[] vertices;
	protected float[] uv;

	protected FFXIMesh(FFXIModel model) {
		this.model = model;
	}

	@Override
	protected void renderNode(float delta) {
		RenderState state = states[RenderState.MATERIAL];
		FFXIMaterial material = null;
		TextureState env = model.getEnvirontmentMap();
		if (state instanceof FFXIMaterial) {
			material = (FFXIMaterial) state;
			if (material != null) {
				if (material.slot == 0) {
					if (material.priority != 0 && material.priority < model.limits[0])
						return;
				} else {
					if (material.priority < model.limits[material.slot])
						return;
				}
			}
		}

		int oldUnit = 0;
		if (material != null && material.reflective && env != null) {
			material.applyReflection();
			oldUnit = env.getUnit();
			env.setUnit(GL_TEXTURE1_ARB);
			env.apply();
		}

		begin();
		for (int i = 0; i < vertices.length; i++) {
			int n = i * 2;
			model.sendVertex(vertices[i], uv[n], uv[n + 1], false);
		}
		end();

		if (model.isMirrored()) {
			GL11.glFrontFace(GL11.GL_CCW);
			begin();
			for (int i = 0; i < vertices.length; i++) {
				int n = i * 2;
				model.sendVertex(vertices[i], uv[n], uv[n + 1], true);
			}
			end();
			GL11.glFrontFace(GL11.GL_CW);
		}

		if (material != null && material.reflective && env != null) {
			env.clear();
			env.setUnit(oldUnit);
		}
	}

	protected abstract void begin();

	protected abstract void end();
}