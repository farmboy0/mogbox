/* 
 * Node.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import net.sf.mogbox.renderer.engine.scene.state.RenderState;

public class Node implements Resource {
	protected RootNode root;
	protected Node parent;
	protected List<Node> children = new LinkedList<Node>();
	protected RenderState[] states = new RenderState[RenderState.TYPE + 1];
	protected RenderState[] finalStates = new RenderState[RenderState.TYPE + 1];

	protected Queue<FutureTask<?>> preUpdateTasks = new ConcurrentLinkedQueue<FutureTask<?>>();
	protected Queue<FutureTask<?>> postUpdateTasks = new ConcurrentLinkedQueue<FutureTask<?>>();
	protected Queue<FutureTask<?>> preRenderTasks = new ConcurrentLinkedQueue<FutureTask<?>>();
	protected Queue<FutureTask<?>> postRenderTasks = new ConcurrentLinkedQueue<FutureTask<?>>();

	private boolean initialized = false;

	public void add(Node node) {
		if (node == this)
			throw new RuntimeException("Cannot add a Node to itself.");
		if (node.parent != null) {
			node.parent.remove(node);
		}
		children.add(node);
		node.setParent(this);
		node.setRoot(root);
		node.updateRenderStates();
		if (root != null && initialized)
			root.initialize(node);
	}

	public void remove(Node node) {
		if (children.remove(node)) {
			node.setParent(null);
			node.setRoot(null);
			node.updateRenderStates();
			if (root != null && initialized)
				root.dispose(node);
		}
	}

	public Node[] getChildren() {
		return children.toArray(new Node[children.size()]);
	}

	public void addState(RenderState state) {
		int type = state.getType();
		if (type < 0 || type > RenderState.TYPE)
			return;
		states[type] = state;
		updateRenderStates();
		if (root != null && initialized)
			root.initialize(state);
	}

	public void removeState(RenderState state) {
		int type = state.getType();
		if (type < 0 || type > RenderState.TYPE)
			return;
		if (state != states[type])
			return;
		states[type] = null;
		updateRenderStates();
		if (root != null && initialized)
			root.dispose(state);
	}

	protected void setParent(Node parent) {
		this.parent = parent;
	}

	protected void setRoot(RootNode root) {
		this.root = root;
		for (Node child : children)
			child.setRoot(root);
	}

	protected void updateRenderStates() {
		for (int i = 0; i < finalStates.length; i++) {
			finalStates[i] = states[i];
			if (finalStates[i] == null && parent != null)
				finalStates[i] = parent.finalStates[i];
		}
		for (Node n : children)
			n.updateRenderStates();
	}

	public <V> Future<V> preUpdateExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<V>(callable);
		preUpdateTasks.add(task);
		return task;
	}

	public <V> Future<V> preUpdateExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<V>(runnable, result);
		preUpdateTasks.add(task);
		return task;
	}

	public <V> Future<V> postUpdateExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<V>(callable);
		postUpdateTasks.add(task);
		return task;
	}

	public <V> Future<V> postUpdateExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<V>(runnable, result);
		postUpdateTasks.add(task);
		return task;
	}

	public <V> Future<V> preRenderExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<V>(callable);
		preRenderTasks.add(task);
		return task;
	}

	public <V> Future<V> preRenderExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<V>(runnable, result);
		preRenderTasks.add(task);
		return task;
	}

	public <V> Future<V> postRenderExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<V>(callable);
		postRenderTasks.add(task);
		return task;
	}

	public <V> Future<V> postRenderExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<V>(runnable, result);
		postRenderTasks.add(task);
		return task;
	}

	@Override
	public void initialize() {
		if (!initialized) {
			initializeNode();
			initialized = true;
			for (Resource r : children)
				r.initialize();
			for (int i = 0; i < states.length; i++) {
				RenderState state = states[i];
				if (state != null)
					state.initialize();
			}
		}
	}

	protected void initializeNode() {
	}

	public void update(float delta) {
		while (!preUpdateTasks.isEmpty())
			preUpdateTasks.poll().run();
		updateNode(delta);
		for (Node n : children) {
			n.update(delta);
		}
		while (!postUpdateTasks.isEmpty())
			postUpdateTasks.poll().run();
	}

	protected void updateNode(float delta) {
	}

	public void render(float delta) {
		while (!preRenderTasks.isEmpty())
			preRenderTasks.poll().run();
		for (int i = 0; i < finalStates.length; i++) {
			if (finalStates[i] != null)
				finalStates[i].apply();
		}
		renderNode(delta);
		for (int i = 0; i < finalStates.length; i++) {
			if (finalStates[i] != null)
				finalStates[i].clear();
		}
		for (Node n : children) {
			n.render(delta);
		}
		while (!postRenderTasks.isEmpty())
			postRenderTasks.poll().run();
	}

	protected void renderNode(float delta) {
	}

	@Override
	public void dispose() {
		if (initialized) {
			disposeNode();
			initialized = false;
			for (Resource r : children)
				r.dispose();
			for (int i = 0; i < states.length; i++) {
				RenderState state = states[i];
				if (state != null)
					state.dispose();
			}
		}
	}

	protected void disposeNode() {
	}
}
