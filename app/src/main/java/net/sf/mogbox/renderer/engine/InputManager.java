/* 
 * InputManager.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

import net.sf.mogbox.renderer.engine.vecmath.Vector2;

public abstract class InputManager {
	public static final int KEY_BACKSPACE = 0x08;
	public static final int KEY_TAB = 0x09;

	public static final int KEY_ENTER = 0x0D;

	public static final int KEY_ESC = 0x1B;

	public static final int KEY_DELETE = 0x7F;

	public static final int KEY_ARROW_UP = 0x80 | 1;
	public static final int KEY_ARROW_DOWN = 0x80 | 2;
	public static final int KEY_ARROW_LEFT = 0x80 | 3;
	public static final int KEY_ARROW_RIGHT = 0x80 | 4;
	public static final int KEY_PAGE_UP = 0x80 | 5;
	public static final int KEY_PAGE_DOWN = 0x80 | 6;
	public static final int KEY_HOME = 0x80 | 7;
	public static final int KEY_END = 0x80 | 8;
	public static final int KEY_INSERT = 0x80 | 9;
	public static final int KEY_F1 = 0x80 | 10;
	public static final int KEY_F2 = 0x80 | 11;
	public static final int KEY_F3 = 0x80 | 12;
	public static final int KEY_F4 = 0x80 | 13;
	public static final int KEY_F5 = 0x80 | 14;
	public static final int KEY_F6 = 0x80 | 15;
	public static final int KEY_F7 = 0x80 | 16;
	public static final int KEY_F8 = 0x80 | 17;
	public static final int KEY_F9 = 0x80 | 18;
	public static final int KEY_F10 = 0x80 | 19;
	public static final int KEY_F11 = 0x80 | 20;
	public static final int KEY_F12 = 0x80 | 21;

	public static final int KEY_NUMPAD_MULTIPLY = 0x80 | 42;
	public static final int KEY_NUMPAD_PLUS = 0x80 | 43;

	public static final int KEY_NUMPAD_MINUS = 0x80 | 45;
	public static final int KEY_NUMPAD_DECIMAL = 0x80 | 46;
	public static final int KEY_NUMPAD_DIVIDE = 0x80 | 47;
	public static final int KEY_NUMPAD_0 = 0x80 | 48;
	public static final int KEY_NUMPAD_1 = 0x80 | 49;
	public static final int KEY_NUMPAD_2 = 0x80 | 50;
	public static final int KEY_NUMPAD_3 = 0x80 | 51;
	public static final int KEY_NUMPAD_4 = 0x80 | 52;
	public static final int KEY_NUMPAD_5 = 0x80 | 53;
	public static final int KEY_NUMPAD_6 = 0x80 | 54;
	public static final int KEY_NUMPAD_7 = 0x80 | 55;
	public static final int KEY_NUMPAD_8 = 0x80 | 56;
	public static final int KEY_NUMPAD_9 = 0x80 | 57;

	public static final int KEY_NUMPAD_ENTER = 0x80 | 80;

	public static final int KEY_HELP = 0x80 | 81;
	public static final int KEY_CAPS_LOCK = 0x80 | 82;
	public static final int KEY_NUM_LOCK = 0x80 | 83;
	public static final int KEY_SCROLL_LOCK = 0x80 | 84;
	public static final int KEY_PAUSE = 0x80 | 85;
	public static final int KEY_BREAK = 0x80 | 86;
	public static final int KEY_PRINT_SCREEN = 0x80 | 87;

	public static final int KEY_SHIFT = 0x80 | 124;
	public static final int KEY_CTRL = 0x80 | 125;
	public static final int KEY_ALT = 0x80 | 126;
	public static final int KEY_COMMAND = 0x80 | 127;

	public static final int MOUSE_BUTTON_1 = 1;
	public static final int MOUSE_BUTTON_2 = 2;
	public static final int MOUSE_BUTTON_3 = 3;

	private AtomicIntegerArray liveKeyStates = new AtomicIntegerArray(256);
	private AtomicIntegerArray liveButtonStates = new AtomicIntegerArray(3);
	private AtomicInteger liveWheelRotation = new AtomicInteger();

	private int[] savedKeyStates = new int[256];
	private int[] savedButtonStates = new int[3];
	private int savedWheelRotation = 0;

	private Vector2 mousePosition = null;
	private Vector2 mouseDelta = new Vector2();

	public final void update() {
		for (int i = 0; i < liveKeyStates.length(); i++) {
			int temp = liveKeyStates.get(i);
			savedKeyStates[i] = temp;
			liveKeyStates.addAndGet(i, temp % 2 - temp);
		}
		for (int i = 0; i < liveButtonStates.length(); i++) {
			int temp = liveButtonStates.get(i);
			savedButtonStates[i] = temp;
			liveButtonStates.addAndGet(i, temp % 2 - temp);
		}
		savedWheelRotation = liveWheelRotation.getAndSet(0);
		if (mousePosition == null) {
			mousePosition = new Vector2();
			if (!readCursorPosition(mousePosition)) {
				mousePosition.x = 0;
				mousePosition.y = 0;
			}
			mouseDelta.x = 0;
			mouseDelta.y = 0;
		} else {
			if (!readCursorPosition(mouseDelta)) {
				mouseDelta.x = mousePosition.x;
				mouseDelta.y = mousePosition.y;
			}
			float x = mousePosition.x - mouseDelta.x;
			float y = mousePosition.y - mouseDelta.y;
			mousePosition.x = mouseDelta.x;
			mousePosition.y = mouseDelta.y;
			mouseDelta.x = x;
			mouseDelta.y = y;
		}
	}

	public final boolean isMousePressed(int button) {
		return savedButtonStates[button - 1] % 2 == 1;
	}

	public final boolean isKeyPressed(int keyCode) {
		return savedKeyStates[keyCode] % 2 == 1;
	}

	public final Vector2 getMouseLocation() {
		return mousePosition;
	}

	public final Vector2 getMouseDelta() {
		return mouseDelta;
	}

	public final int getWheelRotation() {
		return savedWheelRotation;
	}

	protected final void keyDown(int keyCode) {
		if (keyCode >= 0 && keyCode <= 255) {
			int temp = liveKeyStates.get(keyCode);
			if (temp % 2 == 0)
				liveKeyStates.addAndGet(keyCode, 1);
		}
	}

	protected final void keyUp(int keyCode) {
		if (keyCode >= 0 && keyCode <= 255) {
			int temp = liveKeyStates.get(keyCode);
			if (temp % 2 == 1)
				liveKeyStates.addAndGet(keyCode, 1);
		}
	}

	protected final void mouseDown(int button) {
		if (button >= 1 && button <= 3) {
			int temp = liveButtonStates.get(button - 1);
			if (temp % 2 == 0)
				liveButtonStates.addAndGet(button - 1, 1);
		}
	}

	protected final void mouseUp(int button) {
		if (button >= 1 && button <= 3) {
			int temp = liveButtonStates.get(button - 1);
			if (temp % 2 == 1)
				liveButtonStates.addAndGet(button - 1, 1);
		}
	}

	protected final void wheelRotate(int clicks) {
		liveWheelRotation.addAndGet(clicks);
	}

	protected abstract boolean readCursorPosition(Vector2 dst);
}
