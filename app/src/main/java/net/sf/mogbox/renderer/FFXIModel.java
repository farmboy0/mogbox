/* 
 * FFXIModel.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.ARBMultitexture.GL_TEXTURE1_ARB;
import static org.lwjgl.opengl.ARBMultitexture.glActiveTextureARB;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.SkinNode;
import net.sf.mogbox.renderer.engine.scene.state.MaterialState;
import net.sf.mogbox.renderer.engine.scene.state.ShaderState;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;
import net.sf.mogbox.renderer.engine.vecmath.Joint;
import net.sf.mogbox.renderer.engine.vecmath.Vector3;

public class FFXIModel extends SkinNode {
	private static final Logger log = Logger.getLogger(FFXIModel.class.getName());
	private static final ShaderState SHADER;
	static {
		ShaderState temp = null;
		try {
			URL vertexshader = FFXIModel.class.getResource("model-vert.arb");
			URL fragmentshader = FFXIModel.class.getResource("model-frag.arb");
			temp = new ShaderState(vertexshader, fragmentshader);
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
		}
		if (temp != null)
			SHADER = temp;
		else
			SHADER = null;
	}

	private final Vector3 N1 = new Vector3(), N2 = new Vector3();
	private final Vector3 V1 = new Vector3(), V2 = new Vector3();

	private int flags;

	private byte[] bones;
	private float[] weights;
	private Vector3[] vertices;
	private Vector3[] normals;
	private byte[] mirror;

	public int slot;
	public int priority;
	public int[] limits;

	protected TextureState environment;

	private Map<String, TextureState> textures;

	public FFXIModel(ByteBuffer b, Map<String, TextureState> textures) {
		if (SHADER != null)
			addState(SHADER);

		this.textures = textures;

		b.get();
		flags = b.getShort() & 0xFFFF;
		priority = b.get();
		slot = (byte) (priority >>> 4);
		priority &= 0xF;
		switch (slot) {
		case 2:
			slot = 3;
			break;
		case 3:
			slot = 2;
			break;
		}
		flags |= b.getShort() << 16;

		int mesh_offset = b.getInt() << 1;
		int mesh_length = (b.getShort() & 0xFFFF) << 1;
		int jointList_offset = b.getInt() << 1;
		int jointList_length = (b.getShort() & 0xFFFF) << 1;
		int vertexInfo_offset = b.getInt() << 1;
		int vertexInto_length = (b.getShort() & 0xFFFF) << 1;
		int weightList_offset = b.getInt() << 1;
		int weightList_length = (b.getShort() & 0xFFFF) << 1;
		int vertexList_offset = b.getInt() << 1;
		int vertexList_length = (b.getShort() & 0xFFFF) << 1;

		b.position(jointList_offset).limit(jointList_offset + jointList_length);
		int nJoints = b.getShort(16);
		byte[] jointIDs = new byte[nJoints];
		for (int i = 0; i < nJoints; i++) {
			jointIDs[i] = (byte) b.getShort();
		}
		b.clear();

		b.position(vertexInfo_offset).limit(vertexInfo_offset + vertexInto_length);
		short singleWeights = b.getShort();
		int nVerts = (short) (singleWeights + b.getShort() & 0xFFFF);
		b.clear();

		b.position(weightList_offset).limit(weightList_offset + weightList_length);
		bones = new byte[nVerts * 4];
		mirror = new byte[nVerts];
		for (int i = 0; i < nVerts; i++) {
			int s = i * 4;
			int t = b.getInt();
			if ((flags & 0x00008000) == 0) {
				bones[s] = (byte) (t & 0x7F);
				bones[s + 1] = (byte) (t >> 16 & 0x7F);
				bones[s + 2] = (byte) (t >> 7 & 0x7F);
				bones[s + 3] = (byte) (t >> 23 & 0x7F);
			} else {
				bones[s] = jointIDs[t & 0x7F];
				bones[s + 1] = jointIDs[t >> 16 & 0x7F];
				bones[s + 2] = jointIDs[t >> 7 & 0x7F];
				bones[s + 3] = jointIDs[t >> 23 & 0x7F];
			}
			mirror[i] = (byte) (t >> 14 & 0x3);
			mirror[i] |= (byte) (t >> 28 & 0xC);
		}
		b.clear();

		b.position(vertexList_offset).limit(vertexList_offset + vertexList_length);
		weights = new float[nVerts * 2];
		vertices = new Vector3[nVerts * 2];
		normals = new Vector3[nVerts * 2];
		boolean calculateNormals = (flags & 0x00000100) != 0;
		for (int i = 0; i < nVerts; i++) {
			int s = i * 2;
			Vector3 t1, t2;
			if (i < singleWeights) {
				t1 = vertices[s] = new Vector3();
				vertices[s + 1] = new Vector3();
				t1.x = b.getFloat();
				t1.y = b.getFloat();
				t1.z = b.getFloat();

				weights[s] = 1.0f;
				weights[s + 1] = 0.0f;

				t1 = normals[s] = new Vector3();
				normals[s + 1] = new Vector3();
				if (!calculateNormals) {
					t1.x = b.getFloat();
					t1.y = b.getFloat();
					t1.z = b.getFloat();
				}
			} else {
				t1 = vertices[s] = new Vector3();
				t2 = vertices[s + 1] = new Vector3();
				t1.x = b.getFloat();
				t2.x = b.getFloat();
				t1.y = b.getFloat();
				t2.y = b.getFloat();
				t1.z = b.getFloat();
				t2.z = b.getFloat();

				weights[s] = b.getFloat();
				weights[s + 1] = b.getFloat();

				t1 = normals[s] = new Vector3();
				t2 = normals[s + 1] = new Vector3();
				if (!calculateNormals) {
					t1.x = b.getFloat();
					t2.x = b.getFloat();
					t1.y = b.getFloat();
					t2.y = b.getFloat();
					t1.z = b.getFloat();
					t2.z = b.getFloat();
				}
			}
		}
		b.clear();

		Node node;
		TextureState texture = null;
		MaterialState material = null;
		b.position(mesh_offset).limit(mesh_offset + mesh_length);
		loop: while (b.remaining() > 0) {
			int id = b.getShort() & 0xFFFF;
			switch (id) {
			default:
				log.warning(String.format("Unknown Chunk [ID: %04X] at 0x%04X: Aborting", id, b.position() - 2));
				break loop;
			case 0xFFFF:
				break loop;
			case 0x0043: // TODO Figure out what this type is for.
				/*
				 * This is used everywhere. I have no idea what it is supposed to do but I do
				 * know a little bit about the structure. It may have been used in the
				 * development process and SE simply forgot to remove it.
				 * 
				 * It is basically a specialized triangle list. It has a two byte header
				 * specifying the number of triangles followed by the vertex list. The vertex
				 * list is made up of three vertex structures for every triangle in the list.
				 * Each vertex includes three floats for the <x,y,z> coordinates along with four
				 * bytes of unknown data (possibly weights). The triangles are not rendered
				 * in-game in any way and don't seem to have any practical purpose.
				 */
				b.position((b.getShort() & 0xFFFF) * 10 + b.position());
				break;
			case 0x0054:
				node = new FFXITriangleMesh(this, b);
				if (texture != null)
					node.addState(texture);
				if (material != null)
					node.addState(material);
				add(node);
				break;
			case 0x4353: // TODO Figure out what this type is for.
				log.fine(String.format("Unknown Chunk [ID: %04X] at 0x%04X: Skipping", id, b.position() - 2));
				/*
				 * So far this has only been found in Hecteye models. I have yet to figure out
				 * it's use or structure and I can only guess that it is a constant 58 bytes
				 * long. Another possibility is that it has a six byte header and that the
				 * length of the structure is given by the fifth and sixth bytes.
				 * 
				 * Both Model Viewer and AltanaView choke on this type and stop reading the
				 * file, causing strange rendering of Hecteyes.
				 */
				b.position(b.position() + 0x3A);
				break;
			case 0x5453:
				node = new FFXITriangleStrip(this, b);
				if (texture != null)
					node.addState(texture);
				if (material != null)
					node.addState(material);
				add(node);
				break;
			case 0x8000:
				char[] c = new char[16];
				for (int i = 0; i < c.length; i++)
					c[i] = (char) (b.get() & 0xFF);
				String key = new String(c);
				texture = textures.get(key);
				FFXITexture tex = (FFXITexture) texture;
				if (tex != null && !tex.used)
					tex.used = true;
				break;
			case 0x8010:
				material = new FFXIMaterial(b);
				break;
			}
		}
		b.clear();
	}

	public boolean isMirrored() {
		return (flags & 0x00010000) != 0;
	}

	void sendVertex(int index, float u, float v, boolean mirrored) {
		if (pose == null)
			return;
		Joint j1;
		Joint j2;
		if (!mirrored) {
			j1 = pose[bones[index * 4]];
			j2 = pose[bones[index * 4 + 1]];
			normals[index * 2].copy(N1);
			normals[index * 2 + 1].copy(N2);
			vertices[index * 2].copy(V1);
			vertices[index * 2 + 1].copy(V2);
		} else {
			int m1 = mirror[index];
			int m2 = m1 >>> 2;
			m1 &= 3;
			j1 = pose[bones[index * 4 + 2]];
			j2 = pose[bones[index * 4 + 3]];
			mirror(m1, normals[index * 2], N1);
			mirror(m2, normals[index * 2 + 1], N2);
			mirror(m1, vertices[index * 2], V1);
			mirror(m2, vertices[index * 2 + 1], V2);
		}
		j1.orientation.rotate(N1, N1);
		j2.orientation.rotate(N2, N2);
		N1.add(N2, N1);

		j1.scale.multiply(V1, V1);
		j2.scale.multiply(V2, V2);

		j1.orientation.rotate(V1, V1);
		j2.orientation.rotate(V2, V2);
		V1.add(V2, V1);
		j1.position.scale(weights[index * 2], V2);
		V1.add(V2, V1);
		j2.position.scale(weights[index * 2 + 1], V2);
		V1.add(V2, V1);

		glTexCoord2f(u, v);
		glNormal3f(N1.x, N1.y, N1.z);
		glVertex3f(V1.x, V1.y, V1.z);
	}

	private Vector3 mirror(int mirror, Vector3 v, Vector3 dst) {
		v.copy(dst);
		switch (mirror) {
		case 1:
			dst.x = -dst.x;
			break;
		case 2:
			dst.y = -dst.y;
			break;
		case 3:
			dst.z = -dst.z;
			break;
		}
		return dst;
	}

	public void setEnvironmentMap(TextureState environment) {
		this.environment = environment;
	}

	public TextureState getEnvirontmentMap() {
		if (environment != null)
			return environment;
		if (textures != null)
			for (TextureState t : textures.values()) {
				if (t instanceof FFXITexture && !((FFXITexture) t).used) {
					setEnvironmentMap(t);
					textures = null;
					return t;
				}
			}
		textures = null;
		return null;
	}

	@Override
	public void initializeNode() {
		getEnvirontmentMap();
		super.initializeNode();
		if (environment != null)
			environment.initialize();
	}

	@Override
	public void renderNode(float delta) {
		glActiveTextureARB(GL_TEXTURE1_ARB);
		super.renderNode(delta);
	}

	@Override
	protected void disposeNode() {
		super.disposeNode();
		if (environment != null)
			environment.dispose();
	}
}
