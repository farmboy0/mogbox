package net.sf.mogbox.renderer.engine;

public final class RenderImageData {
	public int width;
	public int height;
	public byte[] color;
	public byte[] alpha;
}
