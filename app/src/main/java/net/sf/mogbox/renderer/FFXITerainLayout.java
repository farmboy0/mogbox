package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW_MATRIX;
import static org.lwjgl.opengl.GL11.GL_PROJECTION_MATRIX;
import static org.lwjgl.opengl.GL11.glGetFloatv;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import net.sf.mogbox.renderer.engine.scene.Node;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class FFXITerainLayout extends Node {
	private static final short[] KEY_TABLE = { //
			0xE2, 0xE5, 0x06, 0xA9, 0xED, 0x26, 0xF4, 0x42, //
			0x15, 0xF4, 0x81, 0x7F, 0xDE, 0x9A, 0xDE, 0xD0, //
			0x1A, 0x98, 0x20, 0x91, 0x39, 0x49, 0x48, 0xA4, //
			0x0A, 0x9F, 0x40, 0x69, 0xEC, 0xBD, 0x81, 0x81, //
			0x8D, 0xAD, 0x10, 0xB8, 0xC1, 0x88, 0x15, 0x05, //
			0x11, 0xB1, 0xAA, 0xF0, 0x0F, 0x1E, 0x34, 0xE6, //
			0x81, 0xAA, 0xCD, 0xAC, 0x02, 0x84, 0x33, 0x0A, //
			0x19, 0x38, 0x9E, 0xE6, 0x73, 0x4A, 0x11, 0x5D, //
			0xBF, 0x85, 0x77, 0x08, 0xCD, 0xD9, 0x96, 0x0D, //
			0x79, 0x78, 0xCC, 0x35, 0x06, 0x8E, 0xF9, 0xFE, //
			0x66, 0xB9, 0x21, 0x03, 0x20, 0x29, 0x1E, 0x27, //
			0xCA, 0x86, 0x82, 0xE6, 0x45, 0x07, 0xDD, 0xA9, //
			0xB6, 0xD5, 0xA2, 0x03, 0xEC, 0xAD, 0x62, 0x45, //
			0x2D, 0xCE, 0x79, 0xBD, 0x8F, 0x2D, 0x10, 0x18, //
			0xE6, 0x0A, 0x6F, 0xAA, 0x6F, 0x46, 0x84, 0x32, //
			0x9F, 0x29, 0x2C, 0xC2, 0xF0, 0xEB, 0x18, 0x6F, //
			0xF2, 0x3A, 0xDC, 0xEA, 0x7B, 0x0C, 0x81, 0x2D, //
			0xCC, 0xEB, 0xA1, 0x51, 0x77, 0x2C, 0xFB, 0x49, //
			0xE8, 0x90, 0xF7, 0x90, 0xCE, 0x5C, 0x01, 0xF3, //
			0x5C, 0xF4, 0x41, 0xAB, 0x04, 0xE7, 0x16, 0xCC, //
			0x3A, 0x05, 0x54, 0x55, 0xDC, 0xED, 0xA4, 0xD6, //
			0xBF, 0x3F, 0x9E, 0x08, 0x93, 0xB5, 0x63, 0x38, //
			0x90, 0xF7, 0x5A, 0xF0, 0xA2, 0x5F, 0x56, 0xC8, //
			0x08, 0x70, 0xCB, 0x24, 0x16, 0xDD, 0xD2, 0x74, //
			0x95, 0x3A, 0x1A, 0x2A, 0x74, 0xC4, 0x9D, 0xEB, //
			0xAF, 0x69, 0xAA, 0x51, 0x39, 0x65, 0x94, 0xA2, //
			0x4B, 0x1F, 0x1A, 0x60, 0x52, 0x39, 0xE8, 0x23, //
			0xEE, 0x58, 0x39, 0x06, 0x3D, 0x22, 0x6A, 0x2D, //
			0xD2, 0x91, 0x25, 0xA5, 0x2E, 0x71, 0x62, 0xA5, //
			0x0B, 0xC1, 0xE5, 0x6E, 0x43, 0x49, 0x7C, 0x58, //
			0x46, 0x19, 0x9F, 0x45, 0x49, 0xC6, 0x40, 0x09, //
			0xA2, 0x99, 0x5B, 0x7B, 0x98, 0x7F, 0xA0, 0xD0, };

	private static final FloatBuffer temp = BufferUtils.createFloatBuffer(16);
	private static final float[] p = new float[16];
	private static final float[] m = new float[16];
	private static final float[] c = new float[16];

	private static void decrypt(ByteBuffer b) {
		if ((b.get(3) & 0xFF) >= 0x1B) {
			int decode_length = b.getInt(0) & 0xFFFFFF;
			int key = KEY_TABLE[~b.get(7) & 0xFF];
			int key_counter = 0;

			for (int pos = 8; pos < decode_length;) {
				int xor_length = (key >>> 4 & 7) + 16;

				if ((key & 1) == 1 && (pos + xor_length < decode_length)) {
					for (int i = 0; i < xor_length; i++) {
						b.put(pos + i, (byte) (b.get(pos + i) ^ 0xFF));
					}
				}
				key += ++key_counter;
				pos += xor_length;
			}

			int node_count = b.getInt(4) & 0xFFFFFF;

			for (int i = 0; i < node_count; i++) {
				int offset = i * 100 + 32;
				for (int j = 0; j < 16; j++) {
					b.put(offset + j, (byte) (b.get(offset + j) ^ 0x55));
				}
			}
		}
	}

	private String[] names;
	private float[] rotation;
	private float[] translation;
	private float[] scale;
	private boolean[] reverse;
	private int[][] flags;

	private Map<String, FFXITerrainModel> blocks = new HashMap<String, FFXITerrainModel>();

	public FFXITerainLayout(ByteBuffer b) throws IOException {
		decrypt(b);

		int num = b.getInt(4) & 0xffffff;

		names = new String[num];
		rotation = new float[num * 3];
		translation = new float[num * 3];
		scale = new float[num * 3];
		reverse = new boolean[num];
		flags = new int[num][12];

		char[] c = new char[16];

		b.position(32);

		for (int n = 0; n < num; n++) {
			for (int i = 0; i < c.length; i++)
				c[i] = (char) (b.get() & 0xFF);
			names[n] = new String(c);
			translation[n * 3] = b.getFloat();
			translation[n * 3 + 1] = b.getFloat();
			translation[n * 3 + 2] = b.getFloat();
			rotation[n * 3] = b.getFloat() * 57.29577951308232f;
			rotation[n * 3 + 1] = b.getFloat() * 57.29577951308232f;
			rotation[n * 3 + 2] = b.getFloat() * 57.29577951308232f;
			scale[n * 3] = b.getFloat();
			scale[n * 3 + 1] = b.getFloat();
			scale[n * 3 + 2] = b.getFloat();
			reverse[n] = scale[n * 3] * scale[n * 3 + 1] * scale[n * 3 + 2] < 0;
			for (int i = 0; i < flags[n].length; i++)
				flags[n][i] = b.getInt();
		}
	}

	@Override
	public void add(Node node) {
		if (node instanceof FFXITerrainModel) {
			FFXITerrainModel model = (FFXITerrainModel) node;
			blocks.put(model.getName(), model);
		} else {
			super.add(node);
		}
	}

	@Override
	protected void initializeNode() {
		for (Node n : blocks.values()) {
			if (n != null)
				n.initialize();
		}
	}

	@Override
	protected void disposeNode() {
		for (Node n : blocks.values()) {
			if (n != null)
				n.dispose();
		}
	}

	@Override
	protected void renderNode(float delta) {
		GL11.glPushAttrib(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glDisable(GL11.GL_ALPHA_TEST);

		for (int i = 0; i < names.length; i++) {
			FFXITerrainModel node = blocks.get(names[i]);
			if (node == null)
				continue;

			GL11.glPushMatrix();
			GL11.glTranslatef(translation[i * 3], translation[i * 3 + 1], translation[i * 3 + 2]);
			GL11.glRotatef(rotation[i * 3 + 2], 0, 0, 1);
			GL11.glRotatef(rotation[i * 3 + 1], 0, 1, 0);
			GL11.glRotatef(rotation[i * 3], 1, 0, 0);
			GL11.glScalef(scale[i * 3], scale[i * 3 + 1], scale[i * 3 + 2]);

			if (!checkBounds(node.getBounds())) {
				GL11.glFrontFace(reverse[i] ? GL11.GL_CCW : GL11.GL_CW);
				node.render(delta);
			}

			GL11.glPopMatrix();
		}

		GL11.glFrontFace(GL11.GL_CW);

		GL11.glPopAttrib();
	}

	private boolean checkBounds(float[] bounds) {
		glGetFloatv(GL_PROJECTION_MATRIX, temp);
		temp.get(p);
		temp.clear();
		glGetFloatv(GL_MODELVIEW_MATRIX, temp);
		temp.get(m);
		temp.clear();
		c[0] = m[0] * p[0] + m[1] * p[4] + m[2] * p[8] + m[3] * p[12];
		c[1] = m[0] * p[1] + m[1] * p[5] + m[2] * p[9] + m[3] * p[13];
		c[2] = m[0] * p[2] + m[1] * p[6] + m[2] * p[10] + m[3] * p[14];
		c[3] = m[0] * p[3] + m[1] * p[7] + m[2] * p[11] + m[3] * p[15];
		c[4] = m[4] * p[0] + m[5] * p[4] + m[6] * p[8] + m[7] * p[12];
		c[5] = m[4] * p[1] + m[5] * p[5] + m[6] * p[9] + m[7] * p[13];
		c[6] = m[4] * p[2] + m[5] * p[6] + m[6] * p[10] + m[7] * p[14];
		c[7] = m[4] * p[3] + m[5] * p[7] + m[6] * p[11] + m[7] * p[15];
		c[8] = m[8] * p[0] + m[9] * p[4] + m[10] * p[8] + m[11] * p[12];
		c[9] = m[8] * p[1] + m[9] * p[5] + m[10] * p[9] + m[11] * p[13];
		c[10] = m[8] * p[2] + m[9] * p[6] + m[10] * p[10] + m[11] * p[14];
		c[11] = m[8] * p[3] + m[9] * p[7] + m[10] * p[11] + m[11] * p[15];
		c[12] = m[12] * p[0] + m[13] * p[4] + m[14] * p[8] + m[15] * p[12];
		c[13] = m[12] * p[1] + m[13] * p[5] + m[14] * p[9] + m[15] * p[13];
		c[14] = m[12] * p[2] + m[13] * p[6] + m[14] * p[10] + m[15] * p[14];
		c[15] = m[12] * p[3] + m[13] * p[7] + m[14] * p[11] + m[15] * p[15];

		float x, y, z, w;
		float xMin, xMax, yMin, yMax, zMin, zMax;

		w = bounds[0] * c[3] + bounds[2] * c[7] + bounds[4] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		xMin = (bounds[0] * c[0] + bounds[2] * c[4] + bounds[4] * c[8] + c[12]) / w;
		yMin = (bounds[0] * c[1] + bounds[2] * c[5] + bounds[4] * c[9] + c[13]) / w;
		zMin = (bounds[0] * c[2] + bounds[2] * c[6] + bounds[4] * c[10] + c[14]) / w;
		xMax = xMin;
		yMax = yMin;
		zMax = zMin;

		w = bounds[1] * c[3] + bounds[2] * c[7] + bounds[4] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[1] * c[0] + bounds[2] * c[4] + bounds[4] * c[8] + c[12]) / w;
		y = (bounds[1] * c[1] + bounds[2] * c[5] + bounds[4] * c[9] + c[13]) / w;
		z = (bounds[1] * c[2] + bounds[2] * c[6] + bounds[4] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[1] * c[3] + bounds[2] * c[7] + bounds[5] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[1] * c[0] + bounds[2] * c[4] + bounds[5] * c[8] + c[12]) / w;
		y = (bounds[1] * c[1] + bounds[2] * c[5] + bounds[5] * c[9] + c[13]) / w;
		z = (bounds[1] * c[2] + bounds[2] * c[6] + bounds[5] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[0] * c[3] + bounds[2] * c[7] + bounds[5] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[0] * c[0] + bounds[2] * c[4] + bounds[5] * c[8] + c[12]) / w;
		y = (bounds[0] * c[1] + bounds[2] * c[5] + bounds[5] * c[9] + c[13]) / w;
		z = (bounds[0] * c[2] + bounds[2] * c[6] + bounds[5] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[0] * c[3] + bounds[3] * c[7] + bounds[4] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[0] * c[0] + bounds[3] * c[4] + bounds[4] * c[8] + c[12]) / w;
		y = (bounds[0] * c[1] + bounds[3] * c[5] + bounds[4] * c[9] + c[13]) / w;
		z = (bounds[0] * c[2] + bounds[3] * c[6] + bounds[4] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[1] * c[3] + bounds[3] * c[7] + bounds[4] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[1] * c[0] + bounds[3] * c[4] + bounds[4] * c[8] + c[12]) / w;
		y = (bounds[1] * c[1] + bounds[3] * c[5] + bounds[4] * c[9] + c[13]) / w;
		z = (bounds[1] * c[2] + bounds[3] * c[6] + bounds[4] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[1] * c[3] + bounds[3] * c[7] + bounds[5] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[1] * c[0] + bounds[3] * c[4] + bounds[5] * c[8] + c[12]) / w;
		y = (bounds[1] * c[1] + bounds[3] * c[5] + bounds[5] * c[9] + c[13]) / w;
		z = (bounds[1] * c[2] + bounds[3] * c[6] + bounds[5] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		w = bounds[0] * c[3] + bounds[3] * c[7] + bounds[5] * c[11] + c[15];
		if (w < 0)
			w = 0 - w;
		else if (w == 0)
			w = 1;
		x = (bounds[0] * c[0] + bounds[3] * c[4] + bounds[5] * c[8] + c[12]) / w;
		y = (bounds[0] * c[1] + bounds[3] * c[5] + bounds[5] * c[9] + c[13]) / w;
		z = (bounds[0] * c[2] + bounds[3] * c[6] + bounds[5] * c[10] + c[14]) / w;
		if (x < xMin)
			xMin = x;
		if (x > xMax)
			xMax = x;
		if (y < yMin)
			yMin = y;
		if (y > yMax)
			yMax = y;
		if (z < zMin)
			zMin = z;
		if (z > zMax)
			zMax = z;

		return xMax < -1 || xMin > 1 || yMax < -1 || yMin > 1 || zMax < -1 || zMin > 1;
	}
}
