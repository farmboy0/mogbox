/* 
 * FFXITerrainModel.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.state.MaterialState;
import net.sf.mogbox.renderer.engine.scene.state.RenderState;
import net.sf.mogbox.renderer.engine.scene.state.ShaderState;
import net.sf.mogbox.renderer.engine.scene.state.TextureState;

import org.lwjgl.opengl.GL11;

public class FFXITerrainModel extends Node {
	private static final Logger log = Logger.getLogger(FFXITerrainModel.class.getName());
	private static final MaterialState MATERIAL = new MaterialState();
	private static final ShaderState SHADER;
	private static final ShaderState SHADER_ALPHA;
	static {
		MATERIAL.setAbmientAndDiffuse(1);
		ShaderState temp = null;
		try {
			URL vertexshader = FFXIModel.class.getResource("terrain-vert.arb");
			URL fragmentshader = FFXIModel.class.getResource("terrain-frag.arb");
			temp = new ShaderState(vertexshader, fragmentshader);
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
		}
		if (temp != null)
			SHADER = temp;
		else
			SHADER = null;

		try {
			URL vertexshader = FFXIModel.class.getResource("terrain-alpha-vert.arb");
			URL fragmentshader = FFXIModel.class.getResource("terrain-alpha-frag.arb");
			temp = new ShaderState(vertexshader, fragmentshader);
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
		}
		if (temp != null)
			SHADER_ALPHA = temp;
		else
			SHADER_ALPHA = null;
	}

	private static final short[] KEY_TABLE = { 0xE2, 0xE5, 0x06, 0xA9, 0xED, 0x26, 0xF4, 0x42, 0x15, 0xF4, 0x81, 0x7F,
			0xDE, 0x9A, 0xDE, 0xD0, 0x1A, 0x98, 0x20, 0x91, 0x39, 0x49, 0x48, 0xA4, 0x0A, 0x9F, 0x40, 0x69, 0xEC, 0xBD,
			0x81, 0x81, 0x8D, 0xAD, 0x10, 0xB8, 0xC1, 0x88, 0x15, 0x05, 0x11, 0xB1, 0xAA, 0xF0, 0x0F, 0x1E, 0x34, 0xE6,
			0x81, 0xAA, 0xCD, 0xAC, 0x02, 0x84, 0x33, 0x0A, 0x19, 0x38, 0x9E, 0xE6, 0x73, 0x4A, 0x11, 0x5D, 0xBF, 0x85,
			0x77, 0x08, 0xCD, 0xD9, 0x96, 0x0D, 0x79, 0x78, 0xCC, 0x35, 0x06, 0x8E, 0xF9, 0xFE, 0x66, 0xB9, 0x21, 0x03,
			0x20, 0x29, 0x1E, 0x27, 0xCA, 0x86, 0x82, 0xE6, 0x45, 0x07, 0xDD, 0xA9, 0xB6, 0xD5, 0xA2, 0x03, 0xEC, 0xAD,
			0x62, 0x45, 0x2D, 0xCE, 0x79, 0xBD, 0x8F, 0x2D, 0x10, 0x18, 0xE6, 0x0A, 0x6F, 0xAA, 0x6F, 0x46, 0x84, 0x32,
			0x9F, 0x29, 0x2C, 0xC2, 0xF0, 0xEB, 0x18, 0x6F, 0xF2, 0x3A, 0xDC, 0xEA, 0x7B, 0x0C, 0x81, 0x2D, 0xCC, 0xEB,
			0xA1, 0x51, 0x77, 0x2C, 0xFB, 0x49, 0xE8, 0x90, 0xF7, 0x90, 0xCE, 0x5C, 0x01, 0xF3, 0x5C, 0xF4, 0x41, 0xAB,
			0x04, 0xE7, 0x16, 0xCC, 0x3A, 0x05, 0x54, 0x55, 0xDC, 0xED, 0xA4, 0xD6, 0xBF, 0x3F, 0x9E, 0x08, 0x93, 0xB5,
			0x63, 0x38, 0x90, 0xF7, 0x5A, 0xF0, 0xA2, 0x5F, 0x56, 0xC8, 0x08, 0x70, 0xCB, 0x24, 0x16, 0xDD, 0xD2, 0x74,
			0x95, 0x3A, 0x1A, 0x2A, 0x74, 0xC4, 0x9D, 0xEB, 0xAF, 0x69, 0xAA, 0x51, 0x39, 0x65, 0x94, 0xA2, 0x4B, 0x1F,
			0x1A, 0x60, 0x52, 0x39, 0xE8, 0x23, 0xEE, 0x58, 0x39, 0x06, 0x3D, 0x22, 0x6A, 0x2D, 0xD2, 0x91, 0x25, 0xA5,
			0x2E, 0x71, 0x62, 0xA5, 0x0B, 0xC1, 0xE5, 0x6E, 0x43, 0x49, 0x7C, 0x58, 0x46, 0x19, 0x9F, 0x45, 0x49, 0xC6,
			0x40, 0x09, 0xA2, 0x99, 0x5B, 0x7B, 0x98, 0x7F, 0xA0, 0xD0, };

	private static final short[] KEY_TABLE2 = { 0xB8, 0xC5, 0xF7, 0x84, 0xE4, 0x5A, 0x23, 0x7B, 0xC8, 0x90, 0x1D, 0xF6,
			0x5D, 0x09, 0x51, 0xC1, 0x07, 0x24, 0xEF, 0x5B, 0x1D, 0x73, 0x90, 0x08, 0xA5, 0x70, 0x1C, 0x22, 0x5F, 0x6B,
			0xEB, 0xB0, 0x06, 0xC7, 0x2A, 0x3A, 0xD2, 0x66, 0x81, 0xDB, 0x41, 0x62, 0xF2, 0x97, 0x17, 0xFE, 0x05, 0xEF,
			0xA3, 0xDC, 0x22, 0xB3, 0x45, 0x70, 0x3E, 0x18, 0x2D, 0xB4, 0xBA, 0x0A, 0x65, 0x1D, 0x87, 0xC3, 0x12, 0xCE,
			0x8F, 0x9D, 0xF7, 0x0D, 0x50, 0x24, 0x3A, 0xF3, 0xCA, 0x70, 0x6B, 0x67, 0x9C, 0xB2, 0xC2, 0x4D, 0x6A, 0x0C,
			0xA8, 0xFA, 0x81, 0xA6, 0x79, 0xEB, 0xBE, 0xFE, 0x89, 0xB7, 0xAC, 0x7F, 0x65, 0x43, 0xEC, 0x56, 0x5B, 0x35,
			0xDA, 0x81, 0x3C, 0xAB, 0x6D, 0x28, 0x60, 0x2C, 0x5F, 0x31, 0xEB, 0xDF, 0x8E, 0x0F, 0x4F, 0xFA, 0xA3, 0xDA,
			0x12, 0x7E, 0xF1, 0xA5, 0xD2, 0x22, 0xA0, 0x0C, 0x86, 0x8C, 0x0A, 0x0C, 0x06, 0xC7, 0x65, 0x18, 0xCE, 0xF2,
			0xA3, 0x68, 0xFE, 0x35, 0x96, 0x95, 0xA6, 0xFA, 0x58, 0x63, 0x41, 0x59, 0xEA, 0xDD, 0x7F, 0xD3, 0x1B, 0xA8,
			0x48, 0x44, 0xAB, 0x91, 0xFD, 0x13, 0xB1, 0x68, 0x01, 0xAC, 0x3A, 0x11, 0x78, 0x30, 0x33, 0xD8, 0x4E, 0x6A,
			0x89, 0x05, 0x7B, 0x06, 0x8E, 0xB0, 0x86, 0xFD, 0x9F, 0xD7, 0x48, 0x54, 0x04, 0xAE, 0xF3, 0x06, 0x17, 0x36,
			0x53, 0x3F, 0xA8, 0x11, 0x53, 0xCA, 0xA1, 0x95, 0xC2, 0xCD, 0xE6, 0x1F, 0x57, 0xB4, 0x7F, 0xAA, 0xF3, 0x6B,
			0xF9, 0xA0, 0x27, 0xD0, 0x09, 0xEF, 0xF6, 0x68, 0x73, 0x60, 0xDC, 0x50, 0x2A, 0x25, 0x0F, 0x77, 0xB9, 0xB0,
			0x04, 0x0B, 0xE1, 0xCC, 0x35, 0x31, 0x84, 0xE6, 0x22, 0xF9, 0xC2, 0xAB, 0x95, 0x91, 0x61, 0xD9, 0x2B, 0xB9,
			0x72, 0x4E, 0x10, 0x76, 0x31, 0x66, 0x0A, 0x0B, 0x2E, 0x83 };

	private static void decrypt(ByteBuffer b) {
		int length = b.getInt(0) & 0xFFFFFF;
		int key1 = b.get(5) & 0xFF ^ 0xF0;

		if ((b.get(3) & 0xFF) >= 5) {
			int key2 = KEY_TABLE[key1];
			int counter = 0;

			for (int i = 8; i < length; i++) {
				int x = (key2 & 0xFF) * 0x0101;
				key2 += ++counter;

				b.put(i, (byte) (b.get(i) ^ (x >>> (key2 & 7))));
				key2 += ++counter;
			}
		}

		if (b.getShort(6) == -1) {
			int key2 = KEY_TABLE2[key1];
			length = ((length - 8) & ~0xf) >> 1;

			int offset1 = b.position() + 8;
			int offset2 = offset1 + length;
			for (int i = 0; i < length; i += 8) {
				if ((key2 & 1) == 1) {
					long temp = b.getLong(offset1);
					b.putLong(offset1, b.getLong(offset2));
					b.putLong(offset2, temp);
				}
				key1 += 9;
				key2 += key1;
				offset1 += 8;
				offset2 += 8;
			}
		}

		b.position(b.position() + 8);
	}

	private String name;
	private String artist;

	private boolean strip;
	private short[][] meshes;
	private int[] meshFlags;
	private float[][] meshVerts;
	private float[][] meshNormals;
	private TextureState[] meshTextures;
	private float[][] meshTexCoords;
	private float[][] meshColors;

	private float[] bounds = new float[6];

	public FFXITerrainModel(ByteBuffer b, Map<String, TextureState> textures) throws IOException {
		addState(MATERIAL);

		decrypt(b);

		char[] c = new char[8];
		for (int i = 0; i < c.length; i++)
			c[i] = (char) (b.get() & 0xFF);
		artist = new String(c).trim();

		c = new char[16];
		for (int i = 0; i < c.length; i++)
			c[i] = (char) (b.get() & 0xFF);
		name = new String(c);
		int objectCount = b.getInt();
		if (objectCount > 1 || objectCount < 0)
			log.warning("Object count greater than 1: " + objectCount);
		bounds[0] = b.getFloat();
		bounds[1] = b.getFloat();
		bounds[2] = b.getFloat();
		bounds[3] = b.getFloat();
		bounds[4] = b.getFloat();
		bounds[5] = b.getFloat();
		b.getInt();
		for (int o = 0; o < objectCount; o++) {
			int numMeshes = b.getInt();
			b.position(b.position() + 24); // bounding box
			int unknown = b.getInt();
			strip = unknown != 0;

			meshes = new short[numMeshes][];
			meshFlags = new int[numMeshes];
			meshVerts = new float[numMeshes][];
			meshNormals = new float[numMeshes][];
			meshTextures = new TextureState[numMeshes];
			meshTexCoords = new float[numMeshes][];
			meshColors = new float[numMeshes][];

			for (int n = 0; n < numMeshes; n++) {
				b.position((b.position() + 3) & ~0x3); // dword align
				for (int i = 0; i < c.length; i++)
					c[i] = (char) (b.get() & 0xFF);
				TextureState tex = textures.get(new String(c));
				int numVerts = b.getShort() & 0xFFFF;
				int flags = b.getShort() & 0xFFFF;
				float[] verts = new float[numVerts * 3];
				float[] normals = new float[numVerts * 3];
				float[] texCoords = new float[numVerts * 2];
				float[] colors = new float[numVerts * 4];
				for (int i = 0; i < numVerts; i++) {
					verts[i * 3] = b.getFloat();
					verts[i * 3 + 1] = b.getFloat();
					verts[i * 3 + 2] = b.getFloat();
					if (!strip) {
						b.getFloat();
						b.getFloat();
						b.getFloat();
					}
					normals[i * 3] = b.getFloat();
					normals[i * 3 + 1] = b.getFloat();
					normals[i * 3 + 2] = b.getFloat();
					colors[i * 4] = (b.get() & 0xFF) / 128.0f;
					colors[i * 4 + 1] = (b.get() & 0xFF) / 128.0f;
					colors[i * 4 + 2] = (b.get() & 0xFF) / 128.0f;
					colors[i * 4 + 3] = (b.get() & 0xFF) / 128.0f;
					texCoords[i * 2] = b.getFloat();
					texCoords[i * 2 + 1] = b.getFloat();
				}
				int numTriangles = b.getShort() & 0xFFFF;
				short[] mesh = new short[numTriangles];
				if (numTriangles > 0) {
					b.getShort();
					for (int i = 0; i < mesh.length; i++)
						mesh[i] = b.getShort();
				}
				meshes[n] = mesh;
				meshFlags[n] = flags;
				meshVerts[n] = verts;
				meshNormals[n] = normals;
				meshTextures[n] = tex;
				meshTexCoords[n] = texCoords;
				meshColors[n] = colors;
			}
		}
	}

	public String getName() {
		return name;
	}

	public String getArtistID() {
		return artist;
	}

	public float[] getBounds() {
		return bounds;
	}

	@Override
	protected void initializeNode() {
		if (meshTextures == null)
			return;
		for (RenderState s : meshTextures) {
			if (s != null)
				s.initialize();
		}
		if (SHADER != null)
			SHADER.initialize();
		if (SHADER_ALPHA != null)
			SHADER_ALPHA.initialize();
	}

	@Override
	protected void disposeNode() {
		if (meshTextures == null)
			return;
		for (RenderState s : meshTextures) {
			if (s != null)
				s.dispose();
		}
		if (SHADER != null)
			SHADER.dispose();
		if (SHADER_ALPHA != null)
			SHADER_ALPHA.dispose();
	}

	@Override
	protected void renderNode(float delta) {
		if (meshes == null)
			return;

		GL11.glCullFace(GL11.GL_BACK);

		for (int i = 0; i < meshes.length; i++) {
			short[] mesh = meshes[i];
			if (mesh == null)
				continue;
			int flags = meshFlags[i];
			float[] verts = meshVerts[i];
			float[] normals = meshNormals[i];
			TextureState tex = meshTextures[i];
			float[] texCoords = meshTexCoords[i];
			float[] colors = meshColors[i];

			if ((flags & 0x2000) != 0)
				GL11.glDisable(GL11.GL_CULL_FACE);
			else
				GL11.glEnable(GL11.GL_CULL_FACE);

			if ((flags & 0x8000) != 0) {
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				SHADER_ALPHA.apply();
			} else {
				GL11.glDisable(GL11.GL_BLEND);
				SHADER.apply();
			}

			if (tex != null)
				tex.apply();

			GL11.glBegin(strip ? GL11.GL_TRIANGLE_STRIP : GL11.GL_TRIANGLES);
			for (int j = 0; j < mesh.length; j++) {
				int id = mesh[j];
				GL11.glNormal3f(normals[id * 3], normals[id * 3 + 1], normals[id * 3 + 2]);
				GL11.glTexCoord2f(texCoords[id * 2], texCoords[id * 2 + 1]);
				GL11.glColor4f(colors[id * 4 + 2], colors[id * 4 + 1], colors[id * 4], colors[id * 4 + 3]);
				GL11.glVertex3f(verts[id * 3], verts[id * 3 + 1], verts[id * 3 + 2]);
			}
			GL11.glEnd();

			if (tex != null)
				tex.clear();

			if ((flags & 0x8000) != 0) {
				SHADER_ALPHA.clear();
			} else {
				SHADER.clear();
			}

		}

		GL11.glColor4f(1, 1, 1, 1);
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
}
