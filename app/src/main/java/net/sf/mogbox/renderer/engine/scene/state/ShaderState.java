/* 
 * ShaderState.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.scene.state;

import static org.lwjgl.opengl.ARBFragmentProgram.GL_FRAGMENT_PROGRAM_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.GL_PROGRAM_ERROR_POSITION_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.GL_PROGRAM_ERROR_STRING_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.GL_PROGRAM_FORMAT_ASCII_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.GL_VERTEX_PROGRAM_ARB;
import static org.lwjgl.opengl.ARBVertexProgram.glBindProgramARB;
import static org.lwjgl.opengl.ARBVertexProgram.glDeleteProgramsARB;
import static org.lwjgl.opengl.ARBVertexProgram.glGenProgramsARB;
import static org.lwjgl.opengl.ARBVertexProgram.glGetProgramivARB;
import static org.lwjgl.opengl.ARBVertexProgram.glProgramStringARB;
import static org.lwjgl.opengl.GL11.GL_EXTENSIONS;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetIntegerv;
import static org.lwjgl.opengl.GL11.glGetString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBVertexProgram;

public class ShaderState extends RenderState {
	private static Logger log = Logger.getLogger(ShaderState.class.getName());

	private URL vertexProgram;
	private URL fragmentProgram;

	private static boolean checkedVertexProgram = false;
	private static boolean checkedFragmentProgram = false;
	private static boolean vertexProgramSupported;
	private static boolean fragmentProgramSupported;

	private boolean vertexProgramNative;
	private boolean fragmentProgramNative;

	private IntBuffer progs = BufferUtils.createIntBuffer(2);

	public ShaderState(URL vertexProgram, URL fragmentProgram) throws IOException {
		this.vertexProgram = vertexProgram;
		this.fragmentProgram = fragmentProgram;
	}

	public boolean isVertexProgramNative() {
		return vertexProgramNative;
	}

	public boolean isFragmentProgramNative() {
		return fragmentProgramNative;
	}

	@Override
	public int getType() {
		return SHADER;
	}

	@Override
	protected void initializeState() {
		if (vertexProgram != null && !vertexShaderSupported())
			return;
		if (fragmentProgram != null && !fragmentShaderSupported())
			return;

		IntBuffer temp = BufferUtils.createIntBuffer(16);

		if (vertexProgram != null) {
			try {
				ByteBuffer program = readProgram(vertexProgram);
				glEnable(GL_VERTEX_PROGRAM_ARB);
				progs.position(0).limit(1);
				glGenProgramsARB(progs);
				glBindProgramARB(GL_VERTEX_PROGRAM_ARB, progs.get(0));
				glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB, program);

				temp.position(0);
				glGetIntegerv(GL_PROGRAM_ERROR_POSITION_ARB, temp);
				temp.position(1);
				glGetProgramivARB(GL_VERTEX_PROGRAM_ARB, ARBVertexProgram.GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB, temp);
				if (temp.get(0) != -1) {
					log.log(Level.WARNING, "Error in Vertex Program\n"
							+ glGetString(ARBVertexProgram.GL_PROGRAM_ERROR_STRING_ARB).trim());
					progs.position(0).limit(1);
					ARBVertexProgram.glDeleteProgramsARB(progs);
					progs.put(0, 0);
				} else {
					vertexProgramNative = temp.get(1) == 1;
				}

				glBindProgramARB(GL_VERTEX_PROGRAM_ARB, 0);
				glDisable(GL_VERTEX_PROGRAM_ARB);
			} catch (IOException e) {
			}
		}

		if (fragmentProgram != null) {
			try {
				ByteBuffer program = readProgram(fragmentProgram);
				glEnable(GL_FRAGMENT_PROGRAM_ARB);
				progs.position(1).limit(2);
				glGenProgramsARB(progs);
				progs.clear();
				glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, progs.get(1));
				glProgramStringARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB, program);

				temp.position(0);
				glGetIntegerv(GL_PROGRAM_ERROR_POSITION_ARB, temp);
				temp.position(1);
				glGetProgramivARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB, temp);
				if (temp.get(0) != -1) {
					log.log(Level.WARNING,
							"Error in Fragment Program\n" + glGetString(GL_PROGRAM_ERROR_STRING_ARB).trim());
					progs.position(1).limit(2);
					glDeleteProgramsARB(progs);
					progs.put(1, 0);
				} else {
					vertexProgramNative = temp.get(1) == 1;
				}

				glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, 0);
				glDisable(GL_FRAGMENT_PROGRAM_ARB);
			} catch (IOException e) {
			}
		}

		progs.clear();
	}

	private ByteBuffer readProgram(URL url) throws IOException {
		StringBuilder program = new StringBuilder();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			while ((line = in.readLine()) != null) {
				program.append(line).append("\n");
			}
		} finally {
			if (in != null)
				in.close();
		}
		ByteBuffer buffer = BufferUtils.createByteBuffer(program.length());
		for (int i = 0; i < program.length(); i++) {
			buffer.put((byte) (program.charAt(i) & 0xFF));
		}
		buffer.flip();
		return buffer;
	}

	public static boolean vertexShaderSupported() {
		if (!checkedVertexProgram) {
			checkedVertexProgram = true;
			String extensions = glGetString(GL_EXTENSIONS);
			vertexProgramSupported = extensions.indexOf("GL_ARB_vertex_program") != -1;
		}
		return vertexProgramSupported;
	}

	public static boolean fragmentShaderSupported() {
		if (!checkedFragmentProgram) {
			checkedVertexProgram = true;
			String extensions = glGetString(GL_EXTENSIONS);
			fragmentProgramSupported = extensions.indexOf("GL_ARB_fragment_program") != -1;
		}
		return fragmentProgramSupported;
	}

	@Override
	public void applyState() {
		if (vertexShaderSupported() && progs.get(0) != 0) {
			glEnable(GL_VERTEX_PROGRAM_ARB);
			glBindProgramARB(GL_VERTEX_PROGRAM_ARB, progs.get(0));
		}
		if (fragmentShaderSupported() && progs.get(1) != 0) {
			glEnable(GL_FRAGMENT_PROGRAM_ARB);
			glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, progs.get(1));
		}
	}

	@Override
	protected void clearState() {
		if (vertexShaderSupported()) {
			glDisable(GL_VERTEX_PROGRAM_ARB);
		}
		if (fragmentShaderSupported()) {
			glDisable(GL_FRAGMENT_PROGRAM_ARB);
		}
	}

	@Override
	protected void disposeState() {
		glDeleteProgramsARB(progs);
		progs.put(0).put(0).clear();
	}
}
