/* 
 * FFXITriangleStrip.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;

import java.nio.ByteBuffer;

public class FFXITriangleStrip extends FFXIMesh {
	public FFXITriangleStrip(FFXIModel model, ByteBuffer b) {
		super(model);
		int num = (b.getShort() & 0xFFFF) + 2;
		vertices = new short[num];
		uv = new float[num * 2];
		if (num > 0) {
			vertices[0] = b.getShort();
			vertices[1] = b.getShort();
			vertices[2] = b.getShort();
			uv[0] = b.getFloat();
			uv[1] = b.getFloat();
			uv[2] = b.getFloat();
			uv[3] = b.getFloat();
			uv[4] = b.getFloat();
			uv[5] = b.getFloat();
			for (int i = 3; i < num; i++) {
				vertices[i] = b.getShort();
				uv[i * 2] = b.getFloat();
				uv[i * 2 + 1] = b.getFloat();
			}
		}
	}

	@Override
	protected void begin() {
		glBegin(GL_TRIANGLE_STRIP);
	}

	@Override
	protected void end() {
		glEnd();
	}
}