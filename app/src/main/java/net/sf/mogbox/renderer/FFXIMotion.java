/* 
 * FFXIMotion.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import java.nio.ByteBuffer;

import net.sf.mogbox.renderer.engine.scene.Motion;
import net.sf.mogbox.renderer.engine.vecmath.Joint;

public class FFXIMotion implements Motion {
	private Joint[][] frames;
	private float framerate;

	public FFXIMotion(ByteBuffer b) {
		b.getShort(); // Unknown [Null Padding?]
		int boneCount = b.getShort();
		int frameCount = b.getShort();
		framerate = b.getFloat() * 30;
		frames = new Joint[frameCount][boneCount];

		Joint[] base = new Joint[boneCount];
		int[][] offsets = new int[boneCount][10];
		for (int i = 0; i < boneCount; i++) {
			Joint bone = base[i] = new Joint();
			bone.parent = b.getInt() & 0xFFFF;

			offsets[i][0] = b.getInt() & 0x7FFFFFFF;
			offsets[i][1] = b.getInt() & 0x7FFFFFFF;
			offsets[i][2] = b.getInt() & 0x7FFFFFFF;
			offsets[i][3] = b.getInt() & 0x7FFFFFFF;
			bone.orientation.x = b.getFloat();
			bone.orientation.y = b.getFloat();
			bone.orientation.z = b.getFloat();
			bone.orientation.w = b.getFloat();

			offsets[i][4] = b.getInt() & 0x7FFFFFFF;
			offsets[i][5] = b.getInt() & 0x7FFFFFFF;
			offsets[i][6] = b.getInt() & 0x7FFFFFFF;
			bone.position.x = b.getFloat();
			bone.position.y = b.getFloat();
			bone.position.z = b.getFloat();

			offsets[i][7] = b.getInt() & 0x7FFFFFFF;
			offsets[i][8] = b.getInt() & 0x7FFFFFFF;
			offsets[i][9] = b.getInt() & 0x7FFFFFFF;
			bone.scale.x = b.getFloat();
			bone.scale.y = b.getFloat();
			bone.scale.z = b.getFloat();
		}
		for (int i = 0; i < frames.length; i++) {
			for (int j = 0; j < frames[i].length; j++) {
				Joint bone = frames[i][j] = new Joint(base[j]);
				int[] offs = offsets[j];
				if (offs[0] > 0)
					bone.orientation.x = b.getFloat((offs[0] + i) * 4 + 10);
				if (offs[1] > 0)
					bone.orientation.y = b.getFloat((offs[1] + i) * 4 + 10);
				if (offs[2] > 0)
					bone.orientation.z = b.getFloat((offs[2] + i) * 4 + 10);
				if (offs[3] > 0)
					bone.orientation.w = b.getFloat((offs[3] + i) * 4 + 10);

				if (offs[4] > 0)
					bone.position.x = b.getFloat((offs[4] + i) * 4 + 10);
				if (offs[5] > 0)
					bone.position.y = b.getFloat((offs[5] + i) * 4 + 10);
				if (offs[6] > 0)
					bone.position.z = b.getFloat((offs[6] + i) * 4 + 10);

				if (offs[7] > 0)
					bone.scale.x = b.getFloat((offs[7] + i) * 4 + 10);
				if (offs[8] > 0)
					bone.scale.y = b.getFloat((offs[8] + i) * 4 + 10);
				if (offs[9] > 0)
					bone.scale.z = b.getFloat((offs[9] + i) * 4 + 10);
			}
		}
	}

	public Joint[] apply(float t, Joint[] src, Joint[] dst) {
		t *= framerate;
		int f1 = (int) t;
		int f2 = f1 + 1;
		if (f1 >= frames.length)
			f1 = frames.length - 1;
		if (f2 >= frames.length)
			f2 = frames.length - 1;
		float delta = t % 1;
		Joint temp = new Joint();
		Joint[] frame1 = frames[f1];
		Joint[] frame2 = frames[f2];
		for (int i = 0; i < frame1.length; i++) {
			Joint j = frame2[i];
			Joint d = dst[j.parent];
			Joint s = src[j.parent];
			j.orientation.concat(s.orientation, temp.orientation);
			j.position.add(s.position, temp.position);
			j.scale.multiply(s.scale, temp.scale);
			j = frame1[i];
			j.orientation.concat(s.orientation, d.orientation);
			j.position.add(s.position, d.position);
			j.scale.multiply(s.scale, d.scale);
			Joint.interpolate(d, temp, delta, d);
		}
		return dst;
	}

	public float getDuration() {
		return (frames.length - 1) / framerate;
	}
}
