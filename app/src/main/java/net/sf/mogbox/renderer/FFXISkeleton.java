/* 
 * FFXISkeleton.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer;

import java.nio.ByteBuffer;

import net.sf.mogbox.renderer.engine.scene.Node;
import net.sf.mogbox.renderer.engine.scene.SkeletonNode;
import net.sf.mogbox.renderer.engine.vecmath.Joint;
import net.sf.mogbox.renderer.engine.vecmath.Quaternion;
import net.sf.mogbox.renderer.engine.vecmath.Vector3;

public class FFXISkeleton extends SkeletonNode {
	private boolean prioritize = false;

	public FFXISkeleton(ByteBuffer b) {
		b.getShort(); // Unknown [Null Padding?]
		bind = new Joint[b.getShort()];
		for (int i = 0; i < bind.length; i++) {
			int parent = b.get() & 0xFF;
			b.get(); // 0=Children; 1=No Children
			float x = b.getFloat(), y = b.getFloat(), z = b.getFloat(), w = b.getFloat();
			Quaternion orient = new Quaternion(x, y, z, w);
			Vector3 pos = new Vector3(b.getFloat(), b.getFloat(), b.getFloat());

			bind[i] = new Joint(parent, pos, orient);
		}
		// 4592 Bytes of unknown data follow each skeleton
	}

	@Override
	public void add(Node node) {
		prioritize = true;
		super.add(node);
	}

	@Override
	public void remove(Node node) {
		prioritize = true;
		super.remove(node);
	}

	@Override
	protected void updateNode(float delta) {
		super.updateNode(delta);
		if (prioritize) {
			prioritize = false;
			int[] priorities = new int[16];
			for (Node n : children) {
				if (n == null)
					continue;
				for (Node c : n.getChildren()) {
					if (c instanceof FFXIModel) {
						FFXIModel model = (FFXIModel) c;
						priorities[model.slot] = (byte) Math.max(priorities[model.slot], model.priority);
					}
				}
			}
			for (Node n : children) {
				if (n != null) {
					for (Node c : n.getChildren()) {
						if (c instanceof FFXIModel) {
							((FFXIModel) c).limits = priorities;
						}
					}
				}
			}
		}
	}
}
