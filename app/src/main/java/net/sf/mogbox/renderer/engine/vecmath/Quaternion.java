/* 
 * Quaternion.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine.vecmath;

public class Quaternion extends Vector4 {
	public Quaternion() {
		super(0, 0, 0, 1);
	}

	public Quaternion(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		w = 1 - (x * x) - (y * y) - (z * z);
		if (w < 0)
			w = 0;
		else
			w = (float) -Math.sqrt(w);
	}

	public Quaternion(float x, float y, float z, float w) {
		super(x, y, z, w);
	}

	public Quaternion(Vector3 v) {
		super(v);
		w = 1 - (x * x) - (y * y) - (z * z);
		if (w < 0)
			w = 0;
		else
			w = (float) -Math.sqrt(w);
	}

	public Quaternion(Vector3 axis, float theta) {
		w = (float) Math.sin(theta * 0.5);
		x = axis.x * w;
		y = axis.y * w;
		z = axis.z * w;
		w = (float) Math.cos(theta * 0.5);
	}

	public Quaternion(Vector4 v) {
		super(v);
	}

	public Quaternion concat(Quaternion b, Quaternion dst) {
		float tw = w * b.w - x * b.x - y * b.y - z * b.z;
		float tx = w * b.x + x * b.w + y * b.z - z * b.y;
		float ty = w * b.y + y * b.w + z * b.x - x * b.z;
		float tz = w * b.z + z * b.w + x * b.y - y * b.x;
		dst.w = tw;
		dst.x = tx;
		dst.y = ty;
		dst.z = tz;
		return dst;
	}

	public Quaternion copy(Quaternion dst) {
		super.copy(dst);
		return dst;
	}

	public Quaternion invert(Quaternion dst) {
		super.invert(dst);
		return dst;
	}

	public Quaternion normalize(Quaternion dst) {
		super.normalize(dst);
		return dst;
	}

	public Quaternion scale(float s, Quaternion dst) {
		super.scale(s, dst);
		return dst;
	}

	public Vector3 rotate(Vector3 v, Vector3 dst) {
		if (dst == null)
			dst = new Vector3();
		float a = w * x;
		float b = w * y;
		float c = w * z;
		float d = -x * x;
		float e = x * y;
		float f = x * z;
		float g = -y * y;
		float h = y * z;
		float i = -z * z;
		float tx = v.x + 2 * ((g + i) * v.x + (e - c) * v.y + (b + f) * v.z);
		float ty = v.y + 2 * ((c + e) * v.x + (d + i) * v.y + (h - a) * v.z);
		float tz = v.z + 2 * ((f - b) * v.x + (a + h) * v.y + (d + g) * v.z);
		dst.x = tx;
		dst.y = ty;
		dst.z = tz;
		return dst;
	}

	public float[] toMatrix(float[] dst) {
		float xx = x * x;
		float xy = x * y;
		float xz = x * z;
		float xw = x * w;
		float yy = y * y;
		float yz = y * z;
		float yw = y * w;
		float zz = z * z;
		float zw = z * w;
		dst[0] = 1 - 2 * (yy + zz);
		dst[1] = 2 * (xy - zw);
		dst[2] = 2 * (xz + yw);
		dst[4] = 2 * (xy + zw);
		dst[5] = 1 - 2 * (xx + zz);
		dst[6] = 2 * (yz - xw);
		dst[8] = 2 * (xz - yw);
		dst[9] = 2 * (yz + xw);
		dst[10] = 1 - 2 * (xx + yy);
		dst[15] = 1;
		dst[3] = dst[7] = dst[11] = dst[12] = dst[13] = dst[14] = 0;
		return dst;
	}

	public static Quaternion interpolate(Quaternion a, Quaternion b, float t, Quaternion dst) {
		return nlerp(a, b, t, dst);
	}

	public static Quaternion nlerp(Quaternion a, Quaternion b, float t, Quaternion dst) {
		if (t <= 0)
			return a.copy(dst);
		if (t >= 1)
			return b.copy(dst);
		float c = a.dot(b);
		float s = 1 - t;
		if (c < 0) {
			dst.x = a.x * s - b.x * t;
			dst.y = a.y * s - b.y * t;
			dst.z = a.z * s - b.z * t;
			dst.w = a.w * s - b.w * t;
			return dst.normalize(dst);
		}
		dst.x = a.x * s + b.x * t;
		dst.y = a.y * s + b.y * t;
		dst.z = a.z * s + b.z * t;
		dst.w = a.w * s + b.w * t;
		return dst.normalize(dst);
	}

	public static Quaternion slerp(Quaternion q1, Quaternion q2, float t, Quaternion dst) {
		if (t <= 0)
			return q1.copy(dst);
		if (t >= 1)
			return q2.copy(dst);

		float c = q1.dot(q2);
		if (c < 0) {
			q2 = q2.invert(new Quaternion());
			c = -c;
		}

		if (c > 1 - EPSILON)
			return nlerp(q1, q2, t, dst);
		if (c < EPSILON) {
			/*
			 * The result is undefined because the angle is 180 degrees. Need to pick one of
			 * the infinite number of valid paths to use in a deterministic manner.
			 */
			return q1.copy(dst);
		}

		float d = (float) Math.sqrt(1.0 - c * c);
		c = (float) Math.acos(c);

		float s = (float) Math.sin((1 - t) * c) / d;
		t = (float) Math.sin(t * c) / d;
		dst.x = q1.x * s + q2.x * t;
		dst.y = q1.y * s + q2.y * t;
		dst.z = q1.z * s + q2.z * t;
		dst.w = q1.w * s + q2.w * t;
		return dst;
	}
}