/* 
 * AudioEngine.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.renderer.engine;

import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.Music;
import net.sf.mogbox.pol.ffxi.SoundEffect;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.ALC;

public class AudioEngine {
	private static final int SOURCE_COUNT = 16;

	private static Logger log = Logger.getLogger(AudioEngine.class.getName());

	private int count = 0;
	private IntBuffer sources = BufferUtils.createIntBuffer(SOURCE_COUNT);
	private Sound[] sounds = new Sound[SOURCE_COUNT];
	private long[] priorities = new long[SOURCE_COUNT];

	public AudioEngine() throws LWJGLException {
		ALC.create();
		AL10.alGenSources(sources);
		checkError();
	}

	public void destroy() {
		AL10.alDeleteSources(sources);
		ALC.destroy();
	}

	public void update() {
		for (int i = 0; i < SOURCE_COUNT; i++) {
			Sound s = sounds[i];
			try {
				if (s == null || s.update())
					continue;
			} catch (Throwable t) {
				log.log(Level.WARNING, null, t);
			}
			if (i == 0)
				sounds[0] = null;
			else
				pop(i);
		}
	}

	public Sound playMusic(int id) throws Exception {
		Sound sound = new Music(id).getSound();
		play(0, sound);
		return sound;
	}

	public Sound play(int priority, int id) throws Exception {
		Sound sound = new SoundEffect(id).getSound();
		play(priority, sound);
		return sound;
	}

	public void play(int priority, Sound sound) throws Exception {
		if (priority < 0)
			throw new IndexOutOfBoundsException("Priority cannot be negative.");
		if (priority == 0) {
			if (sounds[0] != null)
				sounds[0].stop();
			sound.play(sources.get(0));
			sounds[0] = sound;
		} else {
			if (priority > SOURCE_COUNT - 1)
				priority = SOURCE_COUNT - 1;
			priority = SOURCE_COUNT - priority;
			long p;
			if (sound.getLoopPoint() < 0) {
				p = System.currentTimeMillis() & 0x01FFFFFFFFFFFFFFL;
				p += (long) (sound.getDuration() * 0x000000FFFFFFFFFFL);
			} else {
				p = 0x03FFFFFFFFFFFFFFL;
			}
			p |= (long) priority << 58;
			if (count >= SOURCE_COUNT - 1) {
				if (p < priorities[1])
					return;
				pop(1).stop();
			}
			sound.play(push(p, sound));
		}
	}

	public final int checkError() {
		int error = AL10.alGetError();
		if (error != AL10.AL_NO_ERROR) {
			if (error == AL10.AL_OUT_OF_MEMORY)
				throw new OpenALException("Out of memory");
			else
				log.warning("OpenAL: " + translateALErrorString(error));
		}
		return error;
	}

	public static String translateALErrorString(int error) {
		switch (error) {
		case AL10.AL_NO_ERROR:
			return "No_error";
		case AL10.AL_INVALID_NAME:
			return "Invalid name";
		case AL10.AL_INVALID_ENUM:
			return "Invalid enum";
		case AL10.AL_INVALID_VALUE:
			return "Invalid value";
		case AL10.AL_INVALID_OPERATION:
			return "Invalid operation";
		case AL10.AL_OUT_OF_MEMORY:
			return "Out of memory";
		}
		return null;
	}

	private int push(long priority, Sound sound) {
		count++;
		int source = sources.get(count);

		int i = count;
		while (i > 1) {
			int parent = i / 2;
			if (priorities[parent] > priority) {
				sources.put(i, sources.get(parent));
				priorities[i] = priorities[parent];
				sounds[i] = sounds[parent];
			} else {
				break;
			}
			i = parent;
		}
		sources.put(i, source);
		priorities[i] = priority;
		sounds[i] = sound;

		return source;
	}

	private Sound pop(int head) {
		Sound result = sounds[head];

		int freeSource = sources.get(count);
		sources.put(head, sources.get(count));
		priorities[head] = priorities[count];
		sounds[head] = sounds[count];

		sources.put(count, freeSource);
		priorities[count] = 0;
		sounds[count] = null;

		count--;

		int source = sources.get(head);
		long priority = priorities[head];
		Sound sound = sounds[head];

		for (int child; head * 2 <= count; head = child) {
			child = head * 2;
			if (child != count && priorities[child + 1] < priorities[child])
				child++;
			if (priorities[child] < priority) {
				sources.put(head, sources.get(child));
				priorities[head] = priorities[child];
				sounds[head] = sounds[child];
			} else {
				break;
			}
		}
		sources.put(head, source);
		priorities[head] = priority;
		sounds[head] = sound;

		return result;
	}
}