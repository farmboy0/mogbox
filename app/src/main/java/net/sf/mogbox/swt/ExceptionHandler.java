package net.sf.mogbox.swt;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;

class ExceptionHandler implements UncaughtExceptionHandler {
	private Object uncaughtException;

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {
		// Unwrap any exceptions that SWT wrapped up
		while (throwable instanceof SWTException && ((SWTException) throwable).code == SWT.ERROR_FAILED_EXEC) {
			Throwable cause = throwable.getCause();
			if (cause == null)
				break;
			throwable = cause;
		}

		StackTraceElement[] trace = throwable.getStackTrace();
		Logger log = Logger.getLogger(trace.length > 0 ? trace[0].getClassName() : ExceptionHandler.class.getName());

		if (thread == null) {
			String msg = "An exception occurred on an unknown thread.";
			log.log(Level.SEVERE, msg, throwable);
		} else {
			String msg = String.format("An exception occurred on thread \"%s\".", thread.getName());
			log.log(Level.SEVERE, msg, throwable);
		}

		if (uncaughtException == null) {
			uncaughtException = throwable;
			Display.getCurrent().wake();
		}
	}
}