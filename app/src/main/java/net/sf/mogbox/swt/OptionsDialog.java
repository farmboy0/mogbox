/* 
 * OptionsDialog.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.mogbox.Strings;
import net.sf.mogbox.os.OS;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;

class OptionsDialog extends Dialog {
	private Shell dialog;

	private PanelSelector selector;

	public OptionsDialog(Shell parent) {
		this(parent, SWT.APPLICATION_MODAL);
	}

	public OptionsDialog(Shell parent, int style) {
		super(parent, style);

		dialog = new Shell(getParent(), SWT.DIALOG_TRIM | style);

		if (OS.isMacOSX())
			dialog.setText(Strings.getString("dialog.options.macosx"));
		else
			dialog.setText(Strings.getString("dialog.options"));

		dialog.setLayout(new FormLayout());

		selector = new PanelSelector(dialog, SWT.V_SCROLL);
	}

	public void open() {
		Rectangle screen = getParent().getMonitor().getClientArea();

		dialog.pack();
		Rectangle location = dialog.getClientArea();
		location.x = (screen.width - location.width) / 2 + screen.x;
		location.y = (screen.height - location.height) / 2 + screen.y;
		dialog.setLocation(location.x, location.y);

		dialog.open();

		// dialog.setDefaultButton(okButton);

		Display display = dialog.getDisplay();

		while (!dialog.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public void addSystemPanel(ImageData image) {
		selector.addSystemPanel(image);
	}

	public void addSystemPanel(InputStream image) {
		selector.addSystemPanel(image);
	}

	public void addSystemPanel(Image image) {
		selector.addSystemPanel(image);
	}

	public void addPanel(ImageData image) {
		selector.addPanel(image);
	}

	public void addPanel(InputStream image) {
		selector.addPanel(image);
	}

	public void addPanel(Image image) {
		selector.addPanel(image);
	}

	private class PanelSelector extends Canvas {
		private static final int ICON_SIZE = 80;
		private static final int BUTTON_SIZE = ICON_SIZE + 16;

		private Display display;

		private Color BACKGROUND;
		private Color BACKGROUND2;
		private Color BACKGROUND_SELECTION;

		private ScrollBar scrollBar;

		private boolean doubleBuffer = true;
		private Image buffer;

		private List<Image> systemPanels = new ArrayList<Image>();
		private List<Image> pluginPanels = new ArrayList<Image>();

		private int hover = -1;
		private int selection = -1;

		public PanelSelector(Composite parent, int style) {
			super(parent, style | SWT.NO_BACKGROUND);

			display = getDisplay();

			BACKGROUND = display.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
			float[] hsb = BACKGROUND.getRGB().getHSB();
			hsb[2] += hsb[2] >= 0.5 ? -0.05 : 0.05;
			BACKGROUND2 = new Color(display, new RGB(hsb[0], hsb[1], hsb[2]));
			BACKGROUND_SELECTION = display.getSystemColor(SWT.COLOR_LIST_SELECTION);

			Listener listener = new EventListener();
			addListener(SWT.MouseMove, listener);
			addListener(SWT.MouseExit, listener);
			addListener(SWT.MouseDown, listener);
			addListener(SWT.Paint, listener);
			addListener(SWT.Resize, listener);
			addListener(SWT.Dispose, listener);

			scrollBar = getVerticalBar();
			scrollBar.setIncrement(BUTTON_SIZE / 2);
			scrollBar.addListener(SWT.Selection, listener);
		}

		public void addSystemPanel(Image image) {
			addSystemPanel(image.getImageData());
		}

		public void addSystemPanel(InputStream in) {
			addSystemPanel(new ImageData(in));
		}

		public void addSystemPanel(ImageData data) {
			int max = Math.max(data.width, data.height);
			if (max != ICON_SIZE) {
				data = data.scaledTo(data.width * ICON_SIZE / max, data.height * ICON_SIZE / max);
			}
			Image img = new Image(display, data);
			systemPanels.add(img);
			scrollBar.setThumb(getSize().y - systemPanels.size() * BUTTON_SIZE);
		}

		public void addPanel(Image image) {
			addPanel(image.getImageData());
		}

		public void addPanel(InputStream in) {
			addPanel(new ImageData(in));
		}

		public void addPanel(ImageData data) {
			int max = Math.max(data.width, data.height);
			if (max != ICON_SIZE) {
				data = data.scaledTo(data.width * ICON_SIZE / max, data.height * ICON_SIZE / max);
			}
			Image img = new Image(display, data);
			pluginPanels.add(img);
			scrollBar.setMaximum(pluginPanels.size() * BUTTON_SIZE);
		}

		public int getSelection() {
			return selection;
		}

		public void setSelection(int selection) {
			if (this.selection == selection)
				return;
			this.selection = selection;
			int size = systemPanels.size();
			if (selection >= size) {
				int scroll = scrollBar.getSelection();
				selection = (selection - size) * BUTTON_SIZE;
				if (selection < scroll)
					scrollBar.setSelection(selection);
				else if (selection + BUTTON_SIZE > scroll + scrollBar.getThumb())
					scrollBar.setSelection(scroll - scroll - scrollBar.getThumb() + selection + BUTTON_SIZE);
			}
			redraw();
		}

		@Override
		public Point computeSize(int wHint, int hHint, boolean changed) {
			return new Point(BUTTON_SIZE + scrollBar.getSize().x, BUTTON_SIZE * 4);
		}

		private void paint(GC gc) {
			Rectangle size = getClientArea();
			int sys = systemPanels.size();
			int sysOffset = systemPanels.size() * BUTTON_SIZE;
			int offset = -scrollBar.getSelection();

			gc.setBackground(BACKGROUND);
			gc.fillRectangle(0, sysOffset, size.width, size.height - sysOffset);
			for (int i = 0; i < pluginPanels.size(); i++) {
				if (selection == i + sys) {
					gc.setBackground(BACKGROUND_SELECTION);
					gc.fillRectangle(0, offset + (i + sys) * BUTTON_SIZE, size.width, BUTTON_SIZE);
				} else if (hover == i + sys) {
					int old = gc.getAlpha();
					gc.setAlpha(old / 4);
					gc.setBackground(BACKGROUND_SELECTION);
					gc.fillRectangle(0, offset + (i + sys) * BUTTON_SIZE, size.width, BUTTON_SIZE);
					gc.setAlpha(old);
				}
				Image img = pluginPanels.get(i);
				int offsetX = (size.width - img.getImageData().width) / 2;
				int offsetY = (BUTTON_SIZE - img.getImageData().height) / 2;
				gc.drawImage(img, offsetX, offset + sysOffset + i * BUTTON_SIZE + offsetY);
			}

			gc.setBackground(BACKGROUND2);
			gc.fillRectangle(0, 0, size.width, sysOffset);
			for (int i = 0; i < systemPanels.size(); i++) {
				if (selection == i) {
					gc.setBackground(BACKGROUND_SELECTION);
					gc.fillRectangle(0, i * BUTTON_SIZE, size.width, BUTTON_SIZE);
				} else if (hover == i) {
					int old = gc.getAlpha();
					gc.setAlpha(old / 4);
					gc.setBackground(BACKGROUND_SELECTION);
					gc.fillRectangle(0, i * BUTTON_SIZE, size.width, BUTTON_SIZE);
					gc.setAlpha(old);
				}
				Image img = systemPanels.get(i);
				int offsetX = (size.width - img.getImageData().width) / 2;
				int offsetY = (BUTTON_SIZE - img.getImageData().height) / 2;
				gc.drawImage(img, offsetX, i * BUTTON_SIZE + offsetY);
			}
		}

		private int getItem(int x, int y) {
			int sys = systemPanels.size();
			if (y < sys * BUTTON_SIZE)
				return y / BUTTON_SIZE;
			return (y - sys * BUTTON_SIZE + scrollBar.getSelection()) / BUTTON_SIZE + sys;
		}

		private class EventListener implements Listener {
			@Override
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Paint:
					if (buffer == null) {
						paint(e.gc);
					} else {
						GC gc = new GC(buffer);
						paint(gc);
						gc.dispose();
						e.gc.drawImage(buffer, 0, 0);
					}
					break;
				case SWT.MouseMove:
					hover = getItem(e.x, e.y);
					redraw();
					break;
				case SWT.MouseExit:
					hover = -1;
					redraw();
					break;
				case SWT.MouseDown:
					setSelection(getItem(e.x, e.y));
					notifyListeners(SWT.Selection, new Event());
					break;
				case SWT.Resize:
					Point size = getSize();
					if (doubleBuffer) {
						if (buffer != null)
							buffer.dispose();
						buffer = new Image(display, size.x, size.y);
					}
					scrollBar.setThumb(size.y - systemPanels.size() * BUTTON_SIZE);
					break;
				case SWT.Selection:
					redraw();
					break;
				case SWT.Dispose:
					BACKGROUND2.dispose();
					if (buffer != null)
						buffer.dispose();
					for (Image img : systemPanels)
						img.dispose();
					for (Image img : pluginPanels)
						img.dispose();
					break;
				}
			}
		}
	}
}
