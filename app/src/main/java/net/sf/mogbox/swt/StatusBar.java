/* 
 * StatusBar.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

class StatusBar extends Composite {
	private Label separator;
	private Label label;
	private Composite componentPanel;
	private StackLayout componentStack;
	private ProgressBar progressBar;

	StatusBar(Composite parent) {
		super(parent, SWT.NONE);

		setLayout(new FormLayout());

		separator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);

		progressBar = new ProgressBar(this, SWT.NONE);
		progressBar.setVisible(false);

		componentPanel = new Composite(this, SWT.NONE);
		componentStack = new StackLayout();
		componentPanel.setLayout(componentStack);
		componentPanel.setBackground(getDisplay().getSystemColor(SWT.COLOR_BLUE));

		label = new Label(this, SWT.NONE);

		FormData data;

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		separator.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(separator, 1);
		data.right = new FormAttachment(1, 1, -1);
		data.bottom = new FormAttachment(1, 1, -1);
		progressBar.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(separator, 2);
		data.bottom = new FormAttachment(1, 1, -4);
		data.left = new FormAttachment(componentPanel, 4);
		data.right = new FormAttachment(progressBar, 4);
		label.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(separator, 2);
		data.bottom = new FormAttachment(1, 1, -4);
		data.left = new FormAttachment(0, 4);
		data.height = label.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
		data.width = 0;
		componentPanel.setLayoutData(data);
	}

}
