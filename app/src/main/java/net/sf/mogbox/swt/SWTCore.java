/* 
 * SWTCore.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import net.sf.mogbox.FullScreenProvider;
import net.sf.mogbox.renderer.engine.Core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.lwjgl.opengl.swt.GLCanvas;
import org.lwjgl.opengl.swt.GLData;

final class SWTCore extends Core {

	private GLCanvas canvas;

	private int maxSamples;

	public SWTCore(FullScreenProvider fullScreenProvider, Composite parent, boolean alpha, int samples) {
		super(fullScreenProvider);

		createCanvas(parent, alpha, samples);
		canvas.setCurrent();
		canvas.addListener(SWT.Resize, e -> {
			Point size = canvas.getSize();
			resize(size.x, size.y);
		});
	}

	private void createCanvas(Composite parent, boolean alpha, int samples) {
		GLData data = new GLData();
		data.majorVersion = 2;
		data.minorVersion = 0;
		data.debug = true;
		data.samples = samples;
		data.alphaSize = alpha ? 8 : 0;
		data.depthSize = 32;
		data.swapInterval = 1;

		do {
			try {
				canvas = new GLCanvas(parent, SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE, data);
				break;
			} catch (SWTException e) {
				if (data.alphaSize != 0) {
					data.alphaSize = 0;
				} else if (data.depthSize > 8) {
					data.depthSize -= 8;
					if (alpha)
						data.alphaSize = 8;
				} else if (data.samples != 0) {
					data.samples -= 1;
					data.depthSize = 32;
					if (alpha)
						data.alphaSize = 8;
				} else {
					throw e;
				}
			}
		} while (true);

		maxSamples = data.samples;
	}

	@Override
	public int getMaxSamples() {
		return maxSamples;
	}

	@Override
	public void focus() {
		canvas.setFocus();
	}

	@Override
	public boolean hasFocus() {
		return canvas.isFocusControl();
	}

	@Override
	public boolean isInBounds(int x, int y) {
		if (!canvas.isVisible())
			return false;
		Point location = canvas.toDisplay(canvas.getLocation());
		if (x < location.x || y < location.y)
			return false;
		return canvas.getBounds().contains(x - location.x, y - location.y);
	}

	@Override
	protected void makeCurrent() {
		canvas.setCurrent();
	}

	@Override
	protected void flipBuffers() {
		canvas.swapBuffers();
	}
}
