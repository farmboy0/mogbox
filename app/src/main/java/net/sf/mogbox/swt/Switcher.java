/* 
 * Switcher.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tracker;
import org.eclipse.swt.widgets.Widget;

class Switcher extends Composite {
	private ToolBar itemBar;
	private Listener switchListener;
	private Listener dragListener;

	private Map<String, ToolItem> items;
	private List<SelectionListener> selectionListeners;

	public Switcher(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout());

		itemBar = new ToolBar(this, SWT.FLAT);

		switchListener = new SwitchListener();

		dragListener = new DragListener();
		itemBar.addListener(SWT.MouseDown, dragListener);
		itemBar.addListener(SWT.MouseUp, dragListener);
		itemBar.addListener(SWT.MouseMove, dragListener);

		items = new LinkedHashMap<String, ToolItem>();
		selectionListeners = new LinkedList<SelectionListener>();

		addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				SelectionEvent event = new SelectionEvent(e);
				for (SelectionListener l : selectionListeners) {
					l.widgetSelected(event);
				}
			}
		});
	}

	public boolean add(String name, String displayName, Image icon) {
		if (name == null || items.containsKey(name))
			return false;
		ToolItem item = new ToolItem(itemBar, SWT.RADIO);
		if (icon != null)
			item.setImage(icon);
		else
			item.setText(displayName);
		item.setToolTipText(displayName);
		items.put(name, item);
		item.addListener(SWT.Selection, switchListener);
		if (itemBar.getItemCount() == 1) {
			item.setSelection(true);
		}
		resizeParent();
		return true;
	}

	public boolean remove(String name) {
		ToolItem item = items.remove(name);
		if (item == null)
			return false;
		boolean selected = item.getSelection();
		item.dispose();
		if (selected && itemBar.getItemCount() > 0) {
			item = itemBar.getItem(0);
			item.setSelection(true);
			item.notifyListeners(SWT.Selection, new Event());
		}
		resizeParent();
		return true;
	}

	public void setSelection(String name) {
		ToolItem item = items.get(name);
		if (item == null)
			return;
		for (ToolItem i : itemBar.getItems()) {
			i.setSelection(i == item);
		}
	}

	public String getSelection() {
		for (String name : items.keySet()) {
			ToolItem item = items.get(name);
			if (item != null && item.getSelection())
				return name;
		}
		return null;
	}

	public void addSelectionListener(SelectionListener listener) {
		selectionListeners.add(listener);
	}

	public void removeSelectionListener(SelectionListener listener) {
		selectionListeners.remove(listener);
	}

	public void setOrder(String[] names) {
		Map<String, ToolItem> map = new LinkedHashMap<String, ToolItem>(items);
		for (String n : names) {
			ToolItem item1 = map.remove(n);
			if (item1 == null)
				continue;
			ToolItem item2 = new ToolItem(itemBar, SWT.RADIO);
			item2.setToolTipText(item1.getToolTipText());
			item2.setImage(item1.getImage());
			item2.setText(item1.getText());
			item2.setSelection(item1.getSelection());
			item2.addListener(SWT.Selection, switchListener);
			item1.dispose();
			items.remove(n);
			items.put(n, item2);
		}
		for (String n : map.keySet()) {
			ToolItem item1 = items.remove(n);
			ToolItem item2 = new ToolItem(itemBar, SWT.RADIO);
			item2.setToolTipText(item1.getToolTipText());
			item2.setImage(item1.getImage());
			item2.setText(item1.getText());
			item2.setSelection(item1.getSelection());
			item2.addListener(SWT.Selection, switchListener);
			item1.dispose();
			items.put(n, item2);
		}
	}

	public String[] getOrder() {
		String[] names = new String[itemBar.getItemCount()];
		for (int i = 0; i < names.length; i++) {
			ToolItem item = itemBar.getItem(i);
			for (Map.Entry<String, ToolItem> e : items.entrySet()) {
				if (item == e.getValue()) {
					names[i] = e.getKey();
					break;
				}
			}
		}
		return names;
	}

	private void resizeParent() {
		Composite parent = getParent();
		if (parent instanceof CBanner) {
			CBanner banner = (CBanner) parent;
			if (banner.getRight() == this) {
				Point size = computeSize(SWT.DEFAULT, SWT.DEFAULT);
				size.x += 4;
				banner.setRightMinimumSize(size);
				banner.setRightWidth(size.x);
			}
		}
	}

	private void move(String name, int index) {
		ToolItem item = items.remove(name);
		if (item == null)
			return;
		String displayName1 = item.getToolTipText();
		String displayName2;
		Image icon1 = item.getImage();
		Image icon2;
		boolean selected1 = item.getSelection();
		boolean selected2;
		item.dispose();
		for (int i = index; i < itemBar.getItemCount(); i++) {
			item = itemBar.getItem(index);
			displayName2 = item.getToolTipText();
			icon2 = item.getImage();
			selected2 = item.getSelection();
			String name2 = null;
			for (Map.Entry<String, ToolItem> e : items.entrySet()) {
				if (item == e.getValue()) {
					name2 = e.getKey();
					break;
				}
			}
			items.remove(name2);
			item.dispose();
			item = new ToolItem(itemBar, SWT.RADIO);
			item.setToolTipText(displayName1);
			if (icon1 != null)
				item.setImage(icon1);
			else
				item.setText(displayName1);
			item.setSelection(selected1);
			item.addListener(SWT.Selection, switchListener);
			items.put(name, item);
			displayName1 = displayName2;
			icon1 = icon2;
			selected1 = selected2;
			name = name2;
		}
		item = new ToolItem(itemBar, SWT.RADIO);
		item.setToolTipText(displayName1);
		if (icon1 != null)
			item.setImage(icon1);
		else
			item.setText(displayName1);
		item.setSelection(selected1);
		item.addListener(SWT.Selection, switchListener);
		items.put(name, item);
	}

	private class SwitchListener implements Listener {
		@Override
		public void handleEvent(Event e) {
			Widget w = e.widget;
			if (w instanceof ToolItem && ((ToolItem) w).getSelection())
				notifyListeners(SWT.Selection, e);
		}
	}

	private class DragListener implements Listener {
		private ToolItem item;

		@Override
		public void handleEvent(Event event) {
			switch (event.type) {
			case SWT.MouseDown:
				item = itemBar.getItem(new Point(event.x, event.y));
				break;
			case SWT.MouseMove:
				if (item == null)
					return;

				Rectangle bounds = item.getBounds();
				if (bounds.contains(event.x, event.y))
					return;

				Tracker tracker = new Tracker(getDisplay(), SWT.NONE);
				tracker.addListener(SWT.Move, new TrackerListener(tracker));
				tracker.notifyListeners(SWT.Move, event);

				tracker.open();

				String name = null;
				for (String n : items.keySet()) {
					if (item == items.get(n)) {
						name = n;
						break;
					}
				}

				bounds = tracker.getRectangles()[0];
				Point point = itemBar.toControl(bounds.x, bounds.y);
				point.x += bounds.width / 2;
				point.y += bounds.height / 2;
				item = itemBar.getItem(point);

				int index = 0;
				for (int i = 0; i < itemBar.getItemCount(); i++) {
					if (item == itemBar.getItem(i)) {
						index = i;
						break;
					}
				}

				move(name, index);

				// FALL THROUGH
			case SWT.MouseUp:
				item = null;
				break;
			}
		}
	}

	private class TrackerListener implements Listener {
		private Tracker tracker;

		public TrackerListener(Tracker tracker) {
			this.tracker = tracker;
		}

		@Override
		public void handleEvent(Event event) {
			switch (event.type) {
			case SWT.Move:
				Point point = itemBar.toControl(getDisplay().getCursorLocation());
				ToolItem item = itemBar.getItem(0);
				for (ToolItem i : itemBar.getItems()) {
					Rectangle bounds = i.getBounds();
					if (point.x >= bounds.x) {
						if (bounds.x >= item.getBounds().x)
							item = i;
					}
				}
				Rectangle bounds = item.getBounds();
				Point location = itemBar.toDisplay(bounds.x, bounds.y);
				bounds.x = location.x;
				bounds.y = location.y;
				tracker.setRectangles(new Rectangle[] { bounds });
				break;
			}
		}
	}
}
