/* 
 * MogBox.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import static java.lang.Integer.MIN_VALUE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.FullScreenProvider;
import net.sf.mogbox.Strings;
import net.sf.mogbox.os.OS;
import net.sf.mogbox.plugin.Extension;
import net.sf.mogbox.plugin.ExtensionPoint;
import net.sf.mogbox.plugin.Plugin;
import net.sf.mogbox.preferences.Preferences;
import net.sf.mogbox.renderer.engine.Core;
import net.sf.mogbox.renderer.engine.RenderImageData;
import net.sf.mogbox.renderer.engine.scene.Node;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.ImageTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;

class MogBox implements FullScreenProvider {

	private static final String ENGINE_SAMPLES = "engine.samples";
	private static final String ENGINE_TRANSPARENCY = "engine.tranceparency";
	private static final String ENGINE_WIREFRAME = "engine.wireframe";

	private static final String PERSPECTIVES_CURRENT = "perspectives.current";
	private static final String PERSPECTIVES_ORDER = "perspectives.order";

	private static final String WINDOW_MAXIMIZED = "window.maximized";
	private static final String WINDOW_POSITION_X = "window.position.x";
	private static final String WINDOW_POSITION_Y = "window.position.y";
	private static final String WINDOW_SIZE_H = "window.size.h";
	private static final String WINDOW_SIZE_W = "window.size.w";

	private static final Logger log = Logger.getLogger(MogBox.class.getName());

	private static final Preferences PREFERENCES = Preferences.getRootInstance();

	private Map<String, Composite> perspectivePanels = new HashMap<>();
	private Map<String, ToolBar> perspectiveToolBars = new HashMap<>();
	private Map<String, Node> perspectiveNodes = new HashMap<>();

	private Plugin plugin;
	private Image[] icons;

	private Shell window;
	private Menu menuBar;
	private SWTCore canvas;

	public MogBox(Plugin plugin, Image... icons) {
		this.plugin = plugin;
		this.icons = icons;
		create();
	}

	public void show() {
		canvas.focus();
		window.open();
	}

	public boolean isDisposed() {
		return window.isDisposed();
	}

	private void dispose() {
		try {
			Preferences.save();
		} catch (IOException e) {
			log.log(Level.WARNING, "Error while saving preferences.", e);
		}
		window.dispose();
	}

	public Core getCore() {
		return canvas;
	}

	public boolean isFullScreen() {
		return window.getFullScreen();
	}

	public void setFullScreen(boolean fullScreen) {
		window.setMenuBar(fullScreen ? null : menuBar);
		window.setFullScreen(fullScreen);
	}

	private void create() {
		Display display = Display.getCurrent();

		window = new Shell(display);
		window.setText(Strings.getString("name"));
		window.setImages(icons);
		window.setLayout(new FormLayout());

		SashForm sash = new SashForm(window, SWT.NONE);
		sash.setSashWidth(3);

		canvas = new SWTCore(this, sash, true, PREFERENCES.getInt(ENGINE_SAMPLES, 16));
		canvas.setTransparencyeEnabled(PREFERENCES.getBoolean(ENGINE_TRANSPARENCY, false));
		canvas.setWireframeEnabled(PREFERENCES.getBoolean(ENGINE_WIREFRAME, false));
		PREFERENCES.setInt(ENGINE_SAMPLES, canvas.getMaxSamples());

		StackLayout perspectivePanelStack = new StackLayout();
		Composite perspectivePanelParent = new Composite(sash, SWT.NONE);
		perspectivePanelParent.setLayout(perspectivePanelStack);

		sash.setWeights(new int[] { 80, 20 });

		CBanner toolBanner = new CBanner(window, SWT.NONE);
		toolBanner.setSimple(false);

		Composite toolBarPanel = new Composite(toolBanner, SWT.NONE);
		toolBarPanel.setLayout(new FormLayout());

		ToolBar applicationToolBar = new ToolBar(toolBarPanel, SWT.NONE);

		toolBanner.setLeft(toolBarPanel);

		StackLayout perspectiveToolBarStack = new StackLayout();
		Composite perspectiveToolBarParent = new Composite(toolBarPanel, SWT.NONE);
		perspectiveToolBarParent.setLayout(perspectiveToolBarStack);

		Switcher perspectiveSwitcher = new Switcher(toolBanner, SWT.NONE);
		perspectiveSwitcher.addListener(SWT.Selection, e -> {
			String perspective = perspectiveSwitcher.getSelection();
			canvas.changeNode(perspectiveNodes.get(perspective));
			perspectivePanelStack.topControl = perspectivePanels.get(perspective);
			perspectiveToolBarStack.topControl = perspectiveToolBars.get(perspective);
			perspectivePanelParent.layout();
			perspectiveToolBarParent.layout();

			String[] values = perspectiveSwitcher.getOrder();
			StringBuilder buffer = new StringBuilder();
			for (String v : values) {
				if (buffer.length() != 0)
					buffer.append(',');
				buffer.append(v.replaceAll("([\\\\,])", "\\$1"));
			}
			PREFERENCES.setString(PERSPECTIVES_ORDER, buffer.toString());
			if (perspective == null)
				PREFERENCES.remove(PERSPECTIVES_CURRENT);
			else
				PREFERENCES.setString(PERSPECTIVES_CURRENT, perspective);
		});
		toolBanner.setRight(perspectiveSwitcher);

		StatusBar statusBar = new StatusBar(window);

		ExtensionPoint perspectivePoint = plugin.getExtensionPoint("perspective");
		for (Extension ext : perspectivePoint.getExtensions()) {
			Perspective perspective = ext.newInstance(Perspective.class);
			if (perspective == null)
				continue;

			String name = perspective.getName();
			Image icon = null;
			if (perspective.getIconLocation() != null) {
				try {
					icon = new Image(display, from(perspective.getIconLocation()));
				} catch (IOException e) {
					log.log(Level.WARNING, "Error loading perspective icon.", e);
				}
			}

			Composite perspectivePanel = new Composite(perspectivePanelParent, SWT.NONE);
			perspectivePanel.setLayout(new FillLayout());
			perspective.createPanel(perspectivePanel);
			perspectivePanels.put(name, perspectivePanel);

			perspectiveNodes.put(name, perspective.getNode());

			ToolBar perspectiveToolBar = new ToolBar(perspectiveToolBarParent, SWT.FLAT);
			perspectiveToolBars.put(name, perspectiveToolBar);

			perspectiveSwitcher.add(name, perspective.getDisplayName(), icon);
		}
		String value = PREFERENCES.getString(PERSPECTIVES_ORDER,
				"mogbox.pc,mogbox.npc,mogbox.chocobo,mogbox.furniture,mogbox.area");
		String[] values = value.split("(?<!\\\\),");
		for (int i = 0; i < values.length; i++)
			values[i] = values[i].replaceAll("\\\\(.)", "$1");
		perspectiveSwitcher.setOrder(values);
		perspectiveSwitcher.setSelection(PREFERENCES.getString(PERSPECTIVES_CURRENT, "mogbox.pc"));
		perspectiveSwitcher.notifyListeners(SWT.Selection, new Event());

		initializeMenu();

		FormData data = new FormData();
		data.top = new FormAttachment(0, 1);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		toolBanner.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(toolBanner, 1);
		data.bottom = new FormAttachment(statusBar, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		sash.setLayoutData(data);

		data = new FormData();
		data.bottom = new FormAttachment(1, 1, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		statusBar.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 4);
		applicationToolBar.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(applicationToolBar, 0, SWT.BOTTOM);
		data.left = new FormAttachment(applicationToolBar, 0);
		data.right = new FormAttachment(1, 1, 0);
		perspectiveToolBarParent.setLayoutData(data);

		Point p = window.getSize();
		p.x = PREFERENCES.getInt(WINDOW_SIZE_W, p.x);
		p.y = PREFERENCES.getInt(WINDOW_SIZE_H, p.y);
		window.setSize(p);

		p = window.getLocation();
		p.x = PREFERENCES.getInt(WINDOW_POSITION_X, p.x);
		p.y = PREFERENCES.getInt(WINDOW_POSITION_Y, p.y);
		window.setLocation(p);

		window.setMaximized(PREFERENCES.getBoolean(WINDOW_MAXIMIZED, false));

		window.addListener(SWT.Move, e -> {
			if (!window.getMaximized()) {
				Point location = window.getLocation();
				if (location.x != MIN_VALUE)
					PREFERENCES.setInt(WINDOW_POSITION_X, location.x);
				if (location.y != MIN_VALUE)
					PREFERENCES.setInt(WINDOW_POSITION_Y, location.y);
			}
			PREFERENCES.setBoolean(WINDOW_MAXIMIZED, window.getMaximized());
		});
		window.addListener(SWT.Resize, e -> {
			if (!window.getMaximized()) {
				Point size = window.getSize();
				if (size.x != MIN_VALUE)
					PREFERENCES.setInt(WINDOW_SIZE_W, size.x);
				if (size.y != MIN_VALUE)
					PREFERENCES.setInt(WINDOW_SIZE_H, size.y);
			}
			PREFERENCES.setBoolean(WINDOW_MAXIMIZED, window.getMaximized());
		});
		window.addListener(SWT.Close, e -> dispose());
	}

	private void initializeMenu() {
		Menu fileMenu = new Menu(window, SWT.DROP_DOWN);

		MenuItem saveScreenshot = new MenuItem(fileMenu, SWT.PUSH);
		saveScreenshot.setText(Strings.getString("menu.file.screen.save") + "\tF11");
		saveScreenshot.setAccelerator(SWT.F11);
		saveScreenshot.addListener(SWT.Selection, e -> {
			FileDialog save = createSaveDialog();
			final String filename = save.open();
			if (filename != null) {
				window.setCursor(window.getDisplay().getSystemCursor(SWT.CURSOR_WAIT));
				saveImage(filename);
				window.setCursor(null);
			}
		});

		MenuItem copyScreenshot = new MenuItem(fileMenu, SWT.PUSH);
		copyScreenshot.setText(Strings.getString("menu.file.screen.copy") + "\tCtrl+F11");
		copyScreenshot.setAccelerator(SWT.MOD1 | SWT.F11);
		copyScreenshot.addListener(SWT.Selection, e -> copyImage());

		new MenuItem(fileMenu, SWT.SEPARATOR);

		MenuItem exit = new MenuItem(fileMenu, SWT.PUSH);
		exit.setText(Strings.getString("menu.file.exit"));
		exit.addListener(SWT.Selection, e -> dispose());

		Menu viewMenu = new Menu(window, SWT.DROP_DOWN);

		MenuItem alpha = new MenuItem(viewMenu, SWT.CHECK);
		alpha.setText(Strings.getString("menu.view.transparency"));
		alpha.setSelection(PREFERENCES.getBoolean(ENGINE_TRANSPARENCY, true));
		alpha.addListener(SWT.Selection, e -> {
			canvas.setTransparencyeEnabled(alpha.getSelection());
			PREFERENCES.setBoolean(ENGINE_TRANSPARENCY, alpha.getSelection());
		});

		MenuItem wireframe = new MenuItem(viewMenu, SWT.CHECK);
		wireframe.setText(Strings.getString("menu.view.wireframe"));
		wireframe.setSelection(PREFERENCES.getBoolean(ENGINE_WIREFRAME, false));
		wireframe.addListener(SWT.Selection, e -> {
			canvas.setWireframeEnabled(wireframe.getSelection());
			PREFERENCES.setBoolean(ENGINE_WIREFRAME, wireframe.getSelection());
		});

		new MenuItem(viewMenu, SWT.SEPARATOR);

		MenuItem fullscreen = new MenuItem(viewMenu, SWT.PUSH);
		fullscreen.setText(Strings.getString("menu.view.fullscreen"));
		fullscreen.addListener(SWT.Selection, e -> setFullScreen(true));

		new MenuItem(viewMenu, SWT.SEPARATOR);

		MenuItem resetCamera = new MenuItem(viewMenu, SWT.PUSH);
		resetCamera.setText(Strings.getString("menu.view.reset-camera"));
		resetCamera.addListener(SWT.Selection, e -> canvas.reset());

		Menu toolsMenu = new Menu(window, SWT.DROP_DOWN);

		MenuItem options = new MenuItem(toolsMenu, SWT.PUSH);
		options.setText(Strings.getString("menu.tools.options"));
		options.addListener(SWT.Selection, e -> {
			OptionsDialog optionsDialog = new OptionsDialog(window, SWT.APPLICATION_MODAL);
			optionsDialog.open();
		});

		Menu helpMenu = new Menu(window, SWT.DROP_DOWN);

		MenuItem about = new MenuItem(helpMenu, SWT.PUSH);
		about.setText(Strings.getString("menu.help.about"));
		about.addListener(SWT.Selection, e -> {
			AboutDialog aboutDialog = new AboutDialog(window, SWT.NONE);
			aboutDialog.open();
		});

		menuBar = new Menu(window, SWT.BAR);

		MenuItem file = new MenuItem(menuBar, SWT.CASCADE);
		file.setText(Strings.getString("menu.file"));
		file.setMenu(fileMenu);

		MenuItem view = new MenuItem(menuBar, SWT.CASCADE);
		view.setText(Strings.getString("menu.view"));
		view.setMenu(viewMenu);

		MenuItem tools = new MenuItem(menuBar, SWT.CASCADE);
		tools.setText(Strings.getString("menu.tools"));
		tools.setMenu(toolsMenu);

		MenuItem help = new MenuItem(menuBar, SWT.CASCADE);
		help.setText(Strings.getString("menu.help"));
		help.setMenu(helpMenu);

		window.setMenuBar(menuBar);
	}

	private void copyImage() {
		SWTGui.postRenderAsyncExec(() -> {
			try {
				ImageData image = getImage();

				Clipboard clipboard = new Clipboard(Display.getCurrent());
				try {
					clipboard.setContents(new Object[] { image }, new Transfer[] { ImageTransfer.getInstance() });
				} finally {
					clipboard.dispose();
				}
			} catch (Exception e) {
				log.log(Level.WARNING, "Error copying image to clipboard", e);
			}
		}, null);
	}

	private void saveImage(String filename) {
		SWTGui.postRenderAsyncExec(() -> {
			ImageLoader loader = new ImageLoader();
			try {
				loader.data = new ImageData[] { getImage() };
				try (OutputStream out = new FileOutputStream(filename)) {
					loader.save(out, SWT.IMAGE_PNG);
				}
			} catch (Exception e) {
				log.log(Level.WARNING, "Error during image save", e);
			}
		}, null);
	}

	private FileDialog createSaveDialog() {
		FileDialog save = new FileDialog(window, SWT.SAVE);
		File start = OS.getPicturesLocation();
		save.setFilterPath(start.getAbsolutePath());
		save.setFilterNames(new String[] { Strings.getString("dialog.save.image.png"),
				Strings.getString("dialog.save.image.all") });
		save.setFilterExtensions(new String[] { "*.png", "*.*" });
		save.setOverwrite(true);

		return save;
	}

	private ImageData getImage() {
		RenderImageData imageData = canvas.readImageData();

		PaletteData palette = new PaletteData(0xFF, 0xFF00, 0xFF0000);
		ImageData image = new ImageData(imageData.width, imageData.height, 24, palette, 4, imageData.color);
		image.alphaData = imageData.alpha;
		return image.scaledTo(imageData.width, -imageData.height);
	}

	private ImageData from(URL imageURL) throws IOException {
		final InputStream imageStream = imageURL.openStream();
		try {
			return new ImageData(imageStream);
		} finally {
			imageStream.close();
		}
	}
}
