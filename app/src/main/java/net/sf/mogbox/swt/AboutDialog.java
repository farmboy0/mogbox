/* 
 * AboutDialog.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import net.sf.mogbox.Strings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

class AboutDialog extends Dialog {
	private static final String WEBSITE_URL = "http://mogbox.sourceforge.net/";

	private Shell dialog;

	private Image image;

	private Button okButton;

	public AboutDialog(Shell parent) {
		this(parent, SWT.APPLICATION_MODAL);
	}

	public AboutDialog(Shell parent, int style) {
		super(parent, style);

		Display display = parent.getDisplay();

		dialog = new Shell(getParent(), SWT.DIALOG_TRIM | style);

		dialog.setText(Strings.getString("dialog.about"));

		image = new Image(parent.getDisplay(), getClass().getResourceAsStream("/about.png"));

		Label imageLabel = new Label(dialog, SWT.CENTER);
		imageLabel.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		imageLabel.setImage(image);

		Label separator = new Label(dialog, SWT.SEPARATOR | SWT.HORIZONTAL);

		String versionString = Strings.getString("version.major") + '.' + Strings.getString("version.minor") + '.'
				+ Strings.getString("version.maintenance") + ' ' + Strings.getString("version.build");
		versionString = String.format("%s v%s", Strings.getString("name"), versionString);

		Label version = new Label(dialog, SWT.NONE);
		version.setText(versionString);

		okButton = new Button(dialog, SWT.PUSH);
		okButton.setText(Strings.getString("dialog.ok"));

		String websiteText = String.format("<a href=\"#website\">%s</a>", WEBSITE_URL);

		Link website = new Link(dialog, SWT.NONE);
		website.setText(websiteText);

		String copyrightText = Strings.getString("copyright") + "\n\n"
				+ "This program comes with ABSOLUTELY NO WARRANTY.\n"
				+ "This is free software, and you are welcome to redistribute it\n" + "under certain conditions.";

		Label copyright = new Label(dialog, SWT.NONE);
		copyright.setText(copyrightText);

		Listener listener = new EventListener();

		okButton.addListener(SWT.Selection, listener);
		website.addListener(SWT.Selection, listener);

		FormData data;

		dialog.setLayout(new FormLayout());

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		imageLabel.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(imageLabel);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(1, 1, 0);
		separator.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(separator, 11);
		data.left = new FormAttachment(0, 11);
		version.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(version, 0);
		data.left = new FormAttachment(0, 11);
		data.right = new FormAttachment(1, 1, -11);
		website.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(website, 11);
		data.left = new FormAttachment(0, 11);
		data.right = new FormAttachment(1, 1, -11);
		copyright.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(copyright, 11);
		data.bottom = new FormAttachment(1, 1, -11);
		data.right = new FormAttachment(1, 1, -11);
		Point size = okButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (size.x > 75)
			data.width = size.x;
		else
			data.width = 75;
		okButton.setLayoutData(data);
	}

	public void open() {
		Rectangle screen = getParent().getMonitor().getClientArea();

		dialog.pack();
		Rectangle location = dialog.getClientArea();
		location.x = (screen.width - location.width) / 2 + screen.x;
		location.y = (screen.height - location.height) / 2 + screen.y;
		dialog.setLocation(location.x, location.y);

		dialog.open();

		dialog.setDefaultButton(okButton);

		Display display = dialog.getDisplay();

		while (!dialog.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		image.dispose();
	}

	private class EventListener implements Listener {
		@Override
		public void handleEvent(Event e) {
			if (e.widget == okButton) {
				dialog.close();
			} else {
				if ("#website".equals(e.text)) {
					dialog.setCursor(dialog.getDisplay().getSystemCursor(SWT.CURSOR_WAIT));
					Program.launch(WEBSITE_URL);
					dialog.setCursor(null);
				}
			}
		}
	}
}
