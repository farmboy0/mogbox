package net.sf.mogbox.swt;

import net.sf.mogbox.Strings;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWTErrorHandler {

	private SWTErrorHandler() {
	}

	public static void handle(Exception e) {
		Display.getCurrent().asyncExec(() -> {
			Shell shell = new Shell(Display.getCurrent());
			ErrorDialog error = new ErrorDialog(shell);
			error.setText(Strings.getString("dialog.error.fatal"));
			error.setMessage(Strings.getString("dialog.error.fatal.message"));
			error.setThrowable(e);
			error.open();
		});
	}
}
