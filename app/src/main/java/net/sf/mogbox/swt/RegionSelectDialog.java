/* 
 * RegionSelectDialog.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import java.util.List;

import net.sf.mogbox.Strings;
import net.sf.mogbox.pol.Region;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

class RegionSelectDialog extends Dialog {
	private Shell dialog;

	private Label message;
	private Button okButton;
	private Combo list;

	private List<Region> regions;
	private Region selection;

	public RegionSelectDialog(Shell parent) {
		this(parent, SWT.APPLICATION_MODAL);
	}

	public RegionSelectDialog(Shell parent, int style) {
		super(parent, style);

		dialog = new Shell(getParent(), SWT.DIALOG_TRIM | style);

		Composite composite = new Composite(dialog, SWT.NONE);

		Label iconLabel = new Label(composite, SWT.NONE);
		iconLabel.setImage(dialog.getDisplay().getSystemImage(SWT.ICON_QUESTION));

		message = new Label(composite, SWT.NONE);

		Listener listener = new EventListener();

		list = new Combo(dialog, SWT.READ_ONLY);
		list.addListener(SWT.Selection, listener);

		okButton = new Button(dialog, SWT.PUSH);
		okButton.setText(Strings.getString("dialog.ok"));
		okButton.addListener(SWT.Selection, listener);

		FormData data;

		composite.setLayout(new FormLayout());

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		iconLabel.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(iconLabel, 1, SWT.TOP);
		data.bottom = new FormAttachment(1, 1, 0);
		data.left = new FormAttachment(iconLabel, 11);
		data.right = new FormAttachment(1, 1, 0);
		message.setLayoutData(data);

		dialog.setLayout(new FormLayout());

		data = new FormData();
		data.top = new FormAttachment(0, 11);
		data.left = new FormAttachment(0, 11);
		data.right = new FormAttachment(1, 1, -11);
		composite.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(composite, 10);
		data.left = new FormAttachment(0, 11);
		data.right = new FormAttachment(1, 1, -11);
		list.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(list, 10);
		data.bottom = new FormAttachment(1, 1, -11);
		data.right = new FormAttachment(1, 1, -11);
		Point size = okButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (size.x > 75)
			data.width = size.x;
		else
			data.width = 75;
		okButton.setLayoutData(data);
	}

	public Region open() {
		if (regions == null)
			return null;

		Rectangle screen = getParent().getMonitor().getClientArea();

		dialog.pack();
		Rectangle location = dialog.getClientArea();
		location.x = (screen.width - location.width) / 2 + screen.x;
		location.y = (screen.height - location.height) / 2 + screen.y;
		dialog.setLocation(location.x, location.y);

		dialog.open();

		dialog.setDefaultButton(okButton);

		Display display = dialog.getDisplay();

		while (!dialog.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		return selection;
	}

	@Override
	public void setText(String string) {
		super.setText(string);
		dialog.setText(string);
	}

	public void setMessage(String string) {
		message.setText(string);
		dialog.pack();
	}

	public String getMessage() {
		return message.getText();
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
		String[] items = new String[regions.size()];
		for (int i = 0; i < items.length; i++) {
			Region r = regions.get(i);
			String k = String.format("dialog.region.%s", r.toString().toLowerCase());
			items[i] = Strings.getString(k);
		}
		list.setItems(items);
		int index = regions.indexOf(Region.NA);
		if (index < 0)
			index = list.getItemCount() - 1;
		list.select(index);
		selection = regions.get(index);
	}

	public void setSelection(Region region) {
		int index = regions.indexOf(region);
		if (index >= 0) {
			list.select(index);
			selection = region;
		}
	}

	private class EventListener implements Listener {
		@Override
		public void handleEvent(Event e) {
			if (e.widget == okButton) {
				dialog.close();
			} else if (e.widget == list) {
				selection = regions.get(list.getSelectionIndex());
			}
		}
	}
}
