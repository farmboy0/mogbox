package net.sf.mogbox.swt;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Logger;

import net.sf.mogbox.Strings;
import net.sf.mogbox.os.OS;
import net.sf.mogbox.plugin.Plugin;
import net.sf.mogbox.pol.Region;
import net.sf.mogbox.renderer.engine.Core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWTGui {
	private static final Logger log = Logger.getLogger(SWTGui.class.getName());

	private static Queue<Runnable> preRenderQueue = new ConcurrentLinkedQueue<>();
	private static Queue<Runnable> postRenderQueue = new ConcurrentLinkedQueue<>();

	private Thread guiThread;
	private ImageData splash;
	private ImageData[] icons;
	private boolean dispose;
	private boolean withSplash;
	private Plugin plugin;

	private Core core;

	private long lastTimestamp;

	public SWTGui(URL imageURL, URL[] iconURLs) throws IOException {
		this.splash = from(imageURL);
		if (iconURLs != null) {
			icons = new ImageData[iconURLs.length];
			for (int i = 0; i < iconURLs.length; i++) {
				icons[i] = from(iconURLs[i]);
			}
		}
		this.dispose = false;
		this.withSplash = false;
		this.guiThread = new Thread(this::render, "UI");
	}

	public void start(boolean withSplash) {
		this.withSplash = withSplash;
		this.guiThread.start();
	}

	public void showMogbox(Plugin plugin) {
		this.plugin = plugin;
		if (this.guiThread.isAlive()) {
			this.dispose = true;
		} else {
			start(false);
		}
	}

	private void render() {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());

		Display display = new Display();

		Image[] iconImages = null;
		if (icons != null) {
			iconImages = new Image[icons.length];
			for (int i = 0; i < icons.length; i++) {
				iconImages[i] = new Image(display, icons[i]);
			}
		}

		if (withSplash && splash != null) {
			SplashScreen splashScreen = new SplashScreen(new Image(display, this.splash), iconImages);
			splashScreen.show();

			while (!dispose) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			splashScreen.dispose();
		}

		MogBox mogBox = new MogBox(this.plugin, iconImages);
		mogBox.show();

		core = mogBox.getCore();
		core.initialize();

		this.dispose = false;
		this.lastTimestamp = System.currentTimeMillis();
		renderLoop();

		while (!mogBox.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		this.dispose = true;

		display.dispose();
	}

	private void renderLoop() {
		if (!dispose) {
			long start = System.currentTimeMillis();
			float delta = (start - lastTimestamp) / 1000f;
			for (Runnable r : preRenderQueue)
				r.run();
			core.update(delta);
			core.render(0.0f);
			for (Runnable r : postRenderQueue)
				r.run();
			long end = System.currentTimeMillis();
			int next = 16 - (int) (end - start);
			while (next < 0)
				next += 16;
			lastTimestamp = start;
			Display.getCurrent().timerExec(next, this::renderLoop);
		}
	}

	private ImageData from(URL imageURL) throws IOException {
		if (imageURL == null) {
			return null;
		}
		final InputStream imageStream = imageURL.openStream();
		try {
			return new ImageData(imageStream);
		} finally {
			imageStream.close();
		}
	}

	public Future<Region> chooseRegion(List<Region> installed) {
		FutureTask<Region> task = new FutureTask<>(() -> {
			Shell shell = new Shell();
			try {
				shell.setText(Strings.getString("name"));
				if (icons != null) {
					Image[] iconImages = new Image[icons.length];
					for (int i = 0; i < icons.length; i++) {
						iconImages[i] = new Image(Display.getCurrent(), icons[i]);
					}
					shell.setImages(iconImages);
				}
				RegionSelectDialog dialog = new RegionSelectDialog(shell, SWT.NONE);
				dialog.setText(Strings.getString("dialog.region"));
				dialog.setMessage(Strings.getString("dialog.region.message"));
				dialog.setRegions(installed);

				return dialog.open();
			} finally {
				shell.dispose();
			}
		});
		Display.getDefault().asyncExec(task);
		return task;
	}

	public Future<String> chooseFFXIPath() {
		FutureTask<String> task = new FutureTask<>(() -> {
			Shell shell = new Shell();
			try {
				shell.setText(Strings.getString("name"));
				if (icons != null) {
					Image[] iconImages = new Image[icons.length];
					for (int i = 0; i < icons.length; i++) {
						iconImages[i] = new Image(Display.getCurrent(), icons[i]);
					}
					shell.setImages(iconImages);
				}

				DirectoryDialog dialog = new DirectoryDialog(shell);
				File location = OS.getWindowsApplicationLocation();
				if (location != null)
					dialog.setFilterPath(location.getAbsolutePath());
				dialog.setText(Strings.getString("dialog.location.ffxi"));
				dialog.setMessage(Strings.getString("dialog.location.ffxi.message"));

				return dialog.open();
			} finally {
				shell.dispose();
			}
		});
		Display.getDefault().asyncExec(task);
		return task;
	}

	public static <V> Future<V> preRenderAsyncExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<>(callable);
		preRenderQueue.add(task);
		return task;
	}

	public static <V> Future<V> preRenderAsyncExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<>(runnable, result);
		preRenderQueue.add(task);
		return task;
	}

	public static <V> Future<V> postRenderAsyncExec(Callable<V> callable) {
		FutureTask<V> task = new FutureTask<>(callable);
		postRenderQueue.add(task);
		return task;
	}

	public static <V> Future<V> postRenderAsyncExec(Runnable runnable, V result) {
		FutureTask<V> task = new FutureTask<>(runnable, result);
		postRenderQueue.add(task);
		return task;
	}
}
