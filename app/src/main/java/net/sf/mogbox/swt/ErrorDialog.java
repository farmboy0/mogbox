/* 
 * ErrorDialog.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.swt;

import java.io.PrintWriter;
import java.io.StringWriter;

import net.sf.mogbox.Strings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

class ErrorDialog extends Dialog {
	private Shell dialog;

	private Label message;
	private Text errorText;
	private Button detailsButton;
	private Button copyButton;
	private Button okButton;

	private Throwable throwable;

	public ErrorDialog(Shell parent) {
		this(parent, SWT.APPLICATION_MODAL);
	}

	public ErrorDialog(Shell parent, int style) {
		this(parent, style, SWT.ICON_ERROR);
	}

	public ErrorDialog(Shell parent, int style, int icon) {
		super(parent, style);

		dialog = new Shell(parent, SWT.DIALOG_TRIM | style);

		Composite composite = new Composite(dialog, SWT.NONE);

		Label iconLabel = new Label(composite, SWT.NONE);
		iconLabel.setImage(dialog.getDisplay().getSystemImage(icon));

		message = new Label(composite, SWT.NONE);

		errorText = new Text(dialog, SWT.BORDER | SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL);
		errorText.setVisible(false);

		okButton = new Button(dialog, SWT.PUSH);
		okButton.setText(Strings.getString("dialog.ok"));

		detailsButton = new Button(dialog, SWT.PUSH);
		detailsButton.setText(Strings.getString("dialog.error.details.show"));
		detailsButton.setVisible(false);

		copyButton = new Button(dialog, SWT.PUSH);
		copyButton.setText(Strings.getString("dialog.error.copy"));
		copyButton.setVisible(false);

		Listener listener = new EventListener();

		okButton.addListener(SWT.Selection, listener);
		detailsButton.addListener(SWT.Selection, listener);
		copyButton.addListener(SWT.Selection, listener);

		Point size;

		FormData data;

		composite.setLayout(new FormLayout());

		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		iconLabel.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(iconLabel, 9, SWT.TOP);
		data.bottom = new FormAttachment(1, 1, 0);
		data.left = new FormAttachment(iconLabel, 11);
		data.right = new FormAttachment(1, 1, 0);
		message.setLayoutData(data);

		dialog.setLayout(new FormLayout());

		data = new FormData();
		data.top = new FormAttachment(0, 11);
		data.left = new FormAttachment(0, 11);
		data.right = new FormAttachment(1, 1, -11);
		composite.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(composite, 11);
		data.left = new FormAttachment(0, 10);
		data.right = new FormAttachment(1, 1, -11);
		data.height = 0;
		data.width = 400;
		errorText.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(errorText, 0, SWT.TOP);
		data.bottom = new FormAttachment(1, 1, -11);
		data.right = new FormAttachment(okButton, -11);
		data.width = 75;
		size = detailsButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (size.x > 75)
			data.width = size.x;
		else
			data.width = 75;
		detailsButton.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(detailsButton, 0, SWT.TOP);
		data.bottom = new FormAttachment(1, 1, -11);
		data.right = new FormAttachment(detailsButton, -7);
		copyButton.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(detailsButton, 0, SWT.TOP);
		data.bottom = new FormAttachment(1, 1, -11);
		data.right = new FormAttachment(1, 1, -11);
		size = okButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (size.x > 75)
			data.width = size.x;
		else
			data.width = 75;
		okButton.setLayoutData(data);

		dialog.setDefaultButton(okButton);
	}

	public void open() {
		Rectangle screen = getParent().getMonitor().getClientArea();

		dialog.pack();
		Rectangle location = dialog.getClientArea();
		location.x = (screen.width - location.width) / 2 + screen.x;
		location.y = (screen.height - location.height) / 2 + screen.y;
		dialog.setLocation(location.x, location.y);

		dialog.open();

		dialog.setDefaultButton(okButton);

		Display display = dialog.getDisplay();

		while (!dialog.isDisposed()) {
			try {
				if (!display.readAndDispatch())
					display.sleep();
			} catch (Throwable e) {
				Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
			}
		}
	}

	@Override
	public void setText(String string) {
		super.setText(string);
		dialog.setText(string);
	}

	public void setMessage(String string) {
		message.setText(string);
		dialog.pack();
	}

	public String getMessage() {
		return message.getText();
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
		if (throwable == null) {
			setDetailsVisible(false);
			detailsButton.setVisible(false);
			copyButton.setVisible(false);
			errorText.setText("");
		} else {
			detailsButton.setVisible(true);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			throwable.printStackTrace(pw);
			pw.flush();
			sw.flush();
			errorText.setText(sw.toString());
		}
	}

	public Throwable getThrowable() {
		return throwable;
	}

	private void setDetailsVisible(boolean visible) {
		if (visible) {
			FormData data;
			data = (FormData) errorText.getLayoutData();
			data.height = errorText.getLineHeight() * 10;

			data = (FormData) detailsButton.getLayoutData();
			data.top.alignment = SWT.BOTTOM;
			data.top.offset = 10;

			errorText.setVisible(true);
			detailsButton.setText(Strings.getString("dialog.error.details.hide"));
			Point size = detailsButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			FormData d = (FormData) detailsButton.getLayoutData();
			if (size.x > 75)
				d.width = size.x;
			else
				d.width = 75;
			copyButton.setVisible(true);
			size = dialog.computeSize(dialog.getClientArea().width, SWT.DEFAULT);
			dialog.setSize(size);
		} else {
			FormData data;
			data = (FormData) errorText.getLayoutData();
			data.height = 0;

			data = (FormData) detailsButton.getLayoutData();
			data.top.alignment = SWT.TOP;
			data.top.offset = 0;

			errorText.setVisible(false);
			detailsButton.setText(Strings.getString("dialog.error.details.show"));
			Point size = detailsButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			FormData d = (FormData) detailsButton.getLayoutData();
			if (size.x > 75)
				d.width = size.x;
			else
				d.width = 75;
			copyButton.setVisible(false);
			size = dialog.computeSize(dialog.getClientArea().width, SWT.DEFAULT);
			dialog.setSize(size);
		}
	}

	private class EventListener implements Listener {
		@Override
		public void handleEvent(Event e) {
			if (e.widget == okButton) {
				dialog.close();
			} else if (e.widget == copyButton) {
				if (errorText.getText().length() > 0) {
					Clipboard clipboard = new Clipboard(dialog.getDisplay());
					clipboard.setContents(new Object[] { errorText.getText() },
							new Transfer[] { TextTransfer.getInstance() });
				}
			} else if (e.widget == detailsButton) {
				setDetailsVisible(!errorText.isVisible());
			}
		}
	}
}