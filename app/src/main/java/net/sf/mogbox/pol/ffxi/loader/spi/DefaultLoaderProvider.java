/* 
 * DefaultLoaderProvider.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader.spi;

import java.io.IOException;
import java.nio.channels.FileChannel;

import net.sf.mogbox.pol.ffxi.loader.DMsg2Loader;
import net.sf.mogbox.pol.ffxi.loader.DMsg3Loader;
import net.sf.mogbox.pol.ffxi.loader.DataBlockLoader;
import net.sf.mogbox.pol.ffxi.loader.DialogLoader;
import net.sf.mogbox.pol.ffxi.loader.InputStreamLoader;
import net.sf.mogbox.pol.ffxi.loader.Loader;
import net.sf.mogbox.pol.ffxi.loader.NPCNameLoader;
import net.sf.mogbox.pol.ffxi.loader.PNGLoader;
import net.sf.mogbox.pol.ffxi.loader.UnsupportedFormatException;
import net.sf.mogbox.pol.ffxi.loader.XIStringLoader;

public class DefaultLoaderProvider extends LoaderProvider {
	@Override
	public Loader<?> newLoader(FileChannel channel) throws IOException {
		long position = channel.position();
		try {
			return new PNGLoader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new XIStringLoader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new DMsg2Loader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new DMsg3Loader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new DialogLoader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new DataBlockLoader(channel);
		} catch (UnsupportedFormatException e) {
		}
		channel.position(position);
		try {
			return new NPCNameLoader(channel);
		} catch (UnsupportedFormatException e) {
		}
		return new InputStreamLoader(channel);
	}
}
