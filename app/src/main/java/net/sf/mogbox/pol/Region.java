/* 
 * Region.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import net.sf.mogbox.os.OS;
import net.sf.mogbox.os.Registry;

public enum Region {
	/**
	 * No region
	 */
	NONE,

	/**
	 * Japan
	 */
	JP(""),

	/**
	 * North America
	 */
	NA("US"),

	/**
	 * European Union
	 */
	EU("EU");

	private static Region defaultPOLRegion = null;
	private static Region defaultFFXIRegion = null;
	private static Region defaultTMRegion = null;

	/**
	 * Sets the default region.
	 * 
	 * @see #getDefaultFFXIRegion()
	 * 
	 * @param region the new default PlayOnline region.
	 */
	public static void setDefaultPOLRegion(Region region) {
		defaultPOLRegion = region;
	}

	/**
	 * Returns the current default PlayOnline region.
	 * 
	 * <p>
	 * If the default region has not been set explicitly by calling
	 * {@link Region#setDefaultPOLRegion(Region)} then this method returns the first
	 * installed region found as follows:
	 * </p>
	 * 
	 * <pre>
	 * List&lt;Region&gt; regions = Region.getInstalledPOLRegions();
	 * if (regions.size() &gt; 0)
	 * 	return regions.get(0);
	 * return Region.NONE;
	 * </pre>
	 * 
	 * <p>
	 * Use <code>setDefaultPOLRegion(null)</code> to return to the default behavior.
	 * </p>
	 * 
	 * @return The default region or {@link Region#NONE} if no suitable default is
	 *         available.
	 */
	public static Region getDefaultPOLRegion() {
		if (defaultPOLRegion == null) {
			List<Region> regions = Region.getInstalledPOLRegions();
			if (regions.size() > 0)
				defaultPOLRegion = regions.get(0);
			else
				defaultPOLRegion = NONE;
		}
		return defaultPOLRegion;
	}

	/**
	 * Sets the default region.
	 * 
	 * @see #getDefaultFFXIRegion()
	 * 
	 * @param region the new default Final Fantasy XI region.
	 */
	public static void setDefaultFFXIRegion(Region region) {
		defaultFFXIRegion = region;
	}

	/**
	 * Returns the current default Final Fantasy XI region.
	 * 
	 * <p>
	 * If the default region has not been set explicitly by calling
	 * {@link Region#setDefaultFFXIRegion(Region)} then this method returns the
	 * first installed region found as follows:
	 * </p>
	 * 
	 * <pre>
	 * List&lt;Region&gt; regions = Region.getInstalledFFXIRegions();
	 * if (regions.size() &gt; 0)
	 * 	return regions.get(0);
	 * return Region.NONE;
	 * </pre>
	 * 
	 * <p>
	 * Use <code>setDefaultFFXIRegion(null)</code> to return to the default
	 * behavior.
	 * </p>
	 * 
	 * @return The default region or {@link Region#NONE} if no suitable default is
	 *         available.
	 */
	public static Region getDefaultFFXIRegion() {
		if (defaultFFXIRegion == null) {
			List<Region> regions = Region.getInstalledFFXIRegions();
			if (regions.size() > 0)
				defaultFFXIRegion = regions.get(0);
			else
				defaultFFXIRegion = NONE;
		}
		return defaultFFXIRegion;
	}

	/**
	 * Sets the default region.
	 * 
	 * @see #getDefaultTMRegion()
	 * 
	 * @param region the new default Tetra Master region.
	 */
	public static void setDefaultTMRegion(Region region) {
		defaultTMRegion = region;
	}

	/**
	 * Returns the current default Tetra Master region.
	 * 
	 * <p>
	 * If the default region has not been set explicitly by calling
	 * {@link Region#setDefaultTMRegion(Region)} then this method returns the first
	 * installed region found as follows:
	 * </p>
	 * 
	 * <pre>
	 * List&lt;Region&gt; regions = Region.getInstalledTMRegions();
	 * if (regions.size() &gt; 0)
	 * 	return regions.get(0);
	 * return Region.NONE;
	 * </pre>
	 * 
	 * <p>
	 * Use <code>setDefaultTMRegion(null)</code> to return to the default behavior.
	 * </p>
	 * 
	 * @return The default region or {@link Region#NONE} if no suitable default is
	 *         available.
	 */
	public static Region getDefaultTMRegion() {
		if (defaultTMRegion == null) {
			List<Region> regions = Region.getInstalledTMRegions();
			if (regions.size() > 0)
				defaultTMRegion = regions.get(0);
			else
				defaultTMRegion = NONE;
		}
		return defaultTMRegion;
	}

	/**
	 * Returns a {@link List} of all regions with PlayOnline installed.
	 * 
	 * @return a {@link List} of all regions with PlayOnline installed.
	 */
	public static List<Region> getInstalledPOLRegions() {
		List<Region> list = new LinkedList<Region>();
		for (Region r : values()) {
			if (r != NONE && r.isPOLInstalled())
				list.add(r);
		}
		return list;
	}

	/**
	 * Returns a {@link List} of all regions with Final Fantasy XI installed.
	 * 
	 * @return a {@link List} of all regions with Final Fantasy XI installed.
	 */
	public static List<Region> getInstalledFFXIRegions() {
		List<Region> list = new LinkedList<Region>();
		for (Region r : values()) {
			if (r != NONE && r.isFFXIInstalled())
				list.add(r);
		}
		return list;
	}

	/**
	 * Returns a {@link List} of all regions with Tetra Master installed.
	 * 
	 * @return a {@link List} of all regions with Tetra Master installed.
	 */
	public static List<Region> getInstalledTMRegions() {
		List<Region> list = new LinkedList<Region>();
		for (Region r : values()) {
			if (r != NONE && r.isFFXIInstalled())
				list.add(r);
		}
		return list;
	}

	private File polLocation;
	private File ffxiLocation;
	private File tmLocation;

	private File polLocationOverride;
	private File ffxiLocationOverride;
	private File tmLocationOverride;

	private Region() {
		File apps = OS.getWindowsApplicationLocation();
		if (apps != null) {
			polLocation = new File(apps, "PlayOnline/SquareEnix/PlayOnlineViewer/");
			if (!polLocation.exists() || !polLocation.isDirectory() || !polLocation.canRead())
				polLocation = null;
			ffxiLocation = new File(apps, "PlayOnline/SquareEnix/FINAL FANTASY XI/");
			if (!ffxiLocation.exists() || !ffxiLocation.isDirectory() || !ffxiLocation.canRead())
				ffxiLocation = null;
			tmLocation = new File(apps, "PlayOnline/SquareEnix/TetraMaster/");
			if (!tmLocation.exists() || !tmLocation.isDirectory() || !tmLocation.canRead())
				tmLocation = null;
		}
	}

	private Region(String code) {
		String key = String.format("Software\\PlayOnline%s\\InstallFolder", code);
		String key64 = String.format("Software\\Wow6432Node\\PlayOnline%s\\InstallFolder", code);
		Registry reg = Registry.getInstance();
		String value;

		if ((value = reg.query(Registry.HKLM, key, "1000")) == null)
			value = reg.query(Registry.HKLM, key64, "1000");
		if (value != null) {
			polLocation = OS.convertWindowsPath(value);
			if (!polLocation.exists() || !polLocation.isDirectory() || !polLocation.canRead())
				polLocation = null;
		}

		if ((value = reg.query(Registry.HKLM, key, "0001")) == null)
			value = reg.query(Registry.HKLM, key64, "0001");
		if (value != null) {
			ffxiLocation = OS.convertWindowsPath(value);
			if (!ffxiLocation.exists() || !ffxiLocation.isDirectory() || !ffxiLocation.canRead())
				ffxiLocation = null;
		}

		if ((value = reg.query(Registry.HKLM, key, "0002")) == null)
			value = reg.query(Registry.HKLM, key64, "0002");
		if (value != null) {
			tmLocation = OS.convertWindowsPath(value);
			if (!tmLocation.exists() || !tmLocation.isDirectory() || !tmLocation.canRead())
				tmLocation = null;
		}
	}

	/**
	 * Sets the PlayOnline installation directory for this <code>Region</code>.
	 * 
	 * @see #getPOLLocation()
	 * 
	 * @param location The installation directory for PlayOnline
	 */
	public void setPOLLocation(File location) {
		if (location == null)
			polLocationOverride = null;
		else if (location.exists() && location.canRead())
			polLocationOverride = location;
	}

	/**
	 * Returns the PlayOnline installation directory for this <code>Region</code>.
	 * 
	 * <p>
	 * If the location has not been set explicitly by calling
	 * {@link #setPOLLocation(File)} then this method will attempt to find the
	 * installation directory by reading the registry. Use
	 * <code>setPOLLocation(null)</code> to return to the default behavior.
	 * </p>
	 * 
	 * @return the PlayOnline installation directory for this <code>Region</code> or
	 *         <code>null</code> if PlayOnline is not installed.
	 */
	public File getPOLLocation() {
		return polLocation;
	}

	/**
	 * Sets the Final Fantasy XI installation directory for this
	 * <code>Region</code>.
	 * 
	 * @see #getFFXILocation()
	 * 
	 * @param location The installation directory for Final Fantasy XI
	 */
	public void setFFXILocation(File location) {
		if (location == null)
			ffxiLocationOverride = null;
		else if (location.exists() && location.canRead())
			ffxiLocationOverride = location;
	}

	/**
	 * Returns the Final Fantasy XI installation directory for this
	 * <code>Region</code>.
	 * 
	 * <p>
	 * If the location has not been set explicitly by calling
	 * {@link #setFFXILocation(File)} then this method will attempt to find the
	 * installation directory by reading the registry. Use
	 * <code>setFFXILocation(null)</code> to return to the default behavior.
	 * </p>
	 * 
	 * @return the Final Fantasy XI installation directory for this
	 *         <code>Region</code> or <code>null</code> if Final Fantasy XI is not
	 *         installed.
	 */
	public File getFFXILocation() {
		if (ffxiLocationOverride != null)
			return ffxiLocationOverride;
		return ffxiLocation;
	}

	/**
	 * Sets the Tetra Master installation directory for this <code>Region</code> .
	 * 
	 * @see #getTMLocation()
	 * 
	 * @param location The installation directory for Tetra Master
	 */
	public void setTMLocation(File location) {
		if (location == null)
			tmLocationOverride = null;
		else if (location.exists() && location.canRead())
			tmLocationOverride = location;
	}

	/**
	 * Returns the Tetra Master installation directory for this <code>Region</code>.
	 * 
	 * <p>
	 * If the location has not been set explicitly by calling
	 * {@link #setTMLocation(File)} then this method will attempt to find the
	 * installation directory by reading the registry. Use
	 * <code>setTMLocation(null)</code> to return to the default behavior.
	 * </p>
	 * 
	 * @return the Tetra Master installation directory for this <code>Region</code>
	 *         or <code>null</code> if Tetra Master is not installed.
	 */
	public File getTMLocation() {
		return tmLocation;
	}

	/**
	 * Returns whether PlayOnline has been installed for this region.
	 * 
	 * @return <code>true</code> if PlayOnline is installed for this
	 *         <code>Region</code> or false otherwise.
	 */
	public boolean isPOLInstalled() {
		return polLocation != null || polLocationOverride != null;
	}

	/**
	 * Returns whether Final Fantasy XI has been installed for this region.
	 * 
	 * @return <code>true</code> if Final Fantasy XI is installed for this
	 *         <code>Region</code> or false otherwise.
	 */
	public boolean isFFXIInstalled() {
		return ffxiLocation != null || ffxiLocationOverride != null;
	}

	/**
	 * Returns whether Tetra Master has been installed for this region.
	 * 
	 * @return <code>true</code> if Tetra Master is installed for this
	 *         <code>Region</code> or false otherwise.
	 */
	public boolean isTMInstalled() {
		return tmLocation != null || tmLocationOverride != null;
	}
}
