/* 
 * ShiftJISFFXICharsetProvider.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.charset.spi;

import java.nio.charset.Charset;
import java.nio.charset.spi.CharsetProvider;
import java.util.Iterator;
import java.util.NoSuchElementException;

import net.sf.mogbox.pol.ffxi.charset.ShiftJISFFXICharset;

public class ShiftJISFFXICharsetProvider extends CharsetProvider {
	private static final Charset charset = new ShiftJISFFXICharset();

	@Override
	public Charset charsetForName(String charsetName) {
		if (charset.name().equals(charsetName))
			return charset;
		for (String alias : charset.aliases())
			if (alias.equals(charsetName))
				return charset;
		return null;
	}

	@Override
	public Iterator<Charset> charsets() {
		return new ShiftJISFFXICharsetIterator();
	}

	private class ShiftJISFFXICharsetIterator implements Iterator<Charset> {
		boolean next = true;

		@Override
		public boolean hasNext() {
			return next;
		}

		@Override
		public Charset next() {
			if (next) {
				next = false;
				return charset;
			}
			throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
