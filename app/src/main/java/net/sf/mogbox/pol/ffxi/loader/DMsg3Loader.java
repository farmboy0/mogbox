/* 
 * DMsg3Loader.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.charset.ShiftJISFFXICharsetDecoder;

public class DMsg3Loader extends StringLoader {
	private static final Charset CHARSET = Charset.forName("x-Shift_JIS:FFXI");

	private static Logger log = Logger.getLogger(DMsg3Loader.class.getName());

	private ByteBuffer buffer;
	private CharBuffer string;

	private int entryCount;
	private int entrySize;

	private CharsetDecoder decoder;

	public DMsg3Loader(FileChannel channel) throws IOException {
		super(channel);
		if (channel.size() < 0x40)
			throw new UnsupportedFormatException();

		buffer = ByteBuffer.allocate(0x40).order(ByteOrder.LITTLE_ENDIAN);

		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			throw new UnsupportedFormatException();
		buffer.flip();

		if (buffer.getLong(0x00) != 0x00000067736D5F64L) // d_msg
			throw new UnsupportedFormatException();

		if (buffer.getInt(0x14) != channel.size()) // File Size
			throw new UnsupportedFormatException();
		if (buffer.getInt(0x18) != 0x00000040) // Header Size
			throw new UnsupportedFormatException();

		if (buffer.getInt(0x1C) != 0) // Version 2 Table Size
			throw new UnsupportedFormatException();
		entrySize = buffer.getInt(0x20);
		if (entrySize == 0)
			throw new UnsupportedFormatException();
		int dataSize = buffer.getInt(0x24);
		if (dataSize + 0x40 != channel.size())
			throw new UnsupportedFormatException();
		entryCount = buffer.getInt(0x28);

		if (entryCount * entrySize != dataSize)
			throw new UnsupportedFormatException();

		string = CharBuffer.allocate(0x40);

		decoder = CHARSET.newDecoder();
		decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		decoder.onMalformedInput(CodingErrorAction.REPLACE);
		if (decoder instanceof ShiftJISFFXICharsetDecoder)
			((ShiftJISFFXICharsetDecoder) decoder).enableVariables(false);
	}

	@Override
	protected String[] getNextElement() throws IOException {
		if (channel.position() >= channel.size())
			return null;

		int entryOff = (int) channel.position();

		buffer.clear().limit(0x04);
		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			throw new UnsupportedFormatException();
		buffer.flip();

		boolean flip = false;
		int stringCount = buffer.getInt(0x00);
		if (stringCount < 0) {
			flip = true;
			stringCount = ~stringCount;
		}

		channel.position(entryOff + entrySize);

		String[] strings = new String[stringCount];
		int dataStart = entryOff + stringCount * 0x08 + 0x04;

		for (int i = 0; i < stringCount; i++) {
			int off = entryOff + i * 0x08 + 0x04;
			buffer.clear().limit(0x08);
			int read;
			do
				off += read = channel.read(buffer, off);
			while (buffer.hasRemaining() && read >= 0);
			if (read < 0 || buffer.hasRemaining())
				return null;
			off = buffer.getInt(0x00);
			if (flip)
				off = ~off;
			buffer.flip();
			strings[i] = readString(dataStart + off, 0, flip);
		}

		return strings;
	}

	private String readString(int off, int len, boolean flip) throws IOException {
		decoder.reset();
		buffer.clear();

		StringBuilder out;
		if (len == 0)
			out = new StringBuilder();
		else
			out = new StringBuilder((len + 0x39) / 0x40 * 0x40);

		CoderResult result = null;
		while (true) {
			if (result == null || result.isUnderflow()) {
				if (result != null)
					buffer.compact();
				buffer.mark();
				int read;
				do
					off += read = channel.read(buffer, off);
				while (buffer.hasRemaining() && read >= 0);
				if (flip) {
					buffer.limit(buffer.position());
					buffer.reset();
					while (buffer.hasRemaining())
						buffer.put((byte) ~buffer.get(buffer.position()));
				}
				buffer.flip();
			} else {
				string.flip();
				out.append(string);
				string.clear();
				int nullIndex = out.indexOf("\0");
				if (nullIndex >= 0) {
					out.setLength(nullIndex);
					break;
				} else if (result.isError()) {
					log.log(Level.WARNING, result.toString());
					break;
				}
			}
			result = decoder.decode(buffer, string, false);
		}
		return out.toString();
	}
}
