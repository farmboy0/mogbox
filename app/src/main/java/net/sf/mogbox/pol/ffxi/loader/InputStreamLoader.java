package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;

public class InputStreamLoader extends Loader<InputStream> {
	boolean loaded = false;

	public InputStreamLoader(FileChannel channel) throws IOException {
		super(channel);
	}

	@Override
	protected InputStream getNextElement() throws IOException {
		if (loaded)
			return null;
		return Channels.newInputStream(channel);
	}
}
