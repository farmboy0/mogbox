/*
 * DialogLoader.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of Moogle Toolbox.
 * 
 * Moogle Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Moogle Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Moogle Toolbox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify Moogle Toolbox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DialogLoader extends StringLoader {
	private static final Charset CHARSET = Charset.forName("x-Shift_JIS:FFXI");

	private static Logger log = Logger.getLogger(DMsg1Loader.class.getName());

	private ByteBuffer buffer;
	private CharBuffer string;

	private int firstString;

	private CharsetDecoder decoder;

	public DialogLoader(FileChannel channel) throws IOException {
		super(channel);
		if (channel.size() < 0x04)
			throw new UnsupportedFormatException();

		buffer = ByteBuffer.allocate(0x40).order(ByteOrder.LITTLE_ENDIAN);

		buffer.limit(0x04);
		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			throw new UnsupportedFormatException();
		buffer.flip();

		if ((buffer.getInt(0x00) & 0x0FFFFFFF) + 4 != channel.size())
			throw new UnsupportedFormatException();

		firstString = (int) channel.size();

		string = CharBuffer.allocate(0x40);

		decoder = CHARSET.newDecoder();
		decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		decoder.onMalformedInput(CodingErrorAction.REPLACE);
	}

	@Override
	protected String[] getNextElement() throws IOException {
		if (channel.position() >= firstString)
			return null;

		buffer.clear().limit(0x04);
		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			return null;
		buffer.flip();

		int off = (buffer.getInt(0x00) ^ 0x80808080) + 4;
		if (off >= channel.size())
			return null;

		if (off < firstString)
			firstString = off;

		decoder.reset();
		buffer.clear();

		StringBuilder out = new StringBuilder();

		CoderResult result = null;
		while (true) {
			if (result == null || result.isUnderflow()) {
				if (result != null)
					buffer.compact();
				buffer.mark();
				int read;
				do
					off += read = channel.read(buffer, off);
				while (buffer.hasRemaining() && read >= 0);
				buffer.limit(buffer.position());
				buffer.reset();
				while (buffer.hasRemaining())
					buffer.put((byte) (0x80 ^ buffer.get(buffer.position())));
				buffer.flip();
			} else {
				string.flip();
				out.append(string);
				string.clear();
				int nullIndex = out.indexOf("\0");
				if (nullIndex >= 0) {
					out.setLength(nullIndex);
					break;
				} else if (result.isError()) {
					log.log(Level.WARNING, result.toString());
					break;
				}
			}
			result = decoder.decode(buffer, string, false);
		}

		return new String[] { out.toString() };
	}

}
