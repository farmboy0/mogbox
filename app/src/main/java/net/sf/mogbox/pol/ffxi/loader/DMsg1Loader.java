/* 
 * DMsg1Loader.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.charset.ShiftJISFFXICharsetDecoder;

/*
 * This format has been completely replaced by d_msg versions 2 and 3, and at
 * the time of writing no samples of this format still exist in the fully
 * updated Final Fantasy XI file tree. As such, this class is wholly untested,
 * and will remain so until I can find a copy that has been left in a state
 * prior to the December 18, 2006 update when version 2 was introduced.
 * 
 * This format is not considered by the default loader for auto-detection.
 */
public class DMsg1Loader extends StringLoader {
	private static final Charset CHARSET = Charset.forName("x-Shift_JIS:FFXI");

	private static Logger log = Logger.getLogger(DMsg1Loader.class.getName());

	private ByteBuffer buffer;
	private CharBuffer string;

	private int entryCount;
	private int tableSize;

	private CharsetDecoder decoder;

	public DMsg1Loader(FileChannel channel) throws IOException {
		super(channel);
		if (channel.size() < 0x38)
			throw new UnsupportedFormatException();

		buffer = ByteBuffer.allocate(0x40).order(ByteOrder.LITTLE_ENDIAN);

		buffer.limit(0x38);
		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			throw new UnsupportedFormatException();
		buffer.flip();

		if (buffer.getLong(0x00) != 0x00000067736D5F64L) // d_msg
			throw new UnsupportedFormatException();

		entryCount = buffer.getInt(0x14);

		if (buffer.getInt(0x1C) != channel.size()) // File Size
			throw new UnsupportedFormatException();
		if (buffer.getInt(0x20) != 0x38) // Header Size
			throw new UnsupportedFormatException();

		tableSize = buffer.getInt(0x24);
		if (entryCount * 0x24 != tableSize)
			throw new UnsupportedFormatException();
		if (buffer.getInt(0x28) + tableSize + 0x38 != channel.size()) // Data Size
			throw new UnsupportedFormatException();

		string = CharBuffer.allocate(0x40);

		decoder = CHARSET.newDecoder();
		decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		decoder.onMalformedInput(CodingErrorAction.REPLACE);
		if (decoder instanceof ShiftJISFFXICharsetDecoder)
			((ShiftJISFFXICharsetDecoder) decoder).enableVariables(false);
	}

	@Override
	protected String[] getNextElement() throws IOException {
		if (channel.position() >= tableSize + 0x38)
			return null;

		buffer.clear().limit(0x24);
		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			return null;
		buffer.flip();

		int off = buffer.getInt(0x00) + tableSize + 0x38;
		if (off >= channel.size())
			return null;
		int len = buffer.getShort(0x08);

		decoder.reset();
		buffer.clear();

		StringBuilder out = new StringBuilder((len + 0x39) / 0x40 * 0x40);

		CoderResult result = null;
		while (true) {
			if (result == null || result.isUnderflow()) {
				if (result != null)
					buffer.compact();
				int read;
				do
					off += read = channel.read(buffer, off);
				while (buffer.hasRemaining() && read >= 0);
				buffer.flip();
			} else {
				string.flip();
				out.append(string);
				string.clear();
				int nullIndex = out.indexOf("\0");
				if (nullIndex >= 0) {
					out.setLength(nullIndex);
					break;
				} else if (result.isError()) {
					log.log(Level.WARNING, result.toString());
					break;
				}
			}
			result = decoder.decode(buffer, string, false);
		}

		return new String[] { out.toString() };
	}
}
