/* 
 * ShiftJISFFXICharset.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.charset;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

public class ShiftJISFFXICharset extends Charset {
	private static final String NAME = "x-Shift_JIS:FFXI";
	private static final String[] ALIASES = { "Shift_JIS:FFXI", "shift_jis:ffxi", "SJIS:FFXI", "sjis:ffxi",
			"X-Shift_JIS:FFXI", "x-shift_jis:ffxi", "x-sjis:ffxi", "ms_kanji:ffxi", "csShiftJIS:FFXI" };

	public ShiftJISFFXICharset() {
		super(NAME, ALIASES);
	}

	@Override
	public boolean contains(Charset cs) {
		if (cs instanceof ShiftJISFFXICharset)
			return true;
		String name = cs.name();
		if (name.equals("US-ASCII"))
			return true;
		if (name.equals("Shift_JIS"))
			return true;
		if (name.equals("JIS_X0201"))
			return true;
		if (name.equals("x-JIS0208"))
			return true;
		return false;
	}

	@Override
	public CharsetDecoder newDecoder() {
		return new ShiftJISFFXICharsetDecoder(this);
	}

	@Override
	public CharsetEncoder newEncoder() {
		return new ShiftJISFFXICharsetEncoder(this);
	}
}
