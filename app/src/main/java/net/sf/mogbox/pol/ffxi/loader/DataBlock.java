/* 
 * DataBlock.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class DataBlock {
	public static final byte END_CONTAINER = 0x00;
	public static final byte BEGIN_CONTAINER = 0x01;
	public static final byte SCHEDULE = 0x07;
	public static final byte TERRAIN_LAYOUT = 0x1C;
	public static final byte TEXTURE = 0x20;
	public static final byte SKELETON = 0x29;
	public static final byte MODEL = 0x2A;
	public static final byte MOTION = 0x2B;
	public static final byte TERRAIN_MODEL = 0x2E;

	private int id;
	private byte type;
	private FileChannel channel;
	private int offset;
	private int size;
	private ByteBuffer data;

	public DataBlock(int id, byte type, FileChannel channel, int offset, int size) {
		this.id = id;
		this.type = type;
		this.channel = channel;
		this.offset = offset;
		this.size = size;
	}

	public int getID() {
		return id;
	}

	public byte getType() {
		return type;
	}

	public ByteBuffer getData() {
		if (size <= 0)
			return null;
		if (data == null) {
			data = ByteBuffer.allocate(size).order(ByteOrder.LITTLE_ENDIAN);
			try {
				int read = 0, readtotal = 0;
				do {
					read = channel.read(data, offset + readtotal);
					readtotal += read;
				} while (data.hasRemaining() && read >= 0);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			data.clear();
		}
		return data;
	}
}
