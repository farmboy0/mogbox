/* 
 * DataBlockLoader.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class DataBlockLoader extends Loader<DataBlock> {
	private ByteBuffer b;

	public DataBlockLoader(FileChannel channel) throws IOException {
		super(channel);
		b = ByteBuffer.allocate(16).order(ByteOrder.LITTLE_ENDIAN);
		if (channel.size() % 16 != 0)
			throw new UnsupportedFormatException();
		channel.position(0);
		long position = 0;
		while (position < channel.size()) {
			while (b.hasRemaining() && (position += channel.read(b, position)) >= 0) {
			}
			if (b.hasRemaining())
				throw new UnsupportedFormatException();
			b.flip();
			int size = b.getInt(4) >> 3 & 0x00FFFFF0;
			if (size == 0) {
				throw new UnsupportedFormatException();
			}
			position += size - 16;
			if (position > channel.size())
				throw new UnsupportedFormatException();
			b.clear();
		}
	}

	@Override
	protected DataBlock getNextElement() throws IOException {
		if (channel.position() >= channel.size())
			return null;
		b.clear();
		while (b.hasRemaining() && channel.read(b) >= 0) {
		}
		b.flip();
		int id = b.getInt();
		int size = b.getInt();
		byte type = (byte) (size & 0x7F);
		size = (size >> 3 & 0x00FFFFF0) - 16;
		int offset = (int) channel.position();
		channel.position(channel.position() + size);
		return new DataBlock(id, type, channel, offset, size);
	}
}
