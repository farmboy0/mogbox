/* 
 * PNGLoader.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;

public class PNGLoader extends Loader<InputStream> {
	private boolean loaded = false;

	public PNGLoader(FileChannel channel) throws IOException {
		super(channel);

		ByteBuffer b = ByteBuffer.allocate(16).order(ByteOrder.LITTLE_ENDIAN);
		int position = 0;
		while (b.hasRemaining() && (position += channel.read(b, position)) >= 0) {
		}
		if (b.hasRemaining())
			throw new UnsupportedFormatException();
		b.flip();
		if (b.getLong(0) != 0x0A1A0A0D474E5089L)
			throw new UnsupportedFormatException();
		if (b.getLong(8) != 0x524448490D000000L)
			throw new UnsupportedFormatException();
	}

	@Override
	protected InputStream getNextElement() throws IOException {
		if (loaded)
			return null;
		loaded = true;
		return Channels.newInputStream(channel);
	}
}
