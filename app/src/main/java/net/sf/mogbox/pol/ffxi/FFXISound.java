/* 
 * FFXISound.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import net.sf.mogbox.pol.ffxi.loader.UnsupportedFormatException;

public class FFXISound extends net.sf.mogbox.renderer.engine.Sound {
	public static final int FORMAT_ADPCM = 0;
	public static final int FORMAT_PCM = 1;
	public static final int FORMAT_ATRAC3 = 3;

	public static final int BUFFER_SIZE = 4096;
	public static final int BUFFER_COUNT = 3;

	private static final Charset ASCII = Charset.forName("US-ASCII");

	private FileChannel channel;

	private int type;
	private int fileSize;
	private int fileID;
	private int format;

	private Decoder decoder;

	public FFXISound(FileChannel channel) throws IOException {
		if (channel.size() < 0x30)
			throw new UnsupportedFormatException("Invalid header");
		this.channel = channel;

		ByteBuffer header = ByteBuffer.allocate(0x30).order(ByteOrder.LITTLE_ENDIAN);
		channel.read(header);
		header.flip().limit(0x10);
		String typeName = ASCII.decode(header).toString();
		typeName = typeName.substring(0, typeName.indexOf('\0'));
		header.clear();

		int channels = 1;
		int bitsPerSample = 16;
		int sampleRate = 44100;
		int length = 0;
		int loopPoint = -1;

		if ("BGMStream".equals(typeName)) {
			type = 0;
			fileSize = header.getInt(0x10);
			if (fileSize != channel.size())
				throw new UnsupportedFormatException();
			fileID = header.getInt(0x14);
			length = header.getInt(0x18);
			loopPoint = header.getInt(0x1C);
			sampleRate = (header.getInt(0x20) + header.getInt(0x24)) & 0x7FFFFFFF;
			header.getInt(0x28); // unknown
			header.get(0x2C); // unknown
			header.get(0x2D); // unknown
			channels = header.get(0x2E);
			bitsPerSample = header.get(0x2F);
			if (bitsPerSample == 0) {
				format = FORMAT_ATRAC3;
				bitsPerSample = 16;
			} else {
				format = FORMAT_ADPCM;
			}
		} else if ("SeWave".equals(typeName)) {
			type = 1;
			fileSize = header.getInt(0x08);
			if (fileSize != channel.size())
				throw new UnsupportedFormatException();
			format = header.getInt(0x0C);
			fileID = header.getInt(0x10);
			length = header.getInt(0x14);
			loopPoint = header.getInt(0x18);
			sampleRate = (header.getInt(0x1C) + header.getInt(0x20)) & 0x7FFFFFFF;
			header.getInt(0x24); // unknown
			header.get(0x28); // unknown
			header.get(0x29); // unknown
			channels = header.get(0x2A);
			bitsPerSample = header.get(0x2B);
		}

		if (format == FORMAT_ADPCM)
			length *= 16;

		setFormat(channels, bitsPerSample, sampleRate);
		setLength(length, loopPoint);
	}

	public int getType() {
		return type;
	}

	public int getFileID() {
		return fileID;
	}

	public int getFormat() {
		return format;
	}

	protected void reset(boolean loop) throws IOException {
		if (decoder == null) {
			channel.position(0x30);
			switch (format) {
			case FORMAT_ADPCM:
				decoder = new ADPCMDecoder(this, channel);
				break;
			case FORMAT_PCM:
				decoder = new PCMDecoder(this, channel);
				break;
			case FORMAT_ATRAC3:
				decoder = new ATRAC3Decoder(this, channel);
				break;
			default:
				throw new UnsupportedFormatException();
			}
		}
		if (loop)
			channel.position(0x30 + decoder.getLoopOffset());
		else
			channel.position(0x30);
		decoder.reset(loop);
	}

	protected boolean load(ByteBuffer data) throws IOException {
		decoder.decode(channel, data);
		return channel.position() < channel.size();
	}

	@Override
	protected int getSamplesPerFrame() {
		return decoder.getSamplesPerFrame();
	}
}
