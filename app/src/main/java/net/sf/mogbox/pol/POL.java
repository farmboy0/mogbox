/* 
 * POL.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.EnumMap;
import java.util.Map;

public class POL {
	private static final Map<Region, String> VERSION_STRINGS = new EnumMap<Region, String>(Region.class);
	private static final Map<Region, String> INTERNAL_VERSION_STRINGS = new EnumMap<Region, String>(Region.class);

	public static long getPatchTime() {
		return getPatchTime(Region.getDefaultPOLRegion());
	}

	public static long getPatchTime(Region region) {
		File patch = new File(region.getPOLLocation(), "patch.ver");
		return patch.lastModified();
	}

	public static String getVersionString() {
		return getVersionString(Region.getDefaultPOLRegion());
	}

	public static String getVersionString(Region region) {
		String version = VERSION_STRINGS.get(region);
		if (version != null)
			return version;

		try {
			File file = new File(region.getPOLLocation(), "version.dat");
			Reader in = new InputStreamReader(new BufferedInputStream(new FileInputStream(file)),
					Charset.forName("US-ASCII"));

			StringBuilder output = new StringBuilder();
			int length;
			char[] buffer = new char[8];
			do {
				length = in.read(buffer);
				if (length > 0)
					output.append(buffer, 0, length);
			} while (length >= 0);

			version = output.toString();
		} catch (Throwable t) {
			version = "Unknown";
		}

		VERSION_STRINGS.put(region, version);

		return version;
	}

	public static String getInternalVersionString() {
		return getInternalVersionString(Region.getDefaultPOLRegion());
	}

	public static String getInternalVersionString(Region region) {
		String version = INTERNAL_VERSION_STRINGS.get(region);
		if (version != null)
			return version;

		/*
		 * Parse patch.cfg to find the version string. I don't have a clue what this
		 * file is used for but It's the only place I know of to find Final Fantasy XI's
		 * version string.
		 * 
		 * The current version is on the last line of the first "{ }" block.
		 */
		try {
			File file = new File(region.getPOLLocation(), "patch.cfg");
			BufferedReader in = new BufferedReader(
					new InputStreamReader(new FileInputStream(file), Charset.forName("US-ASCII")));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.contains("}"))
					break;
				version = line;
			}
			int index = version.indexOf(" ");
			version = version.substring(0, index);
		} catch (Throwable t) {
			version = "Unknown";
		}

		INTERNAL_VERSION_STRINGS.put(region, version);

		return version;
	}

	private POL() {
	}
}
