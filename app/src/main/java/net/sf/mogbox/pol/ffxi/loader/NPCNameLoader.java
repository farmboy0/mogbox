/*
 * NPCNameLoader.java
 * 
 * Copyright © 2009 Sean Whalen <alphaone2@gmail.com>
 * 
 * This file is part of Moogle Toolbox.
 * 
 * Moogle Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Moogle Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Moogle Toolbox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify Moogle Toolbox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

/**
 * @author Sean Whalen <alphaone2@gmail.com>
 * 
 */
public class NPCNameLoader extends StringLoader {
	private static final Charset CHARSET = Charset.forName("US-ASCII");

	private ByteBuffer buffer;
	private CharBuffer string;

	private int currentIndex;
	private int nextIndex;
	private String nextString;

	private CharsetDecoder decoder;

	/**
	 * @param channel
	 * @throws IOException
	 */
	public NPCNameLoader(FileChannel channel) throws IOException {
		super(channel);

		if (channel.size() % 0x1C != 0 || channel.size() == 0)
			throw new UnsupportedFormatException();

		buffer = ByteBuffer.allocate(0x1C).order(ByteOrder.LITTLE_ENDIAN);

		while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
		}
		if (buffer.hasRemaining())
			throw new UnsupportedFormatException();
		buffer.flip();

		nextIndex = buffer.getInt(0x18);
		if (nextIndex != 0)
			throw new UnsupportedFormatException();

		string = CharBuffer.allocate(0x18);
		decoder = CHARSET.newDecoder();
		decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		decoder.onMalformedInput(CodingErrorAction.REPLACE);

		buffer.limit(0x18);
		decoder.reset();
		if (decoder.decode(buffer, string, true).isError())
			throw new UnsupportedFormatException();
		string.flip();
		nextString = string.toString();
		if (!"none\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0".equals(nextString))
			throw new UnsupportedFormatException();
		int end = nextString.indexOf('\0');
		if (end > 0)
			nextString = nextString.substring(0, end);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mogbox.pol.ffxi.loader.Loader#getNextElement()
	 */
	@Override
	protected String[] getNextElement() throws IOException {
		if (nextString != null) {
			if (nextIndex <= currentIndex) {
				String[] out = { nextString };
				nextString = null;
				currentIndex++;
				return out;
			}
			currentIndex++;
			return new String[] { "" };
		} else {
			if (channel.position() >= channel.size())
				return null;

			buffer.clear();
			string.clear();

			while (buffer.hasRemaining() && channel.read(buffer) >= 0) {
			}
			if (buffer.hasRemaining())
				return null;
			buffer.flip();

			nextIndex = buffer.getInt(0x18) & 0x00000FFF;

			buffer.limit(0x18);
			decoder.reset();
			if (decoder.decode(buffer, string, true).isError())
				return null;
			string.flip();
			nextString = string.toString();
			int end = nextString.indexOf('\0');
			if (end >= 0)
				nextString = nextString.substring(0, end);

			if (nextIndex <= currentIndex) {
				String[] out = { nextString };
				nextString = null;
				currentIndex++;
				return out;
			}
			currentIndex++;
			return new String[] { "" };
		}
	}
}
