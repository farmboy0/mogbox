/* 
 * Loader.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi.loader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.pol.ffxi.loader.spi.LoaderProvider;

public abstract class Loader<T> implements Iterator<T>, Iterable<T> {
	private static final List<LoaderProvider> providers = new LinkedList<LoaderProvider>();

	private static Logger log = Logger.getLogger(Loader.class.getName());

	static {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Enumeration<URL> urls = null;
		try {
			urls = cl.getResources("META-INF/services/mogbox.pol.ffxi.loader.spi.LoaderProvider");
			Charset utf8 = Charset.forName("UTF-8");
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), utf8));
				try {
					String l;
					while ((l = in.readLine()) != null) {
						try {
							int comment = l.indexOf('#');
							if (comment != -1)
								l = l.substring(0, comment);
							l = l.trim();
							if (l.length() != 0) {
								Object o = cl.loadClass(l).newInstance();
								if (o instanceof LoaderProvider)
									providers.add((LoaderProvider) o);
							}
						} catch (ClassNotFoundException e) {
							log.log(Level.WARNING, null, e);
						} catch (InstantiationException e) {
							log.log(Level.WARNING, null, e);
						} catch (IllegalAccessException e) {
							log.log(Level.WARNING, null, e);
						}
					}
				} catch (IOException e) {
					log.log(Level.WARNING, null, e);
				} finally {
					in.close();
				}
			}
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
		}
	}

	public static Loader<?> newInstance(FileChannel channel) {
		for (LoaderProvider p : providers) {
			try {
				return p.newLoader(channel);
			} catch (UnsupportedFormatException e) {
				log.log(Level.FINE, null, e);
			} catch (IOException e) {
				log.log(Level.WARNING, null, e);
			}
		}
		throw new UnsupportedFormatException();
	}

	protected FileChannel channel;

	protected T nextElement;

	public Loader(FileChannel channel) throws IOException {
		this.channel = channel;
	}

	@Override
	public boolean hasNext() {
		if (nextElement != null)
			return true;
		try {
			nextElement = getNextElement();
			return nextElement != null;
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
			return false;
		}
	}

	@Override
	public Iterator<T> iterator() {
		return this;
	}

	@Override
	public T next() {
		T next;
		if (nextElement != null) {
			next = nextElement;
			nextElement = null;
			return next;
		}
		try {
			next = getNextElement();
			if (next == null)
				throw new NoSuchElementException();
			return next;
		} catch (IOException e) {
			log.log(Level.WARNING, null, e);
			throw new NoSuchElementException(e.getMessage());
		}
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	protected abstract T getNextElement() throws IOException;
}
