/* 
 * PCMDecoder.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.pol.ffxi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class PCMDecoder implements Decoder {
	private int channels;
	private int frameSize;
	private int loopOffset;

	public PCMDecoder(FFXISound sound, ReadableByteChannel in) {
		channels = sound.getChannels();
		frameSize = channels * (sound.getBitsPerSample() >> 3);
		loopOffset = sound.getLoopPoint() * frameSize;
	}

	@Override
	public int getFrameSize() {
		return frameSize;
	}

	@Override
	public int getSamplesPerFrame() {
		return 1;
	}

	@Override
	public int getLoopOffset() {
		return loopOffset;
	}

	@Override
	public void reset(boolean loop) {
	}

	@Override
	public void decode(ReadableByteChannel in, ByteBuffer out) throws IOException {
		int read;
		do {
			read = in.read(out);
		} while (read >= 0 && out.hasRemaining());
	}
}
