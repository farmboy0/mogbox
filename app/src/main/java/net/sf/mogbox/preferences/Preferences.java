/* 
 * Preferences.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.preferences;

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.text.Collator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import net.sf.mogbox.os.OS;
import net.sf.mogbox.plugin.Plugin;

public final class Preferences {
	private static final String Q_NAME = "preferences";
	private static final String XML_NAMESPACE = "urn:xmlns:mogbox:prefs:1.0";
	private static final String SCHEMA_SOURCE = "preferences.xsd";

	private static final MapSerializer SERIALIZER = new MapSerializer(Q_NAME, XML_NAMESPACE, SCHEMA_SOURCE);

	private static final Map<String, Preferences> PREFERENCES_MAP = new HashMap<>();

	private File location;
	private Map<String, Object> map;
	private boolean changed = true;

	private Preferences(File location) {
		this.location = requireNonNull(location);
		map = new TreeMap<>(Collator.getInstance(Locale.US));
		loadPreferences();
	}

	public static Preferences getInstance(Object o) {
		String name = requireNonNull(Plugin.getPlugin(o).getName());
		return PREFERENCES_MAP.computeIfAbsent(name,
				k -> new Preferences(new File(OS.getUserSettingsLocation(), "plugins/" + name + ".xml")));
	}

	public static Preferences getRootInstance() {
		return PREFERENCES_MAP.computeIfAbsent(null,
				k -> new Preferences(new File(OS.getUserSettingsLocation(), "settings.xml")));
	}

	public static void save() throws IOException {
		for (Preferences p : PREFERENCES_MAP.values()) {
			p.savePreferences();
		}
	}

	private void loadPreferences() {
		if (!location.exists() || !location.isFile() || !location.canRead())
			return;

		map.putAll(SERIALIZER.deserialize(location));
	}

	private void savePreferences() throws IOException {
		if (!changed || map.isEmpty())
			return;

		if (!location.getParentFile().exists())
			location.getParentFile().mkdirs();

		SERIALIZER.serialize(map, location);
	}

	private void set(String name, Object value) {
		changed = true;
		map.put(name, requireNonNull(value));
	}

	public void reset() {
		map.clear();
	}

	public void remove(String name) {
		changed = true;
		map.remove(name);
	}

	public void setString(String name, String value) {
		set(name, value);
	}

	public String getString(String name, String defaultValue) {
		Object o = map.get(name);
		if (o != null)
			return o.toString();
		return defaultValue;
	}

	public void setBoolean(String name, boolean value) {
		set(name, value);
	}

	public boolean getBoolean(String name, boolean defaultValue) {
		Object o = map.get(name);
		if (o != null) {
			if (o instanceof Boolean)
				return (Boolean) o;
			else if (o instanceof Number)
				return ((Number) o).intValue() == 1;
			else
				return Boolean.parseBoolean(o.toString());
		}
		return defaultValue;
	}

	public void setByte(String name, byte value) {
		set(name, value);
	}

	public byte getByte(String name, byte defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).byteValue();
		return defaultValue;
	}

	public void setShort(String name, short value) {
		set(name, value);
	}

	public short getShort(String name, short defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).shortValue();
		return defaultValue;
	}

	public void setInt(String name, int value) {
		set(name, value);
	}

	public int getInt(String name, int defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).intValue();
		return defaultValue;
	}

	public void setLong(String name, long value) {
		set(name, value);
	}

	public long getLong(String name, long defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).longValue();
		return defaultValue;
	}

	public void setFloat(String name, float value) {
		set(name, value);
	}

	public float getFloat(String name, float defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).floatValue();
		return defaultValue;
	}

	public void setDouble(String name, double value) {
		set(name, value);
	}

	public double getDouble(String name, double defaultValue) {
		Object o = map.get(name);
		if (o instanceof Number)
			return ((Number) o).doubleValue();
		return defaultValue;
	}

	public void setData(String name, byte[] value) {
		set(name, value);
	}

	public byte[] getData(String name, byte[] defaultValue) {
		Object o = map.get(name);
		if (o instanceof byte[])
			return (byte[]) o;
		return defaultValue;
	}
}
