package net.sf.mogbox.preferences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

public class MapSerializer {
	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	private static final char[] TAB_NEWLINE = new char[] { '\t', '\n' };

	private static final Logger log = Logger.getLogger(MapSerializer.class.getName());

	private String rootElementName;
	private String rootNamespace;
	private String schemaSource;

	public MapSerializer(String rootElementName, String rootNamespace, String schemaSource) {
		this.rootElementName = rootElementName;
		this.rootNamespace = rootNamespace;
		this.schemaSource = schemaSource;
	}

	public void serialize(Map<String, Object> values, File destination) throws IOException {
		StreamResult streamResult = new StreamResult(new FileOutputStream(destination));
		SAXTransformerFactory factory = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
		try {
			TransformerHandler out = factory.newTransformerHandler();
			out.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
			out.setResult(streamResult);

			AttributesImpl attrs = new AttributesImpl();

			out.startDocument();
			out.startElement(rootNamespace, null, rootElementName, attrs);

			for (Map.Entry<String, Object> e : values.entrySet()) {
				attrs.clear();

				if (e.getKey() != null)
					attrs.addAttribute(rootNamespace, null, "name", null, e.getKey());

				Object value = e.getValue();
				if (value != null) {
					if (value instanceof byte[]) {
						out.startElement(rootNamespace, null, "data", attrs);
						out.characters(TAB_NEWLINE, 1, 1);
						char[] encodedData = Base64.encodeBytes((byte[]) value).toCharArray();
						int pieces = (encodedData.length + 71) / 72;
						for (int i = 0; i < pieces; i++) {
							out.characters(TAB_NEWLINE, 0, 1);
							if (i == pieces - 1) {
								out.characters(encodedData, i * 72, encodedData.length - i * 72);
							} else {
								out.characters(encodedData, i * 72, 72);
							}
							out.characters(TAB_NEWLINE, 1, 1);
						}
						out.endElement(rootNamespace, null, "data");
					} else {
						attrs.addAttribute(rootNamespace, null, "value", null, value.toString());

						String type = "string";
						if (value instanceof Boolean)
							type = "boolean";
						else if (value instanceof Byte)
							type = "byte";
						else if (value instanceof Short)
							type = "short";
						else if (value instanceof Integer)
							type = "int";
						else if (value instanceof Long)
							type = "long";
						else if (value instanceof Float)
							type = "float";
						else if (value instanceof Double)
							type = "double";

						out.startElement(rootNamespace, null, type, attrs);
						out.endElement(rootNamespace, null, type);
					}
				}
			}

			out.endElement(rootNamespace, null, rootElementName);
			out.endDocument();

		} catch (SAXException | TransformerConfigurationException e) {
			throw new IOException(e);
		}
	}

	public Map<String, Object> deserialize(File source) {
		final Map<String, Object> result = new HashMap<>();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(true);

		try {
			SAXParser parser = factory.newSAXParser();
			parser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			parser.setProperty(JAXP_SCHEMA_SOURCE, getClass().getResourceAsStream(schemaSource));
			parser.parse(source, new ElementHandler(result));
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException | IOException e) {
			result.clear();
			log.log(Level.SEVERE,
					"An error occurred while deserializing input file \"" + source.getAbsolutePath() + "\".", e);
		}
		return result;
	}

	private class ElementHandler extends DefaultHandler {
		private Map<String, Object> map;

		private StringBuilder sb;
		private String key;

		public ElementHandler(Map<String, Object> map) {
			this.map = map;
		}

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			if (ns.equals(rootNamespace)) {
				key = attrs.getValue("", "name");
				String value = attrs.getValue("", "value");
				try {
					if (name.equals("boolean")) {
						map.put(key, new Boolean(value));
					} else if (name.equals("byte")) {
						map.put(key, new Byte(value));
					} else if (name.equals("short")) {
						map.put(key, new Short(value));
					} else if (name.equals("int")) {
						map.put(key, new Integer(value));
					} else if (name.equals("long")) {
						map.put(key, new Long(value));
					} else if (name.equals("float")) {
						map.put(key, new Float(value));
					} else if (name.equals("double")) {
						map.put(key, new Double(value));
					} else if (name.equals("data")) {
						sb = new StringBuilder();
					} else if (!name.equals(rootElementName)) {
						map.put(key, value);
					}
				} catch (NumberFormatException e) {
					log.log(Level.WARNING, null, e);
				}
			}
		}

		@Override
		public void endElement(String ns, String name, String q) throws SAXException {
			if (ns.equals(rootNamespace)) {
				try {
					if (name.equals("data")) {
						map.put(key, Base64.decode(sb.toString()));
						sb = null;
					}
				} catch (IOException e) {
					log.log(Level.WARNING, null, e);
				}
			}
		}

		@Override
		public void characters(char[] chars, int start, int length) throws SAXException {
			if (sb != null)
				sb.append(chars, start, length);
		}
	}
}
