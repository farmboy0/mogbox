package net.sf.mogbox.input;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.java.games.input.Component;
import net.java.games.input.Component.Identifier.Key;
import net.java.games.input.Controller;
import net.java.games.input.Controller.Type;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Keyboard;
import net.java.games.input.Mouse;
import net.sf.mogbox.renderer.Camera;
import net.sf.mogbox.renderer.engine.Core;

public class JInputHandler {
	private static Logger log = Logger.getLogger(JInputHandler.class.getName());

	private static final List<KeyAction> ACTIONS = new ArrayList<>();

	private Core core;
	private Camera camera;

	private Keyboard keyboard;
	private Mouse mouse;
	private Component mousePrimary = null;
	private Component mouseSecondary = null;
	private Component mouseTertiary = null;
	private Component mouseX = null;
	private Component mouseY = null;
	private Component mouseWheel = null;

	public JInputHandler(Core core, Camera camera) {
		this.core = core;
		this.camera = camera;
	}

	public void initialize() {
		ControllerEnvironment environment = ControllerEnvironment.getDefaultEnvironment();
		for (Controller c : environment.getControllers()) {
			if (keyboard == null && c.getType() == Type.KEYBOARD && c.getComponents().length > 0
					&& c.getComponents()[0].getIdentifier() instanceof Key) {
				keyboard = (Keyboard) c;
				log.log(Level.INFO, "Found keyboard: " + keyboard.getName());
			} else if (mouse == null && c.getType() == Type.MOUSE) {
				mouse = (Mouse) c;
				mousePrimary = mouse.getPrimaryButton();
				mouseSecondary = mouse.getSecondaryButton();
				mouseTertiary = mouse.getTertiaryButton();
				mouseX = mouse.getX();
				mouseY = mouse.getY();
				mouseWheel = mouse.getWheel();
				log.log(Level.INFO, "Found mouse: " + mouse.getName());
			}
		}
		registerAllActions();
	}

	public void update() {
		keyboard.poll();
		mouse.poll();

		PointerInfo pointer = MouseInfo.getPointerInfo();
		Point p = pointer.getLocation();
		boolean inBounds = core.isInBounds(p.x, p.y);

		boolean shift = keyboard.isKeyDown(Key.LSHIFT) || keyboard.isKeyDown(Key.RSHIFT);
		boolean ctrl = keyboard.isKeyDown(Key.LCONTROL) || keyboard.isKeyDown(Key.RCONTROL);

		boolean mousePrimaryDown = false;
		boolean mouseSecondaryDown = false;
		boolean mouseTertiaryDown = false;
		float mouseWheelValue = 0;
		if (inBounds) {
			mousePrimaryDown = mousePrimary.getPollData() == 1.0f;
			mouseSecondaryDown = mouseSecondary != null && mouseSecondary.getPollData() == 1.0f;
			mouseTertiaryDown = mouseTertiary != null && mouseTertiary.getPollData() == 1.0f;
			mouseWheelValue = mouseWheel != null ? mouseWheel.getPollData() : 0;
		}

		handleMouseInput(shift, ctrl, mousePrimaryDown, mouseSecondaryDown, mouseTertiaryDown, mouseWheelValue);
		handleKeyboardInput(shift, ctrl);
	}

	private void handleMouseInput(boolean shift, boolean ctrl, boolean mousePrimaryDown, boolean mouseSecondaryDown,
			boolean mouseTertiaryDown, float mouseWheelValue) {

		if (mousePrimaryDown) {
			core.focus();
			if (!ctrl) {
				camera.setYaw(camera.getYaw() + (mouseX.getPollData() * 0.5f));
				camera.setPitch(camera.getPitch() + (mouseY.getPollData() * 0.5f));
			}
		}
		if (mousePrimaryDown && ctrl || mouseSecondaryDown) {
			camera.setPanX(camera.getPanX() - (mouseX.getPollData() * 0.05f));
			camera.setPanY(camera.getPanY() - (mouseY.getPollData() * 0.05f));
		}
		if (mouseTertiaryDown) {
			camera.resetCamera();
		}
		if (ctrl) {
			float fov = camera.getFov();
			fov -= mouseWheelValue * (shift ? 0.5 : 2);
			camera.setFov(fov);
		} else if (Math.abs(mouseWheelValue) > 0) {
			float distance = camera.getDistance();
			distance *= Math.pow(shift ? 1.02 : 1.08, -mouseWheelValue);
			camera.setDistance(distance);
		}
	}

	private void handleKeyboardInput(boolean shift, boolean ctrl) {
		if (core.hasFocus()) {
			ACTIONS.stream().filter(a -> a.shift == shift && a.ctrl == ctrl && keyboard.isKeyDown(a.key))
					.map(a -> a.action).forEach(Runnable::run);
		}
	}

	private void registerAllActions() {
		registerKeyAction(false, true, Key.LEFT, () -> camera.setRoll(camera.getRoll() + 2));
		registerKeyAction(false, true, Key.RIGHT, () -> camera.setRoll(camera.getRoll() - 2));
		registerKeyAction(false, true, Key.UP, () -> camera.setRoll(camera.getRoll() + 2));
		registerKeyAction(false, true, Key.DOWN, () -> camera.setRoll(camera.getRoll() - 2));

		registerKeyAction(true, true, Key.PAGEUP, () -> camera.setFov(camera.getFov() + 0.25f));
		registerKeyAction(true, true, Key.PAGEDOWN, () -> camera.setFov(camera.getFov() - 0.25f));

		registerKeyAction(false, true, Key.PAGEUP, () -> camera.setFov(camera.getFov() + 1));
		registerKeyAction(false, true, Key.PAGEDOWN, () -> camera.setFov(camera.getFov() - 1));

		registerKeyAction(false, false, Key.LEFT, () -> camera.setYaw(camera.getYaw() + 2));
		registerKeyAction(false, false, Key.RIGHT, () -> camera.setYaw(camera.getYaw() - 2));
		registerKeyAction(false, false, Key.UP, () -> camera.setPitch(camera.getPitch() + 2));
		registerKeyAction(false, false, Key.DOWN, () -> camera.setPitch(camera.getPitch() - 2));

		registerKeyAction(true, false, Key.PAGEUP, () -> camera.setDistance(camera.getDistance() / 1.005f));
		registerKeyAction(true, false, Key.PAGEDOWN, () -> camera.setDistance(camera.getDistance() * 1.005f));

		registerKeyAction(false, false, Key.PAGEUP, () -> camera.setDistance(camera.getDistance() / 1.02f));
		registerKeyAction(false, false, Key.PAGEDOWN, () -> camera.setDistance(camera.getDistance() * 1.02f));

		registerKeyAction(false, false, Key.ESCAPE, () -> core.setFullscreen(false));
		registerKeyAction(false, false, Key.HOME, camera::resetCamera);
	}

	private void registerKeyAction(boolean shift, boolean ctrl, Key key, Runnable action) {
		ACTIONS.add(new KeyAction(shift, ctrl, key, action));
	}

	private static final class KeyAction {
		boolean shift;
		boolean ctrl;
		Key key;
		Runnable action;

		public KeyAction(boolean shift, boolean ctrl, Key key, Runnable action) {
			this.shift = shift;
			this.ctrl = ctrl;
			this.key = key;
			this.action = action;
		}
	}
}
