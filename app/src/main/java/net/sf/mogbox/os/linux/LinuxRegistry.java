/* 
 * LinuxRegistry.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.os.linux;

import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.regex.Pattern.MULTILINE;
import static java.util.regex.Pattern.compile;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.mogbox.os.Registry;

public class LinuxRegistry extends Registry {
	private static final Pattern KEY_PATTERN = compile("^\\[(.*)\\]$((?:\r?\n?^[^\\[\n].*[^\\]\n]$)*)", MULTILINE);
	private static final Pattern VALUE_PATTERN = compile("^(?:@|\"(.*)\")=\"((?:[^\\\\\n]|\\\\.)*)\"$", MULTILINE);
	private static final Pattern ESCAPE_PATTERN = compile("\\\\(.)", MULTILINE);

	private static Logger log = Logger.getLogger(LinuxRegistry.class.getName());

	private static volatile Registry singleton = new LinuxRegistry();

	private final Map<String, String> queriedKeys = new HashMap<>();

	public static Registry getInstance() {
		return singleton;
	}

	private LinuxRegistry() {
	}

	@Override
	public String query(int hKey, final String key, final String value) {
		String completeKey;
		switch (hKey) {
		case Registry.HKCR:
			completeKey = "HKEY_CLASSES_ROOT\\" + key;
			break;
		case Registry.HKCU:
			completeKey = "HKEY_CURRENT_USER\\" + key;
			break;
		case Registry.HKLM:
			completeKey = "HKEY_LOCAL_MACHINE\\" + key;
			break;
		default:
			return null;
		}
		return find(queriedKeys.computeIfAbsent(completeKey, this::query), completeKey, value);
	}

	private String query(String completeKey) {
		log.fine(() -> String.format("Reading registry for %s.", completeKey));

		String[] cmd = { "wine", "regedit", "-e", "-", completeKey };
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			final Process proc = new ProcessBuilder(cmd).redirectError(Redirect.INHERIT).start();
			try (final BufferedInputStream in = new BufferedInputStream(proc.getInputStream())) {
				byte[] buf = new byte[0xFFFF];
				for (int r = in.read(buf); r != -1; r = in.read(buf)) {
					os.write(buf, 0, r);
				}
			}

			if (proc.waitFor() != 0)
				return null;
		} catch (IOException | InterruptedException e) {
			log.log(Level.SEVERE, "Error calling wine to read the registry", e);
			return null;
		}

		byte[] bytes = os.toByteArray();
		final String output = new String(bytes, bytes[0] == -1 && bytes[1] == -2 ? UTF_16 : UTF_8);
		log.finer(output::toString);

		return output;
	}

	private String find(String registry, String completeKey, String value) {
		if (registry == null)
			return null;

		Matcher m = KEY_PATTERN.matcher(registry);
		while (m.find()) {
			if (completeKey.equals(m.group(1))) {
				m = VALUE_PATTERN.matcher(m.group(2));
				while (m.find()) {
					String name = m.group(1);
					if (name == null ^ value == null)
						continue;
					if (name != null && !name.equals(value))
						continue;
					String result = ESCAPE_PATTERN.matcher(m.group(2)).replaceAll("$1");
					log.fine(() -> String.format("Found value in registry %s", result));
					return result;
				}
				break;
			}
		}
		return null;
	}
}
