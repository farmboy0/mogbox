/* 
 * Registry.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.os;

import net.sf.mogbox.os.linux.LinuxRegistry;
import net.sf.mogbox.os.windows.WindowsRegistry;

public abstract class Registry {
	public static final int HKCR = 0x00000000;
	public static final int HKCU = 0x00000001;
	public static final int HKLM = 0x00000002;

	private static volatile Registry singleton = null;

	public static Registry getInstance() {
		if (singleton != null)
			return singleton;
		if (OS.isWindows())
			singleton = WindowsRegistry.getInstance();
		else
			singleton = LinuxRegistry.getInstance();
		return singleton;
	}

	protected Registry() {
	}

	public abstract String query(int hKey, String key, String value);
}
