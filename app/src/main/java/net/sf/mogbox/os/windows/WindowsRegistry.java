/* 
 * WindowsRegistry.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.os.windows;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.mogbox.os.OS;
import net.sf.mogbox.os.Registry;

public final class WindowsRegistry extends Registry {
	private static final Pattern VALUE_PATTERN = Pattern
			.compile("^\\s*(?:\\(Default\\)|.*?)\\s+(?:REG_SZ)\\s+(.*?)\\s*$", Pattern.MULTILINE);

	private static Logger log = Logger.getLogger(WindowsRegistry.class.getName());

	private static volatile Registry singleton = null;

	public static Registry getInstance() {
		if (OS.isWindows()) {
			if (singleton == null) {
				synchronized (WindowsRegistry.class) {
					if (singleton == null)
						singleton = new WindowsRegistry();
				}
			}
			return singleton;
		}
		return Registry.getInstance();
	}

	private boolean useNative = true;

	private Class<?> osClass;
	private Class<?> tcharClass;

	private int hKeyClassesRoot;
	private int hKeyCurrentUser;
	private int hKeyLocalMachine;
	private int keyRead;

	private int tcharSizeof;

	private Constructor<?> tcharConstructor1;
	private Constructor<?> tcharConstructor2;

	private Method regOpenKeyEx;
	private Method regQueryValueEx;
	private Method regCloseKey;

	private Method strlen;
	private Method toString;

	private WindowsRegistry() {
		try {
			osClass = Class.forName("org.eclipse.swt.internal.win32.OS");
			tcharClass = Class.forName("org.eclipse.swt.internal.win32.TCHAR");

			hKeyClassesRoot = osClass.getField("HKEY_CLASSES_ROOT").getInt(null);
			hKeyCurrentUser = osClass.getField("HKEY_CURRENT_USER").getInt(null);
			hKeyLocalMachine = osClass.getField("HKEY_LOCAL_MACHINE").getInt(null);
			keyRead = osClass.getField("KEY_READ").getInt(null);

			tcharSizeof = tcharClass.getField("sizeof").getInt(null);

			tcharConstructor1 = tcharClass.getConstructor(int.class, String.class, boolean.class);
			tcharConstructor2 = tcharClass.getConstructor(int.class, int.class);

			if (OS.is64Bit()) {
				regOpenKeyEx = osClass.getMethod("RegOpenKeyEx", long.class, tcharClass, int.class, int.class,
						long[].class);
				regQueryValueEx = osClass.getMethod("RegQueryValueEx", long.class, tcharClass, long.class, int[].class,
						tcharClass, int[].class);
				regCloseKey = osClass.getMethod("RegCloseKey", long.class);
			} else {
				regOpenKeyEx = osClass.getMethod("RegOpenKeyEx", int.class, tcharClass, int.class, int.class,
						int[].class);
				regQueryValueEx = osClass.getMethod("RegQueryValueEx", int.class, tcharClass, int.class, int[].class,
						tcharClass, int[].class);
				regCloseKey = osClass.getMethod("RegCloseKey", int.class);
			}

			strlen = tcharClass.getMethod("strlen");
			toString = tcharClass.getMethod("toString", int.class, int.class);
		} catch (Throwable t) {
			log.log(Level.WARNING, "Error initializing registry classes; Using fallback", t);
			useNative = false;
		}
	}

	@Override
	public String query(int hKey, String key, String value) {
		if (useNative)
			return queryNative(hKey, key, value);
		return queryExec(hKey, key, value);
	}

	private String queryNative(int hKey, String key, String value) {
		try {
			String out = null;
			switch (hKey) {
			default:
				return null;
			case Registry.HKCR:
				hKey = hKeyClassesRoot;
				break;
			case Registry.HKCU:
				hKey = hKeyCurrentUser;
				break;
			case Registry.HKLM:
				hKey = hKeyLocalMachine;
				break;
			}
			Object k = tcharConstructor1.newInstance(0, key, true);
			Object result = null;
			if (OS.is64Bit()) {
				long[] phkResult = new long[1];
				result = regOpenKeyEx.invoke(null, hKey, k, 0, keyRead, phkResult);
				if (result != null && result.equals(0)) {
					int[] lpcbData = new int[1];
					Object buffer = tcharConstructor1.newInstance(0, value, true);
					result = regQueryValueEx.invoke(null, phkResult[0], buffer, 0, null, null, lpcbData);
					if (result != null && result.equals(0)) {
						Object lpData = tcharConstructor2.newInstance(0, lpcbData[0] / tcharSizeof);
						result = regQueryValueEx.invoke(null, phkResult[0], buffer, 0, null, lpData, lpcbData);
						if (result != null && result.equals(0)) {
							out = toString.invoke(lpData, 0, strlen.invoke(lpData)).toString();
						}
					}
					regCloseKey.invoke(null, phkResult[0]);
				}
			} else {
				int[] phkResult = new int[1];
				result = regOpenKeyEx.invoke(null, hKey, k, 0, keyRead, phkResult);
				if (result != null && result.equals(0)) {
					int[] lpcbData = new int[1];
					Object buffer = tcharConstructor1.newInstance(0, value, true);
					result = regQueryValueEx.invoke(null, phkResult[0], buffer, 0, null, null, lpcbData);
					if (result != null && result.equals(0)) {
						Object lpData = tcharConstructor2.newInstance(0, lpcbData[0] / tcharSizeof);
						result = regQueryValueEx.invoke(null, phkResult[0], buffer, 0, null, lpData, lpcbData);
						if (result != null && result.equals(0)) {
							out = toString.invoke(lpData, 0, strlen.invoke(lpData)).toString();
						}
					}
					regCloseKey.invoke(null, phkResult[0]);
				}
			}
			return out;
		} catch (Exception e) {
			log.log(Level.WARNING, "Error reading registry key; Switching to fallback", e);
			useNative = false;
			return queryExec(hKey, key, value);
		}
	}

	private String queryExec(int hKey, String key, String value) {
		switch (hKey) {
		case Registry.HKCR:
			key = "HKEY_CLASSES_ROOT\\" + key;
			break;
		case Registry.HKCU:
			key = "HKEY_CURRENT_USER\\" + key;
			break;
		case Registry.HKLM:
			key = "HKEY_LOCAL_MACHINE\\" + key;
			break;
		}
		try {
			String[] cmd;
			if (value == null)
				cmd = new String[] { "REG", "QUERY", key, "/ve" };
			else
				cmd = new String[] { "REG", "QUERY", key, "/v", value };
			Process proc = Runtime.getRuntime().exec(cmd);
			Reader in = new InputStreamReader(new BufferedInputStream(proc.getInputStream()));

			StringBuilder output = new StringBuilder();
			int length;
			char[] buffer = new char[512];
			do {
				length = in.read(buffer);
				if (length > 0)
					output.append(buffer, 0, length);
			} while (length >= 0);

			if (proc.waitFor() != 0)
				return null;

			Matcher m = VALUE_PATTERN.matcher(output);
			if (m.find())
				return m.group(1);
		} catch (Throwable t) {
			log.log(Level.WARNING, "Error reading registry key", t);
		}
		return null;
	}
}
