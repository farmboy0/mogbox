/* 
 * OS.java
 * 
 * Copyright © 2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.os;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OS {
	private static final String SHELL_FOLDERS_KEY = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders";

	private static final boolean IS_WINDOWS;
	private static final boolean IS_MAC_OS_X;
	private static final boolean IS_64_BIT;

	private static Logger log = Logger.getLogger(OS.class.getName());

	private static File globalSettingsLocation = null;
	private static File userSettingsLocation = null;
	private static File desktopLocation = null;
	private static File documentsLocation = null;
	private static File picturesLocation = null;
	private static File musicLocation = null;
	private static File videosLocation = null;
	private static File windowsApplicationLocation = null;

	static {
		String osname = System.getProperty("os.name").trim().toLowerCase(Locale.ENGLISH);
		String osarch = System.getProperty("os.arch").trim().toLowerCase(Locale.ENGLISH);

		IS_WINDOWS = osname.startsWith("windows");
		IS_MAC_OS_X = osname.startsWith("mac os x");
		IS_64_BIT = osarch.equals("amd64");
	}

	public static boolean isWindows() {
		return IS_WINDOWS;
	}

	public static boolean isMacOSX() {
		return IS_MAC_OS_X;
	}

	public static boolean is64Bit() {
		return IS_64_BIT;
	}

	public static File convertWindowsPath(String path) {
		if (path == null)
			return null;
		if (IS_WINDOWS)
			return new File(path);

		try {
			String[] cmd = { "winepath", "-u", path };
			Process proc = Runtime.getRuntime().exec(cmd);

			Reader in = new InputStreamReader(new BufferedInputStream(proc.getInputStream()));

			StringBuilder output = new StringBuilder();
			int length;
			char[] buffer = new char[512];
			do {
				length = in.read(buffer);
				if (length > 0)
					output.append(buffer, 0, length);
			} while (length >= 0);

			if (proc.waitFor() != 0)
				return null;

			return new File(output.toString().trim()).getCanonicalFile();
		} catch (Throwable t) {
			log.log(Level.FINE, null, t);
		}
		return null;
	}

	public static File getGlobalSettingsLocation() {
		if (globalSettingsLocation != null)
			return globalSettingsLocation;

		if (IS_WINDOWS) {
			try {
				URL u = OS.class.getProtectionDomain().getCodeSource().getLocation();
				globalSettingsLocation = new File(new File(u.toURI()).getParentFile(), "settings");
			} catch (URISyntaxException e) {
				globalSettingsLocation = new File(".", "settings");
			}
		} else if (IS_MAC_OS_X) {
			globalSettingsLocation = new File("/Library/Preferences/Moogle Toolbox/");
		} else {
			globalSettingsLocation = new File("/etc/mogbox/");
		}

		return globalSettingsLocation;
	}

	public static File getUserSettingsLocation() {
		if (userSettingsLocation != null)
			return userSettingsLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			userSettingsLocation = new File(reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "AppData"), "MogBox/");
		} else if (IS_MAC_OS_X) {
			userSettingsLocation = new File(System.getProperty("user.home"), "Library/Preferences/Moogle Toolbox/");
		} else {
			userSettingsLocation = new File(System.getProperty("user.home"), ".mogbox/");
		}

		return userSettingsLocation;
	}

	public static File getDesktopLocation() {
		if (desktopLocation != null)
			return desktopLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			String value = reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "Desktop");
			if (value != null)
				desktopLocation = new File(value);
			else
				desktopLocation = new File(System.getProperty("user.home"), "Desktop");
		} else {
			desktopLocation = new File(System.getProperty("user.home"), "Desktop");
			if (!desktopLocation.exists())
				desktopLocation = desktopLocation.getParentFile();
		}

		return desktopLocation;
	}

	public static File getDocumentsLocation() {
		if (documentsLocation != null)
			return documentsLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			String value = reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "Personal");
			if (value != null)
				documentsLocation = new File(value);
			else
				documentsLocation = new File(System.getProperty("user.home"), "My Documents");
		} else {
			documentsLocation = new File(System.getProperty("user.home"), "Documents");
			if (!documentsLocation.exists())
				documentsLocation = documentsLocation.getParentFile();
		}

		return documentsLocation;
	}

	public static File getPicturesLocation() {
		if (picturesLocation != null)
			return picturesLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			String value = reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "My Pictures");
			if (value != null)
				picturesLocation = new File(value);
			else
				picturesLocation = getDocumentsLocation();
		} else {
			picturesLocation = new File(System.getProperty("user.home"), "Pictures");
			if (!picturesLocation.exists())
				picturesLocation = picturesLocation.getParentFile();
		}

		return picturesLocation;
	}

	public static File getMusicLocation() {
		if (musicLocation != null)
			return musicLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			String value = reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "My Music");
			if (value != null)
				musicLocation = new File(value);
			else
				musicLocation = getDocumentsLocation();
		} else {
			musicLocation = new File(System.getProperty("user.home"), "Music");
			if (!musicLocation.exists())
				musicLocation = musicLocation.getParentFile();
		}

		return musicLocation;
	}

	public static File getVideosLocation() {
		if (videosLocation != null)
			return videosLocation;

		if (IS_WINDOWS) {
			Registry reg = Registry.getInstance();
			String value = reg.query(Registry.HKCU, SHELL_FOLDERS_KEY, "My Video");
			if (value != null)
				videosLocation = new File(value);
			else
				videosLocation = getDocumentsLocation();
		} else {
			videosLocation = new File(System.getProperty("user.home"), "Videos");
			if (!videosLocation.exists())
				videosLocation = new File(System.getProperty("user.home"), "Video");
			if (!videosLocation.exists())
				videosLocation = videosLocation.getParentFile();
		}

		return videosLocation;
	}

	public static File getWindowsApplicationLocation() {
		if (windowsApplicationLocation != null)
			return windowsApplicationLocation;

		String key = "Software\\Microsoft\\Windows\\CurrentVersion";
		Registry reg = Registry.getInstance();
		String value = reg.query(Registry.HKLM, key, "ProgramFilesDir (x86)");
		if (value != null) {
			windowsApplicationLocation = convertWindowsPath(value);
		} else {
			value = reg.query(Registry.HKLM, key, "ProgramFilesDir");
			if (value != null)
				windowsApplicationLocation = convertWindowsPath(value);
			else if (IS_WINDOWS && IS_64_BIT)
				windowsApplicationLocation = new File("C:\\Program Files (x86)");
			else
				windowsApplicationLocation = convertWindowsPath("C:\\Program Files");
		}

		return windowsApplicationLocation;
	}
}
