/* 
 * PluginVersion.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PluginVersion implements Comparable<PluginVersion> {
	private static final Pattern VERSION = Pattern.compile("([0-9]+)(?:\\.([0-9]+)(?:\\.([0-9]+))?)?(.*)");

	private final int majorVersion;
	private final int minorVersion;
	private final int maintVersion;
	private final String buildString;

	public PluginVersion(String versionString) {
		Matcher m = VERSION.matcher(versionString);
		if (!m.matches())
			throw new IllegalArgumentException("Invalid version string.");
		majorVersion = Integer.parseInt(m.group(1));
		String s;
		if ((s = m.group(2)) != null)
			minorVersion = Integer.parseInt(s);
		else
			minorVersion = 0;
		if ((s = m.group(3)) != null)
			maintVersion = Integer.parseInt(s);
		else
			maintVersion = 0;
		buildString = m.group(4).trim();
	}

	public PluginVersion(int major, int minor, int maint, String build) {
		majorVersion = major >= 0 ? major : 0;
		minorVersion = minor >= 0 ? minor : 0;
		maintVersion = maint >= 0 ? maint : 0;
		buildString = build != null ? build.trim() : "";
	}

	public PluginVersion(int major, int minor, int maint) {
		this(major, minor, maint, "");
	}

	public PluginVersion(int major, int minor) {
		this(major, minor, 0, "");
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(majorVersion).append('.').append(minorVersion);
		if (maintVersion != 0)
			s.append('.').append(maintVersion);
		if (!buildString.isEmpty())
			s.append(' ').append(buildString);
		return s.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + majorVersion;
		result = prime * result + minorVersion;
		result = prime * result + maintVersion;
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		PluginVersion p = (PluginVersion) o;
		if (majorVersion != p.majorVersion)
			return false;
		if (minorVersion != p.minorVersion)
			return false;
		if (maintVersion != p.maintVersion)
			return false;
		return true;
	}

	@Override
	public int compareTo(PluginVersion p) {
		if (majorVersion != p.majorVersion)
			return majorVersion - p.majorVersion;
		if (minorVersion != p.minorVersion)
			return minorVersion - p.minorVersion;
		if (maintVersion != p.maintVersion)
			return maintVersion - p.maintVersion;
		return 0;
	}
}
