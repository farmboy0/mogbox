/* 
 * PluginClassLoader.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.Set;

public final class PluginClassLoader extends URLClassLoader {
	private Plugin plugin;
	private File location;
	private File resourceLocation;

	public PluginClassLoader(Plugin plugin) {
		super(new URL[0]);
		this.plugin = plugin;
		location = plugin.getLocation();
		if (location != null) {
			try {
				addURL(location.toURI().toURL());
			} catch (MalformedURLException e) {
			}
		}
		resourceLocation = plugin.getResourceLocation();
	}

	protected Plugin getPlugin() {
		return plugin;
	}

	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		if (!plugin.isResolved())
			throw new ClassNotFoundException(name);

		if (location == null)
			return super.loadClass(name);

		Class<?> loadedClass = findLoadedClass(name);

		if (loadedClass == null) {
			try {
				loadedClass = findClass(name);
			} catch (ClassNotFoundException e) {
			}

			if (loadedClass == null) {
				Set<Dependency> dependancies = plugin.getDependencies();
				for (Dependency dependency : dependancies) {
					Plugin dependencyPlugin = dependency.getPlugin();
					if (dependencyPlugin == null || !dependencyPlugin.isResolved())
						continue;
					PluginClassLoader cl = dependencyPlugin.getClassLoader();
					try {
						if ((loadedClass = cl.findClass(name)) != null)
							break;
					} catch (ClassNotFoundException e) {
					}
				}
			}

			if (loadedClass == null)
				loadedClass = super.loadClass(name, resolve);
		}

		if (resolve)
			resolveClass(loadedClass);

		return loadedClass;
	}

	@Override
	public URL getResource(String name) {
		if (!plugin.isResolved())
			return null;

		URL url = findResource(name);

		if (url == null) {
			Set<Dependency> dependancies = plugin.getDependencies();
			for (Dependency dependency : dependancies) {
				Plugin dependencyPlugin = dependency.getPlugin();
				if (dependencyPlugin == null || !dependencyPlugin.isResolved())
					continue;
				PluginClassLoader cl = dependencyPlugin.getClassLoader();
				if ((url = cl.findResource(name)) != null)
					break;
			}
		}

		if (url == null)
			url = getParent().getResource(name);

		return url;
	}

	@Override
	public URL findResource(String name) {
		Locale locale = Locale.getDefault();

		if (!locale.getVariant().isEmpty() && (!locale.getLanguage().isEmpty() || !locale.getCountry().isEmpty())) {
			String l = locale.getLanguage() + "_" + locale.getCountry() + "_" + locale.getVariant();
			File file = new File(resourceLocation, l + File.separator + name);
			if (file.exists()) {
				try {
					return file.toURI().toURL();
				} catch (MalformedURLException e) {
				}
			}

			URL url = super.findResource(l + "/" + name);
			if (url != null)
				return url;
		}

		if (!locale.getCountry().isEmpty()) {
			String l = locale.getLanguage() + "_" + locale.getCountry();
			File file = new File(resourceLocation, l + File.separator + name);
			if (file.exists()) {
				try {
					return file.toURI().toURL();
				} catch (MalformedURLException e) {
				}
			}

			URL url = super.findResource(l + "/" + name);
			if (url != null)
				return url;
		}

		if (!locale.getLanguage().isEmpty()) {
			String l = locale.getLanguage();
			File file = new File(resourceLocation, l + File.separator + name);
			if (file.exists()) {
				try {
					return file.toURI().toURL();
				} catch (MalformedURLException e) {
				}
			}

			URL url = super.findResource(l + "/" + name);
			if (url != null)
				return url;
		}

		File file = new File(resourceLocation, name);
		if (file.exists()) {
			try {
				return file.toURI().toURL();
			} catch (MalformedURLException e) {
			}
		}

		return super.findResource(name);
	}
}
