/* 
 * ExtensionPoint.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.util.Collection;
import java.util.LinkedList;

import net.sf.mogbox.plugin.event.ExtensionEvent;
import net.sf.mogbox.plugin.event.ExtensionListener;

public class ExtensionPoint {
	private final String name;
	private final Collection<Extension> extensions = new LinkedList<Extension>();
	private final Collection<ExtensionListener> listeners = new LinkedList<ExtensionListener>();

	public ExtensionPoint(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addExtensionListener(ExtensionListener listener) {
		listeners.add(listener);
	}

	public void removeExtensionListener(ExtensionListener listener) {
		listeners.remove(listener);
	}

	protected void loadExtension(Extension extension) {
		if (extensions.add(extension)) {
			ExtensionEvent event = new ExtensionEvent(this, extension);
			for (ExtensionListener l : listeners) {
				l.extensionLoaded(event);
			}
		}
	}

	protected void unloadExtension(Extension extension) {
		if (extensions.remove(extension)) {
			ExtensionEvent event = new ExtensionEvent(this, extension);
			for (ExtensionListener l : listeners) {
				l.extensionLoaded(event);
			}
		}
	}

	public Extension[] getExtensions() {
		return extensions.toArray(new Extension[extensions.size()]);
	}
}
