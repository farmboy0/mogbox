/* 
 * PluginEngine.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PluginEngine {
	private static Logger log = Logger.getLogger(PluginEngine.class.getName());
	private final Map<String, Plugin> plugins = new HashMap<String, Plugin>();

	private final List<Plugin> unresolvedPlugins = new LinkedList<Plugin>();
	private final List<Dependency> unresolvedDependencies = new LinkedList<Dependency>();

	private Set<File> locations = new HashSet<File>();
	private Boolean loadDirectories = true;
	private List<String> extensions = new LinkedList<String>();

	private File resources;

	private boolean running = false;

	public PluginEngine() {
		resources = new File("");
	}

	public PluginEngine(File resourceLocation) {
		resources = resourceLocation;
	}

	public void start() {
		if (running)
			return;
		loadPlugins();
		running = true;
		resolve();
	}

	public void addPluginExtension(String ext) {
		extensions.add(ext);
	}

	public void loadDirectories(boolean load) {
		loadDirectories = load;
	}

	public void addPluginLocation(File file) {
		locations.add(file);
	}

	private void loadPlugins() {
		for (File location : locations) {
			if (location.isDirectory()) {
				File[] files = location.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						if (loadDirectories && pathname.isDirectory()) {
							return true;
						} else {
							if (extensions.size() > 0) {
								for (String s : extensions) {
									if (pathname.getName().endsWith("." + s))
										return true;
								}
							} else {
								return true;
							}
						}
						return false;
					}
				});
				for (File file : files) {
					if (file.exists()) {
						try {
							new Plugin(this, file, false);
						} catch (IOException e) {
							log.log(Level.WARNING, null, e);
						}
					}
				}
			} else {
				if (location.exists()) {
					try {
						new Plugin(this, location, false);
					} catch (IOException e) {
						log.log(Level.WARNING, null, e);
					}
				}
			}
		}
		resolve();
	}

	public void loadPlugins(File[] locations) {
		for (File f : locations) {
			if (f.exists()) {
				try {
					new Plugin(this, f, false);
				} catch (IOException e) {
					log.log(Level.WARNING, null, e);
				}
			}
		}
		resolve();
	}

	public void loadPlugin(File location) {
		if (location.exists()) {
			try {
				new Plugin(this, location);
			} catch (IOException e) {
				log.log(Level.WARNING, null, e);
			}
		}
	}

	protected void add(Plugin plugin, boolean resolve) {
		if (plugins.containsKey(plugin.getName()))
			return;
		plugins.put(plugin.getName(), plugin);
		unresolvedPlugins.add(plugin);
		if (resolve)
			resolve();
	}

	public Plugin getPlugin(String name) {
		return plugins.get(name);
	}

	public void setResourceLocation(File location) {
		resources = location;
	}

	public File getRecourceLocation(Plugin plugin) {
		return new File(resources, plugin.getName());
	}

	protected void resolve() {
		if (!running)
			return;
		Set<Plugin> resolvablePlugins = new HashSet<Plugin>();

		// find resolvable plugins
		for (Iterator<Plugin> i = unresolvedPlugins.iterator(); i.hasNext();) {
			Plugin plugin = (Plugin) i.next();
			if (isResolvable(plugin, new HashSet<Plugin>(resolvablePlugins))) {
				i.remove();
				resolvablePlugins.add(plugin);
				plugin.setResolved(true);
			}
		}

		// resolve each plugin's dependencies
		for (Iterator<Plugin> i = resolvablePlugins.iterator(); i.hasNext();) {
			Plugin plugin = (Plugin) i.next();
			for (Iterator<Dependency> j = plugin.getDependencies().iterator(); j.hasNext();) {
				Dependency dependency = (Dependency) j.next();
				Plugin dependencyPlugin = plugins.get(dependency.getPluginName());
				if (dependencyPlugin == null || !dependencyPlugin.isResolved())
					unresolvedDependencies.add(dependency);
				else
					dependency.resolve(dependencyPlugin);
			}
		}

		// // resolve any optional dependencies that are now resolvable
		// for (Iterator<Dependency> i = unresolvedDependencies.iterator();
		// i.hasNext();) {
		// Dependency dependency = i.next();
		// Plugin resolveToPlugin = getPlugin(dependency.getPluginName());
		//
		// if (resolveToPlugin != null) {
		// i.remove();
		// dependency.resolve(resolveToPlugin);
		// }
		// }

		// resolve each plugin's extensions
		for (Iterator<Plugin> i = resolvablePlugins.iterator(); i.hasNext();) {
			Plugin plugin = i.next();

			// resolve any extensions that resolve to this plugin.
			for (Plugin p : plugins.values()) {
				if (p != plugin && p.isResolved()) {
					for (Extension e : p.getExtensions()) {
						if (e.isResolved())
							continue;
						if (plugin.getName().equals(e.getPluginName())) {
							String name = e.getExtensionPointName();
							ExtensionPoint point = plugin.getExtensionPoint(name);
							if (point != null) {
								e.resolve(point);
							}
						}
					}
				}
			}

			// resolve this plugin's extensions.
			for (Extension e : plugin.getExtensions()) {
				if (e.isResolved())
					continue;
				Plugin p = plugins.get(e.getPluginName());
				if (p != null && p.isResolved()) {
					ExtensionPoint point = p.getExtensionPoint(e.getExtensionPointName());
					if (point != null) {
						e.resolve(point);
					}
				}
			}
		}
	}

	private boolean isResolvable(Plugin plugin, Set<Plugin> ignore) {
		ignore.add(plugin);

		try {
			for (Dependency dependency : plugin.getDependencies()) {
				Plugin dependencyPlugin = plugins.get(dependency.getPluginName());
				if (dependencyPlugin == null || !dependency.isCompatable(dependencyPlugin))
					return false;
				if (!dependencyPlugin.isResolved()) {
					if (!ignore.contains(dependencyPlugin)) {
						if (!isResolvable(dependencyPlugin, ignore)) {
							return false;
						}
					}
				}
			}
			return true;
		} finally {
			ignore.remove(plugin);
		}
	}
}
