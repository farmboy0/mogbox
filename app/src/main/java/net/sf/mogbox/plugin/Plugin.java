/* 
 * Plugin.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.mogbox.plugin.manifest.PluginManifest;
import net.sf.mogbox.plugin.manifest.XMLPluginManifest;

public class Plugin {
	private static Logger log = Logger.getLogger(Plugin.class.getName());

	public static Plugin getPlugin(Object o) {
		return getPlugin(o.getClass());
	}

	public static Plugin getPlugin(Class<?> c) {
		ClassLoader loader = c.getClassLoader();
		if (loader instanceof PluginClassLoader)
			return ((PluginClassLoader) loader).getPlugin();
		throw new IllegalArgumentException("Class must belong to a plugin.");
	}

	private File location;
	private PluginManifest manifest;

	private boolean resolved = false;

	private PluginEngine engine;
	private PluginClassLoader classLoader;

	public Plugin(PluginEngine engine, PluginManifest manifest) {
		this(engine, manifest, true);
	}

	public Plugin(PluginEngine engine, File location) throws IOException {
		this(engine, location, true);
	}

	Plugin(PluginEngine engine, PluginManifest manifest, boolean resolve) {
		if (engine == null)
			throw new IllegalArgumentException("Engine cannot be null.");
		if (manifest == null)
			throw new IllegalArgumentException("Manifest cannot be null.");

		this.engine = engine;
		this.manifest = manifest.clone();

		for (Extension e : manifest.getExtensions()) {
			e.setPlugin(this);
		}

		engine.add(this, resolve);
	}

	Plugin(PluginEngine engine, File location, boolean resolve) throws IOException {
		this.engine = engine;
		this.location = location;

		InputStream in;
		if (location.isDirectory()) {
			File manifestFile = new File(location, "plugin.xml");
			if (!manifestFile.exists()) {
				String msg = String.format("%s not found.", manifestFile.getAbsolutePath());
				throw new FileNotFoundException(msg);
			}
			in = new FileInputStream(manifestFile);
		} else {
			JarFile jar = new JarFile(location, true, JarFile.OPEN_READ);
			JarEntry manifestFile = jar.getJarEntry("plugin.xml");
			if (manifestFile == null) {
				String msg = String.format("%s!/plugin.xml not found.", jar.getName());
				throw new FileNotFoundException(msg);
			}
			in = jar.getInputStream(manifestFile);
		}

		try {
			manifest = new XMLPluginManifest(in);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				log.log(Level.WARNING, "Failed to close plugin manifest stream.", e);
			}
		}

		for (Extension e : manifest.getExtensions()) {
			e.setPlugin(this);
		}

		engine.add(this, resolve);
	}

	PluginEngine getPluginEngine() {
		return engine;
	}

	public String getName() {
		return manifest.getName();
	}

	public PluginVersion getVersion() {
		return manifest.getVersion();
	}

	public File getLocation() {
		return location;
	}

	Set<Dependency> getDependencies() {
		return manifest.getDependencies();
	}

	Collection<Extension> getExtensions() {
		return manifest.getExtensions();
	}

	Map<String, ExtensionPoint> getExtensionPoints() {
		return manifest.getExtensionPoints();
	}

	public ExtensionPoint getExtensionPoint(String name) {
		return manifest.getExtensionPoints().get(name);
	}

	File getResourceLocation() {
		return engine.getRecourceLocation(this);
	}

	public boolean isResolved() {
		return resolved;
	}

	void setResolved(boolean resolved) {
		this.resolved = resolved;
	}

	public PluginClassLoader getClassLoader() {
		if (!isResolved()) {
			String msg = String.format("Plugin \"%s\" has not been resolved.", getName());
			throw new RuntimeException(msg);
		}
		if (classLoader == null)
			classLoader = new PluginClassLoader(this);
		return classLoader;
	}

	public Object newInstance(String className) {
		try {
			Class<?> c = getClassLoader().loadClass(className);
			return c.newInstance();
		} catch (Exception e) {
			log.log(Level.WARNING, null, e);
		}
		return null;
	}

	public <T> T newInstance(String className, Class<T> superClass) {
		try {
			Class<?> c = getClassLoader().loadClass(className);
			return superClass.cast(c.newInstance());
		} catch (ClassNotFoundException e) {
			log.log(Level.FINE, null, e);
		} catch (InstantiationException e) {
			log.log(Level.FINE, null, e);
		} catch (IllegalAccessException e) {
			log.log(Level.FINE, null, e);
		} catch (ClassCastException e) {
			log.log(Level.FINE, null, e);
		}
		return null;
	}

	public URL getResource(String name) {
		return getClassLoader().getResource(name);
	}

	public InputStream getResourceAsStream(String name) {
		return getClassLoader().getResourceAsStream(name);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((engine == null) ? 0 : engine.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plugin other = (Plugin) obj;
		if (engine == null) {
			if (other.engine != null)
				return false;
		} else if (!engine.equals(other.engine))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}
}
