/* Extension.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

import java.io.InputStream;
import java.net.URL;

public class Extension {
	private final String pluginName;
	private final String extensionPointName;
	private final String className;

	// private Map<String, String> parameters = new HashMap<String, String>();

	private Plugin plugin;

	private ExtensionPoint extensionPoint;

	public Extension(String pluginName, String extensionPointName, String className) {
		this.pluginName = pluginName;
		this.extensionPointName = extensionPointName;
		this.className = className;
	}

	void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}

	public String getPluginName() {
		return pluginName;
	}

	public String getExtensionPointName() {
		return extensionPointName;
	}

	public String getClassName() {
		return className;
	}

	// public void setParameter(String name, String value) {
	// parameters.put(name, value);
	// }
	//
	// public String getParameter(String name) {
	// return parameters.get(name);
	// }

	public Object newInstance() {
		if (className == null)
			return null;
		return plugin.newInstance(className);
	}

	public <T> T newInstance(Class<T> superClass) {
		if (className == null)
			return null;
		return plugin.newInstance(className, superClass);
	}

	public URL getResource(String name) {
		return plugin.getResource(name);
	}

	public InputStream getResourceAsStream(String name) {
		return plugin.getResourceAsStream(name);
	}

	public boolean isResolved() {
		return extensionPoint != null;
	}

	protected void resolve(ExtensionPoint point) {
		extensionPoint = point;
		extensionPoint.loadExtension(this);
	}
}
