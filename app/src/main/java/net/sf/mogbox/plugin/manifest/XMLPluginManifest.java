/* 
 * XMLPluginManifest.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin.manifest;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.sf.mogbox.plugin.Dependency;
import net.sf.mogbox.plugin.Extension;
import net.sf.mogbox.plugin.ExtensionPoint;
import net.sf.mogbox.plugin.PluginVersion;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLPluginManifest extends PluginManifest {
	private static final String XML_NAMESPACE = "urn:xmlns:mogbox:plugin:1.0";

	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	public XMLPluginManifest(InputStream in) throws IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(true);

		try {
			SAXParser parser = factory.newSAXParser();
			parser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			parser.setProperty(JAXP_SCHEMA_SOURCE, getClass().getResourceAsStream("plugin.xsd"));
			parser.parse(in, new XMLPluginManifestHandler());
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		}
	}

	private class XMLPluginManifestHandler extends DefaultHandler {
		private StringBuilder buffer;

		@Override
		public void startElement(String ns, String name, String q, Attributes attrs) throws SAXException {
			if (ns.equals(XML_NAMESPACE)) {
				if (name.equals("name") || name.equals("version")) {
					buffer = new StringBuilder();
				} else if (name.equals("dependency")) {
					String temp;
					Dependency dependency = new Dependency();

					dependency.setPluginName(attrs.getValue("", "plugin"));

					temp = attrs.getValue("", "versionMin");
					if (temp != null)
						dependency.setMinVersion(new PluginVersion(temp));
					temp = attrs.getValue("", "versionMax");
					if (temp != null)
						dependency.setMaxVersion(new PluginVersion(temp));

					addDependency(dependency);
				} else if (name.equals("extension")) {
					String pluginName = attrs.getValue("", "plugin");
					String pointName = attrs.getValue("", "name");
					String className = attrs.getValue("", "class");
					addExtension(new Extension(pluginName, pointName, className));
				} else if (name.equals("extensionpoint")) {
					addExtensionPoint(new ExtensionPoint(attrs.getValue("", "name")));
				}
			}
		}

		@Override
		public void endElement(String ns, String name, String q) throws SAXException {
			if (ns.equals(XML_NAMESPACE)) {
				if (name.equals("name")) {
					setName(buffer.toString());
					buffer = null;
				} else if (name.equals("version")) {
					setVersion(new PluginVersion(buffer.toString()));
					buffer = null;
				}
			}
		}

		@Override
		public void characters(char[] chars, int start, int length) throws SAXException {
			if (buffer != null)
				buffer.append(chars, start, length);
		}
	}
}
