/* 
 * PluginManifest.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin.manifest;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import net.sf.mogbox.plugin.Dependency;
import net.sf.mogbox.plugin.Extension;
import net.sf.mogbox.plugin.ExtensionPoint;
import net.sf.mogbox.plugin.PluginVersion;

public class PluginManifest implements Cloneable {
	private String name;
	private PluginVersion version;
	private Set<Dependency> dependencies = new HashSet<Dependency>();
	private Collection<Extension> extensions = new LinkedList<Extension>();
	private Map<String, ExtensionPoint> extensionPoints = new HashMap<String, ExtensionPoint>();

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setVersion(PluginVersion version) {
		this.version = version;
	}

	public PluginVersion getVersion() {
		return version;
	}

	public void addDependency(Dependency dependency) {
		dependencies.add(dependency);
	}

	public Set<Dependency> getDependencies() {
		return dependencies;
	}

	public void addExtension(Extension extension) {
		extensions.add(extension);
	}

	public Collection<Extension> getExtensions() {
		return extensions;
	}

	public void addExtensionPoint(ExtensionPoint extensionPoint) {
		extensionPoints.put(extensionPoint.getName(), extensionPoint);
	}

	public Map<String, ExtensionPoint> getExtensionPoints() {
		return extensionPoints;
	}

	public PluginManifest clone() {
		PluginManifest clone = new PluginManifest();
		clone.name = name;
		clone.version = version;
		clone.dependencies = new HashSet<Dependency>(dependencies);
		clone.extensions = new LinkedList<Extension>(extensions);
		clone.extensionPoints = new HashMap<String, ExtensionPoint>(extensionPoints);
		return clone;
	}
}
