/* 
 * Dependency.java
 * 
 * Copyright © 2008-2009 Sean Whalen (alphaone2@gmail.com)
 * 
 * This file is part of MogBox.
 * 
 * MogBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MogBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MogBox.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify MogBox, or any covered work, by linking or combining it
 * with the Standard Widget Toolkit (SWT) (or a modified version of that
 * library), containing parts covered by the terms of the the
 * Eclipse Public License, the licensors of MogBox grant you additional
 * permission to convey the resulting work.
 */
package net.sf.mogbox.plugin;

public class Dependency {
	private String name;
	private Plugin plugin;
	private PluginVersion minVersion;
	private PluginVersion maxVersion;

	// private boolean optional;

	public void setPluginName(String name) {
		this.name = name;
	}

	public String getPluginName() {
		return name;
	}

	public void setMinVersion(PluginVersion version) {
		minVersion = version;
	}

	public PluginVersion getMinVersion() {
		return minVersion;
	}

	public void setMaxVersion(PluginVersion version) {
		maxVersion = version;
	}

	public PluginVersion getMaxVersion() {
		return maxVersion;
	}

	// public boolean isOptional() {
	// return optional;
	// }

	public Plugin getPlugin() {
		return plugin;
	}

	protected boolean isCompatable(Plugin plugin) {
		if (plugin == null || !plugin.getName().equals(name))
			return false;
		PluginVersion version = plugin.getVersion();
		if (version == null)
			return minVersion == null && maxVersion == null;
		if (minVersion != null && minVersion.compareTo(version) > 0)
			return false;
		if (minVersion != null && maxVersion.compareTo(version) < 0)
			return false;
		return true;
	}

	protected void resolve(Plugin plugin) {
		if (isCompatable(plugin))
			this.plugin = plugin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((maxVersion == null) ? 0 : maxVersion.hashCode());
		result = prime * result + ((minVersion == null) ? 0 : minVersion.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((plugin == null) ? 0 : plugin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dependency other = (Dependency) obj;
		if (maxVersion == null) {
			if (other.maxVersion != null)
				return false;
		} else if (!maxVersion.equals(other.maxVersion))
			return false;
		if (minVersion == null) {
			if (other.minVersion != null)
				return false;
		} else if (!minVersion.equals(other.minVersion))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (plugin == null) {
			if (other.plugin != null)
				return false;
		} else if (!plugin.equals(other.plugin))
			return false;
		return true;
	}
}
